﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Objects
{
    public class ServiceError
    {
        public String errorTarget { get; set; }
        public String errorText { get; set; }

        public ServiceError()
        {
            errorText = String.Empty;
            errorTarget = String.Empty;
        }
    }
}
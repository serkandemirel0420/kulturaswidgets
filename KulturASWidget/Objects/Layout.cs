﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Helpers.NotesHelper;
using KulturASWidget.Models.Marketing;
using Newtonsoft.Json;
using System.Globalization;

namespace KulturASWidget.Objects
{
    public class Layout : BaseObject
    {
        private const double COMMISSON_RATE = 0.049;
        private const int CONSTANT_COMMISSION = 49;
        private const double KDV_RATE = 1.18;

        public Int32 LayoutID { get; set; }
        public Int32 EventID { get; set; }
        public String Name { get; set; }
        
        public Int32 MaxCapacity { get; set; }
        public Int32 UsedCapacity { get; set; }
        public Boolean Status { get; set; }


        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Int32 MaxTicket { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Int32 MinTicket { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public List<MarketingDiscountsModel> Discounts { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Boolean IsBoothTicket { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public DateTime SaleStartDate { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public DateTime SaleEndDate { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string Detail { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public CommissionPayer commissionPayer { get; set; }

        public string saleStartDateStr
        {
            get
            {
                return SaleStartDate.ToString("dd/MM/yyyy",CultureInfo.InvariantCulture);
            }
        }
        public string saleEndDateStr
        {
            get
            {
                return SaleEndDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
        }
        public Layout deserialize(Newtonsoft.Json.Linq.JObject jObject)
        {
            this.LayoutID = jObject.Value<Int32>("layoutID");
            this.EventID = jObject.Value<Int32>("eventID");
            this.Name = jObject.Value<String>("name");
            this.PriceWithoutCommission = jObject.Value<Int32>("fee");
            string notes = jObject.Value<String>("notes");

            NotesCreator.Instance.SetValuesToObject(this, notes, NOTES_GROUP_NAME_NOTES);

            this.MaxCapacity = jObject.Value<Int32>("maxCapacity");
            this.UsedCapacity = jObject.Value<Int32>("usedCapacity");
            this.Status = jObject.Value<Boolean>("status");

            return this;
        }




        public Int32 PriceWithoutCommission { get; set; }


        public Int32 Commission
        {
            get
            {
                if (this.PriceWithoutCommission != 0)
                {
                    double com = this.PriceWithoutCommission * COMMISSON_RATE * KDV_RATE;
                    if (com > 0)
                    {
                        com += CONSTANT_COMMISSION * KDV_RATE;
                    }
                    return (int)Math.Round(com);
                }
                else
                {
                    return 0;
                }
            }
        }

        public Int32 TotalPrice
        {
            get
            {
                if (commissionPayer == CommissionPayer.byUser)
                {
                    return PriceWithoutCommission + Commission;
                }
                else
                {
                    return PriceWithoutCommission;
                }
            }
        }

        public Int32 ToOrganizator
        {
            get
            {
                return TotalPrice - Commission;
            }
        }

        public Int32 AvaliableCapacity
        {
            get
            {
                return MaxCapacity - UsedCapacity;
            }
        }

        public String DisplayTotalPrice
        {
            get
            {
                return String.Format("{0} TL", ((double)this.TotalPrice) / 100);
            }
        }
        public String DisplayCommission
        {
            get
            {
                return String.Format("{0} TL", ((double)this.Commission) / 100);
            }
        }

        public String DisplayPriceWithoutCommission
        {
            get
            {
                return String.Format("{0} TL", ((double)this.PriceWithoutCommission) / 100);
            }
        }

        public String DisplayCommissionPayer
        {
            get
            {
                return this.commissionPayer == CommissionPayer.byOrganization ? "Organizatör Tarafından" : "Kullanıcı Tarafından";
            }
        }



    }


}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Objects
{
    public class EventQuestion
    {
        public String Question { get; set; }
        public IList<String> AnswerList { get; set; }
        public bool ShowInModeration { get; set; }
        public EventQuestion()
        {
            AnswerList = new List<String>();
        }
    }
}
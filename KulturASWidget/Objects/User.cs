﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;
using System.Linq;
using KulturASWidget.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KulturASWidget.Helpers.NotesHelper;
using KulturASWidget.Helpers;
using KulturASWidget.Services.Security;
using System.Collections.Generic;

using KulturASWidget.Services;
using KulturASWidget.Resources;

namespace KulturASWidget.Objects
{
    public class User : BaseObject
    {
        public Int32 UserID { get; set; }
        public String FacebookID { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Mail { get; set; }
        private String cell;
        public String Cell
        {
            get
            {
                return cell;
            }
            set
            {
                this.cell = value.FormatPhoneNumber();
            }
        }
        public String Password { get; set; }
        public String CountryCode { get; set; }
        public String City { get; set; }
        public String District { get; set; }
        public String Address { get; set; }
        private String organizationName;
        public User()
        {
     
            this.ticketSalesInfoMailList = new List<string>();
            this.userRoles = new List<UserRoles>();
        }

        public String Validation { get; set; }
        public Boolean Status { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Gender Gender { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String Interests { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String Notes { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool HasLogo { get; set; }


       
        

        
       

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String OrganizationPhotoCacheBuster
        {
            get
            {
                if (organizationPhotoCacheBuster.IsEmpty())
                {
                    this.generateOrganizationPhotoCacheBuster();
                }
                return organizationPhotoCacheBuster;
            }
            set
            {
                organizationPhotoCacheBuster = value;
            }
        }

        public String organizationPhotoCacheBuster;


        public void generateOrganizationPhotoCacheBuster()
        {
            this.organizationPhotoCacheBuster = Methods.Instance.GenerateRandomAlphanumeric(5);
        }


        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String OrganizationBackgroundCacheBuster
        {
            get
            {
                if (organizationBackgroundCacheBuster.IsEmpty())
                {
                    this.generateOrganizationBackgroundCacheBuster();
                }
                return organizationBackgroundCacheBuster;
            }
            set
            {
                organizationBackgroundCacheBuster = value;
            }
        }

        public String organizationBackgroundCacheBuster;

        public void generateOrganizationBackgroundCacheBuster()
        {
            this.organizationBackgroundCacheBuster = Methods.Instance.GenerateRandomAlphanumeric(5);
        }


        public String LastLoginDateFormat
        {
            get
            {
                DateTime dt = TimeZoneInfo.ConvertTimeFromUtc(this.LastLoginDateUTC, ApplicationConfiguration.CurrentTimezone);
                if (this.LastLoginDateUTC == new DateTime())
                {
                    return PageResources.NotLoggedIn;
                }
                return String.Format("{0:dd MMM yyyy HH:mm}", dt);
            }
        }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public DateTime LastLoginDateUTC { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String IdentityTC { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public DateTime CreationDateUTC { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String CreationIPAddress { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public DateTime ResetPasswordExpireTime { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String PasswordResetKey { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Boolean IsBlocked { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Int32 RoleID { get; set; }
       

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String ContactMail
        {
            get
            {
                return this.contactMail;
            }
            set
            {
                if (value.IsExists())
                {
                    this.contactMail = value;
                }
                else
                {
                    this.contactMail = this.Mail;
                }

            }
        }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public List<String> TicketSalesInfoMailList
        {
            get
            {
                return this.ticketSalesInfoMailList;
            }

            set
            {
                if (value.Count == 0)
                {
                    this.ticketSalesInfoMailList.Add(this.Mail);
                }
                else
                {
                    this.ticketSalesInfoMailList = value;
                }
            }
        }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsSubscriber { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String FacebookAccount { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String TwitterAccount { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String WebSite { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String OrganizationSlogan { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public List<UserRoles> userRoles { get; set; }

        public string contactMail { get; set; }
        public List<string> ticketSalesInfoMailList { get; set; }

        public String GetTwitterURL
        {
            get
            {
                return String.Format("http://www.twitter.com/{0}", TwitterAccount);
            }
        }
        public String GetFacebookURL
        {
            get
            {
                return String.Format("http://www.facebook.com/{0}", FacebookAccount);
            }
        }

        public User deserialize(JObject jObject)
        {
            this.UserID = jObject.Value<Int32>("userID");
            this.FirstName = jObject.Value<String>("firstName");
            this.LastName = jObject.Value<String>("lastName");
            this.Mail = jObject.Value<String>("mail");
            this.FacebookID = jObject.Value<String>("facebookID");
            this.Cell = jObject.Value<String>("cell");
            this.CountryCode = jObject.Value<String>("countryCode");
            this.City = jObject.Value<String>("city");
            this.District = jObject.Value<String>("district");
            this.Address = jObject.Value<String>("address");
            this.OrganizationName = jObject.Value<String>("organization");

            string notes = jObject.Value<String>("notes");

            if (!String.IsNullOrEmpty(notes))
            {
                try
                {
                    NotesCreator.Instance.SetValuesToObject(this, notes, NOTES_GROUP_NAME_NOTES);
                }
                catch (Exception e)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                }
            }
            this.Validation = jObject.Value<String>("validation");
            this.Status = jObject.Value<Boolean>("status");
            return this;
        }
        public string FullName
        {
            get
            {
                return String.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        public bool IsActivated
        {
            get
            {
                return this.Status;
            }
        }


       




        public string FacebookProfilePageURL
        {
            get
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}?id={1}", ApplicationConfiguration.FacebookProfileUrl, this.FacebookID);
            }
        }/*
        public string ExternalProfilePageURL
        {
            get
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}{1}?organizerID={2}", ApplicationConfiguration.AppRoot, Url.Action("OrganizerProfile", "Profile"), this.UserID);
            }
        }*/
        public string ProfilePictureUrl
        {
            get
            {

                return String.Format("{0}/User/{1}/profile.png?u={2}", ApplicationConfiguration.ImagesUrlPath, this.UserID, OrganizationPhotoCacheBuster);
            }
        }
        public string ProfilePictureUrlMini
        {
            get
            {

                return String.Format("{0}/User/{1}/profilemini.png?u={2}", ApplicationConfiguration.ImagesUrlPath, this.UserID, OrganizationPhotoCacheBuster);
            }
        }
        public string ProfilePictureUrlOriginal
        {
            get
            {

                return String.Format("{0}/User/{1}/original.png?u={2}", ApplicationConfiguration.ImagesUrlPath, this.UserID, OrganizationPhotoCacheBuster);
            }
        }

        public string OrganizationLogoPictureUrl
        {
            get
            {
                return String.Format("{0}/User/{1}/OriginalLogo.png?u={2}", ApplicationConfiguration.ImagesUrlPath, this.UserID, OrganizationPhotoCacheBuster);
            }
        }
        public string OrganizationBackgroundPictureUrl
        {
            get
            {
                return String.Format("{0}/User/{1}/OriginalBackground.png?u={2}", ApplicationConfiguration.ImagesUrlPath, this.UserID, OrganizationBackgroundCacheBuster);
            }
        }
        public static string ProfilePictureNotFoundUrlMini
        {
            get
            {
                return String.Format("{0}/images/notfound/50_50.jpg", ApplicationConfiguration.ImagesUrlPath);
            }
        }
        public static string ProfilePictureNotFoundUrl
        {
            get
            {
                return String.Format("{0}/images/notfound/180_180.jpg", ApplicationConfiguration.ImagesUrlPath);
            }
        }
        public String OrganizationName
        {
            get
            {
                if ((organizationName != null && string.IsNullOrEmpty(organizationName.Trim())) || String.IsNullOrEmpty(organizationName))
                    return this.FullName;
                else
                    return organizationName;
            }
            set
            {
                organizationName = value;
            }
        }/*
        public String EventListWidget
        {
            get
            {
                UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("<iframe width=\"300\" height=\"450\" target=\"_blank\" src=\"{0}{1}?userID={2}\" frameborder=\"0\" scrolling=\"no\"></iframe>", ApplicationConfiguration.AppRoot, URL.Action("EventList", "Widget"), this.UserID);
            }
        }*/

        public string FacebookPictureURL
        {
            get
            {
                return String.Format("https://graph.facebook.com/{0}/picture?type=large", this.FacebookID);
            }
        }

        public string GeneratePasswordResetCode
        {
            get
            {
                string userResetPasswordKey = Methods.Instance.GenerateRandomAlphanumeric(10);
                string plain = String.Format("{0}#{1}", this.UserID, userResetPasswordKey);
                CryptolographyService service = new CryptolographyService(CryptolographyService.ServiceProviderEnum.Rijndael);
                string code = service.Encrypt(plain);
                this.PasswordResetKey = userResetPasswordKey;
                this.ResetPasswordExpireTime = DateTime.UtcNow.AddMinutes(15);
                return code;
            }
        }

        public void ChangeValidationCode()
        {
            Random rand = new Random();
            this.Validation = rand.Next(100000000).ToString();
        }

        public string ResendActivationHtmlString
        {
            get
            {
                return "";
            }
        }

        public string ActivationLink
        {
            get
            {
                string code = String.Format("{0}#{1}", this.UserID, this.Validation);
                CryptolographyService service = new CryptolographyService(CryptolographyService.ServiceProviderEnum.Rijndael);
                code = service.Encrypt(code);
                UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}{1}?c={2}", ApplicationConfiguration.AppRoot, url.Action("UserActivate", "User"), code);
            }
        }

        public bool HasReservation(Reservation res)
        {
            return this.UserID == res.UserID;
        }



        public void RemoveUserFromOrganization(int userID)
        {
            var obj = this.userRoles.Where(x => x.userID == userID).First();
            this.userRoles.Remove(obj);
            var user = this;
            UserService.Instance.UpdateUser(ref user);
        }

        public void AddUserToOrganization(int userID, Role role, bool status)
        {
            if (!IsUserInOrganization(userID))
            {
                this.userRoles.Add(new UserRoles() { role = role, userID = userID, status = status });
            }
            else
            {
                this.userRoles.Where(x => x.userID == userID).First().status = status;
                this.userRoles.Where(x => x.userID == userID).First().role = role;

            }
            var user = this;
            UserService.Instance.UpdateUser(ref user);
        }

        public bool IsUserInOrganization(int userID)
        {
            return this.userRoles.Where(x => x.userID == userID).Count() > 0;
        }


    }
    public class UserRoles
    {
        public int userID;
        public Role role;
        public bool status;
        public UserRoles()
        {

        }
    }
}
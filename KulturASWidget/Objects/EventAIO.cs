﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KulturASWidget.Common;

namespace KulturASWidget.Objects
{
    public class EventAIO
    {
        public EventAIO()
        {
            this.User = new User();
            this.UserEvent = new Event();
            this.UserVenue = new Venue();
        }

        public Event UserEvent { get; set; }
        public Venue UserVenue { get; set; }
        public User User { get; set; }

        public String EncodeUrlForSEO()
        {
            var urlString = this.User.OrganizationName + "_" + this.UserVenue.Name + "_" + this.UserEvent.Name;
            urlString = urlString.ConverToLowerEnglishChars().RemoveCharsForUrl();
            return HttpUtility.UrlEncode(urlString);

        }
        public EventAIO deserialize(JObject jObject)
        {
            JObject eventObject = jObject.Value<JObject>("event");
            this.UserEvent = new Event().deserialize(eventObject);

            JObject venueObject = jObject.Value<JObject>("venue");
            this.UserVenue = new Venue().deserialize(venueObject);

            JObject userObject = jObject.Value<JObject>("user");
            this.User = new User().deserialize(userObject);

            return this;
        }


        

        [JsonIgnore]
        public bool HasMap
        {
            get { return !this.UserVenue.Latitude.Equals("0") && !this.UserVenue.Longitude.Equals("0"); }
        }

        [JsonIgnore]
        public String FilterClass
        {
            get
            {

                return String.Format("{0} ct{1}", this.UserEvent.FilterClass, this.UserVenue.City.ConverToLowerEnglishChars());
            }
        }

        #region CookieSerializable<EventAIO> Members

        public string Serialize()
        {
            throw new NotImplementedException();
        }

        public EventAIO DeSerialize(JObject jObject)
        {
            return this.deserialize(jObject);
        }

        #endregion

    }
}
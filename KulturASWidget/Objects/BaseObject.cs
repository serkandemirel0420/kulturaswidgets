﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Helpers.NotesHelper;

namespace KulturASWidget.Objects
{
    public abstract class BaseObject
    {
        [Notes(NotesAttribute.EXTRA_DICTIONARY_NOTES_GROUP, NOTES_GROUP_NAME_NOTES)]
        public Dictionary<String, object> NotesExtras { get; set; }

        public const String NOTES_GROUP_NAME_NOTES = "Notes";

        public BaseObject()
        {
            NotesExtras = new Dictionary<string, object>();
        }

        public string NotesJson
        {
            get
            {
                return NotesCreator.Instance.GetJsonString(this, NOTES_GROUP_NAME_NOTES);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using KulturASWidget.Helpers.NotesHelper;
using KulturASWidget.Common;

using System.Globalization;
using KulturASWidget.Models.Marketing;

namespace KulturASWidget.Objects
{
    public class Event : BaseObject
    {

        public Event()
        {
            this.Questions = new List<EventQuestion>();
            this.Tags = new List<string>();
            this.NotesExtras = new Dictionary<string, object>();
        }

        public Int32 EventID { get; set; }
        public string Name { get; set; }
        public Int32 VenueID { get; set; }
        public Int32 UserID { get; set; }
        public DateTime CommenceDate { get; set; }
        public DateTime EndDate { get; set; }
        public String Detail { get; set; }
        public Boolean Status { get; set; }




        [JsonIgnore]
        public String FilterClass
        {
            get
            {
                String cls = "";
                foreach (var item in this.Tags)
                {
                    cls = String.Format("{0} tag{1}", cls, item);
                }

                //cls = String.Format("{0} mnt{1}", cls, CharacterUtils.ConverToLowerEnglishChars(CommenceDate.ToString("MMMM")));
                return cls;
            }
        }
        public List<String> Tags = new List<String>();
        public String TagDisplay
        {
            get
            {
                if (!this.Tags.IsEmpty())
                {
                    return this.Tags.Aggregate((a, b) => String.Format("{0}, {1}", a, b));
                }
                else
                {
                    return String.Empty;
                }

            }
        }

        public String SpecialAreaDisplay
        {
            get
            {

                return String.Format("{0}, {1}, {2}",
                    this.IsPrivate ? "Gizli" : "Gizli değil",
                    this.IsModerationRequired ? "Moderasyon Gerekli" : "Moderasyon Yok",
                    this.Status ? "Aktif" : "Aktif Değil");
            }
        }

        public String TagJSON
        {
            get
            {
                return JsonConvert.SerializeObject(this.Tags);
            }
        }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsPrivate { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsInvisible { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string Password { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public List<EventQuestion> Questions { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String FacebookID { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsApproved { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string VideoLink { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public int ImageCount { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public Boolean SmsAllowedOverride { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public List<MarketingTrackingCodesModel> Tracing { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String EventURL { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsModerationRequired { get; set; }



        public Event deserialize(JObject jObject)
        {
            this.EventID = jObject.Value<Int32>("eventID");
            this.VenueID = jObject.Value<Int32>("venueID");
            this.UserID = jObject.Value<Int32>("userID");
            this.Name = jObject.Value<String>("name");
            this.Detail = jObject.Value<String>("details");
            long sCommence = jObject.Value<long>("commence") / 1000;
            this.CommenceDate = ApplicationConfiguration.FromUnixTime(sCommence);
            this.CommenceDate = TimeZoneInfo.ConvertTimeFromUtc(this.CommenceDate, ApplicationConfiguration.CurrentTimezone);
            long sEnd = jObject.Value<long>("end") / 1000;
            this.EndDate = ApplicationConfiguration.FromUnixTime(sEnd);
            this.EndDate = TimeZoneInfo.ConvertTimeFromUtc(this.EndDate, ApplicationConfiguration.CurrentTimezone);


            string notes = jObject.Value<String>("notes");
            NotesCreator.Instance.SetValuesToObject(this, notes, "Notes");
            var tagStr = jObject.Value<String>("tags");
            tagStr = tagStr.IsEmpty() ? String.Empty : tagStr;
            this.Tags = JsonConvert.DeserializeObject<List<String>>(tagStr);
            this.Status = jObject.Value<Boolean>("status");
            return this;
        }

        [JsonIgnore]
        private string EventImagesPath
        {
            get { return String.Format("{0}/Event/{1}", ApplicationConfiguration.ImagesUrlPath, this.EventID); }
        }

        [JsonIgnore]
        public string LargeBase
        {
            get
            {
                return EventImagesPath + "/Large_{0}.png";
            }
        }
        [JsonIgnore]
        public string Large
        {
            get
            {
                return String.Format(LargeBase, 0);
            }
        }

        [JsonIgnore]
        public string PosterBase
        {
            get
            {
                return EventImagesPath + "/Poster_{0}.png";
            }
        }
        [JsonIgnore]
        public string Poster
        {
            get
            {
                return String.Format(PosterBase, 0);
            }
        }
        [JsonIgnore]
        public string WithoutCropBase
        {
            get
            {
                return EventImagesPath + "/OriginalWithoutCrop_{0}.png";
            }
        }
        [JsonIgnore]
        public string OriginalBase
        {
            get
            {
                return EventImagesPath + "/Original_{0}.png";
            }
        }
        [JsonIgnore]
        public string Original
        {
            get
            {
                return String.Format(OriginalBase, 0);
            }
        }
        [JsonIgnore]
        public string MiddleBase
        {
            get
            {
                return EventImagesPath + "/Middle_{0}.png";
            }
        }
        [JsonIgnore]
        public string Middle
        {
            get { return String.Format(MiddleBase, 0); }
        }
        [JsonIgnore]
        public string SmallBase
        {
            get
            {
                return EventImagesPath + "/Small_{0}.png";
            }
        }
        [JsonIgnore]
        public string Small
        {
            get { return String.Format(SmallBase, 0); }
        }
        [JsonIgnore]
        public static string ProfilePictureNotFoundUrl
        {
            get
            {
                return String.Format("{0}/images/notfound/orta.jpg", ApplicationConfiguration.ImagesUrlPath);

            }
        }
        [JsonIgnore]
        public static string ProfilePictureNotFoundLargeUrl
        {
            get
            {
                return String.Format("{0}/images/notfound/buyuk.jpg", ApplicationConfiguration.ImagesUrlPath);

            }
        }



        [JsonIgnore]
        public static string ProfilePictureNotFoundSmallUrl
        {
            get
            {
                return String.Format("{0}/images/notfound/kucuk.jpg", ApplicationConfiguration.ImagesUrlPath);

            }
        }




        [JsonIgnore]
        public string ReservationCheckURL
        {
            get
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}", Url.Action("ShowCheckReservationPage", "Event", new { @eventID = this.EventID }));
            }
        }

        [JsonIgnore]
        public string ShowSendPageURL
        {
            get
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}?eventID={1}", Url.Action("ShowSendPage", "Event"), this.EventID);
            }
        }

        [JsonIgnore]
        public string ReportPageURL
        {
            get
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}?eventID={1}", Url.Action("Reports", "Account"), this.EventID);
            }
        }


        [JsonIgnore]
        public String FormattedCommence
        {
            get { return String.Format("{0:dd MMM HH:mm}", this.CommenceDate); }
        }

        [JsonIgnore]
        public String FormattedCommenceDate
        {
            get { return this.CommenceDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture); }
        }

        [JsonIgnore]
        public String FormattedCommenceLong
        {
            get { return String.Format("{0:dd MMM yyyy HH:mm}", this.CommenceDate); }
        }
        [JsonIgnore]
        public String FormattedEnd
        {
            get { return String.Format("{0:dd MMM HH:mm}", this.EndDate); }
        }

        [JsonIgnore]
        public String FormattedEndDate
        {
            get { return this.EndDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture); }
        }

        [JsonIgnore]
        public String FormattedEndLong
        {
            get { return String.Format("{0:dd MMM yyyy HH:mm}", this.EndDate); }
        }
        /*
        [JsonIgnore]
        public String EventWidget
        {
            get
            {
                UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("<iframe width=\"300\" height=\"450\" target=\"_blank\" src=\"{0}{1}?eventID={2}\" frameborder=\"0\" scrolling=\"no\"></iframe>", ApplicationConfiguration.Path, URL.Action("EventDetail", "Widget"), this.EventID);
            }
        }

        */





        public String SummaryPageURL
        {
            get
            {
                UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return URL.Action("EventSummary", "Event", new { @id = this.EventID });
            }
        }
    }



}
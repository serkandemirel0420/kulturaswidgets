﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using System.Web.Mvc;
using KulturASWidget.Helpers.NotesHelper;

namespace KulturASWidget.Objects
{

    public class Venue : BaseObject
    {
        public Int32 VenueID { get; set; }
        public Int32 UserID { get; set; }
        public String FoursquareId { get; set; }
        public String Name { get; set; }
        public String Longitude { get; set; }
        public String Latitude { get; set; }
        public String CountryCode { get; set; }
        public String City { get; set; }
        public String District { get; set; }
        public String Address { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public VenueSourceType VenueType { get; set; }

        public String Details { get; set; }

        public Boolean Status { get; set; }

        [JsonIgnore]
        public String CitySearchLink
        {
            get
            {
                UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return URL.Action("SearchByCity", "Search", new { @city = this.City });
            }
        }

        public Venue deserialize(Newtonsoft.Json.Linq.JObject jObject)
        {
            this.VenueID = jObject.Value<Int32>("venueID");
            this.UserID = jObject.Value<Int32>("userID");
            this.Name = jObject.Value<String>("name");
            this.Longitude = jObject.Value<Double>("longitude").ToString().Replace(',', '.');
            this.Latitude = jObject.Value<Double>("latitude").ToString().Replace(',', '.');
            this.CountryCode = jObject.Value<String>("countryCode");
            this.City = jObject.Value<String>("city");
            if (!this.City.IsEmpty() && this.City.Contains(','))
            {
                this.City = this.City.Split(',')[0];
            }
            else if(this.City.IsEmpty())
            {
                this.City = String.Empty;
            }

            this.District = jObject.Value<String>("district");
            this.Address = jObject.Value<String>("address");
            this.Details = jObject.Value<String>("details");
            this.Status = jObject.Value<Boolean>("status");
            string notes = jObject.Value<String>("notes");

            NotesCreator.Instance.SetValuesToObject(this, notes, NOTES_GROUP_NAME_NOTES);

            return this;


        }
    }
}
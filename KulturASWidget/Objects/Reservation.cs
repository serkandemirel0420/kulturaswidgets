﻿using System;
using System.Collections.Generic;
using System.Linq;
using KulturASWidget.Helpers.NotesHelper;
using KulturASWidget.Common;
using System.Web;


namespace KulturASWidget.Objects
{
    public class Reservation : BaseObject
    {

        public Reservation()
        {
            this.QuestionAnswers = new Dictionary<string, string>();
            this.PaymentExtras = new Dictionary<string, object>();
        }

        public int ReservationID { get; set; }
        public int UserID { get; set; }
        public int LayoutID { get; set; }
        public int EventID { get; set; }
        public int PaymentID { get; set; }
        public string VerificationID { get; set; }
        public string SmsID { get; set; }
        public string ReservationQueue { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime CreationDate { get; set; }
        public string VerificationPNR { get; set; }
        public bool Status { get; set; }
        public bool CheckedIn
        {
            get
            {
                return this.EntryDate > new DateTime(2012, 01, 01);

            }
        }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string TrackingCode { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string AttendeeName { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string AttendeeEmail { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string AttendeePhone { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string DiscountCode { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string DiscountAmount { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string DiscountName { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public string ReservationType { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsReservationDeclined { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public String SendChoice { get; set; }
        [Notes(NOTES_GROUP_NAME_NOTES)]
        public bool IsReservationApproved { get; set; }
        public String PdfLink
        {
            get
            {
                return String.Format("{0}/reservation/{1}/pdfTicket?applicationKey={2}", ApplicationConfiguration.RestServerUrl, this.ReservationID, ApplicationConfiguration.AppKey);
            }
        }


        public string CreditCardTransactionID { get; set; }
        public string CreditCardAuthorizationID { get; set; }
        public string CreditCardTransactionAuthCode { get; set; }

        [Notes(NOTES_GROUP_NAME_PAYMENT)]
        public string CreditCardTransactionBatchNumber { get; set; }
        [Notes(NOTES_GROUP_NAME_PAYMENT)]
        public string CreditCardTransactionSquenceNumber { get; set; }
        [Notes(NOTES_GROUP_NAME_PAYMENT)]
        public string CreditCardTransactionProvDate { get; set; }
        [Notes(NOTES_GROUP_NAME_PAYMENT)]
        public string CreditCardOrderID { get; set; }

        [Notes(NOTES_GROUP_NAME_NOTES)]
        [Notes(NOTES_GROUP_NAME_PAYMENT)]
        public Dictionary<String, String> QuestionAnswers { get; set; }

        [Notes(NotesAttribute.EXTRA_DICTIONARY_NOTES_GROUP, NOTES_GROUP_NAME_PAYMENT)]
        public Dictionary<String, object> PaymentExtras { get; set; }

        private const String NOTES_GROUP_NAME_PAYMENT = "Payment";

        public string PaymentNotesJson()
        {
            return NotesCreator.Instance.GetJsonString(this, NOTES_GROUP_NAME_PAYMENT);
        }

        public Reservation deserialize(Newtonsoft.Json.Linq.JObject jObject)
        {
            this.ReservationID = jObject.Value<Int32>("reservationID");
            this.UserID = jObject.Value<Int32>("userID");
            this.LayoutID = jObject.Value<Int32>("layoutID");
            this.PaymentID = jObject.Value<Int32>("paymentID");
            this.EventID = jObject.Value<Int32>("eventID");
            this.ReservationQueue = jObject.Value<String>("queueID");
            this.VerificationID = jObject.Value<string>("verificationID");
            this.SmsID = jObject.Value<string>("smsID");
            this.VerificationPNR = jObject.Value<string>("verificationPNR");
            long entryDate = jObject.Value<long>("entryDate") / 1000;
            this.EntryDate = ApplicationConfiguration.FromUnixTime(entryDate);
            long creationDate = jObject.Value<long>("creationDate") / 1000;
            this.CreationDate = ApplicationConfiguration.FromUnixTime(creationDate);
            this.Status = jObject.Value<Boolean>("status");

            string notes = jObject.Value<string>("notes");
            NotesCreator.Instance.SetValuesToObject(this, notes, NOTES_GROUP_NAME_NOTES);

            if (this.QuestionAnswers.Count > 0)
            {
                string s = "";
                s.Trim();
            }
            return this;
        }


        public void AddAnswer(String question, String answer)
        {
            var answerValues = (from answers in QuestionAnswers where answers.Key == question select answers.Value);
            if (answerValues.Count() != 0)
            {
                QuestionAnswers.Remove(question);
            }
            QuestionAnswers.Add(question, answer);
        }

        public String GetAnswer(String question)
        {
            if (this.QuestionAnswers.ContainsKey(question))
            {
                return this.QuestionAnswers[question];
            }
            else
            {
                return String.Empty;
            }
        }

        public bool IsSmsAvaliable
        {
            get
            {
                var phone = this.AttendeePhone.ClearWhiteSpaces();
                return !phone.IsEmpty() && phone.Length > 8  && phone.IsAllDigits();
            }

        }

    }
}
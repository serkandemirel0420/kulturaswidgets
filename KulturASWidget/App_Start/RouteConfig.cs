﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace KulturASWidget
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Main",
                url: "",
                defaults: new { controller = "Home", action = "MainWidget" }
            );

            routes.MapRoute(
                name: "Payment",
                url: "Payment",
                defaults: new { controller = "Home", action = "PaymentWidget" }
            ); 
            
            routes.MapRoute(
                 name: "Message",
                 url: "Message",
                 defaults: new { controller = "Home", action = "MessageWidget" }
             );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "MainWidget", id = UrlParameter.Optional }
            );
        }
    }
}
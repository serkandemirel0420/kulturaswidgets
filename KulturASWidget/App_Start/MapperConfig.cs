﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using KulturASWidget.Models.Marketing;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using System.Globalization;

namespace KulturASWidget
{
    public class MapperConfig
    {
        public static void Init()
        {
            Mapper.CreateMap<MarketingDiscountsModel, MarketingDiscountsPartialViewModel>();
            Mapper.CreateMap<MarketingTrackingCodesModel, MarketingTrackingListPartialViewModel>();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services;
using KulturASWidget.Common;
using KulturASWidget.Objects;
using System.Threading;

namespace KulturASWidget.Models.Widget
{

    public class Events
    {
        public String EventName { get; set; }
        public String EventDetail { get; set; }
        public String EventDate { get; set; }
        public String EventAddress { get; set; }
        public String EventURL { get; set; }
        public String EventDay { get; set; }
        public String EventMonth { get; set; }
        public EventAIO EventAIO { get; set; }
    }
    public class EventListWidgetModel
    {
        private int organizorID;
        public List<Events> eventList;
        public EventAIO EventAIO { get; set; }
        public EventListWidgetModel(int organizorID)
        {
   
            this.organizorID = organizorID;
            var eventList = EventService.Instance.GetByUserID(organizorID);
            List<Events> temp = new List<Events>();

            foreach (var eventAIO in eventList)
            {
                var eventObject = new Events();
                eventObject.EventName = eventAIO.UserEvent.Name;
                eventObject.EventDetail = eventAIO.UserEvent.Detail;
                eventObject.EventDate = eventAIO.UserEvent.CommenceDate.ToString();
                eventObject.EventDay = eventAIO.UserEvent.CommenceDate.Day.ToString();
                eventObject.EventMonth = eventAIO.UserEvent.CommenceDate.ToString("MMMM", Thread.CurrentThread.CurrentUICulture);
                eventObject.EventAddress = eventAIO.UserVenue.Address;
  
                eventObject.EventAIO = eventAIO;
                temp.Add(eventObject);
            }
            this.EventAIO = eventList.First();
            this.eventList = temp;
        }

        public EventListWidgetModel()
        {
            // TODO: Complete member initialization
        }

    }
}
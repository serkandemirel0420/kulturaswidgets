﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KulturASWidget.Services;
using KulturASWidget.Common;
using KulturASWidget.Objects;
using System.Globalization;
using System.Threading;

namespace KulturASWidget.Models.Widget
{
    public class EventWidgetModel
    {
        public String EventPictureURL { get; set; }
        public String EventName { get; set; }
        public String EventDate { get; set; }
        public String EventAdress { get; set; }
        public String EventMonth { get; set; }
        public String EventDay { get; set; }

        public Objects.EventAIO EventAIO { get; set; }

        public String trackingCode { get; set; }

        public EventWidgetModel()
        {
            this.EventAIO = new EventAIO();
        }

        public EventWidgetModel(EventAIO eventObj)
        {
            this.EventAIO = eventObj;

            this.EventPictureURL = eventObj.UserEvent.Poster;
            this.EventName = eventObj.UserEvent.Name;
            this.EventDate = eventObj.UserEvent.CommenceDate.ToString();
            this.EventAdress = eventObj.UserVenue.Address;
            this.EventMonth = eventObj.UserEvent.CommenceDate.ToString("MMMM", Thread.CurrentThread.CurrentUICulture);
            this.EventDay = eventObj.UserEvent.CommenceDate.Day.ToString();
           
        }

        
    }
}

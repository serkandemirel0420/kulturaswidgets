﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services;
using System.ComponentModel.DataAnnotations;
using KulturASWidget.Common.ValidationAttributes;

namespace KulturASWidget.Models.Widget
{
    public class EventLayout
    {
        public String LayoutName { get; set; }
        public String LayoutPrice { get; set; }
        public String LayoutMaxTicketCount { get; set; }
        public String LayoutId { get; set; }
        public String TicketFeeStr { get; set; }
        public DateTime StartSaleDate { get; set; }
        public DateTime EndSaleDate { get; set; }
    }
    public class EventTicketDetailModel
    {
        private int eventIDRef;

        public String EventName { get; set; }
        public String EventMonth { get; set; }
        public String EventDay { get; set; }
        public String EventDate { get; set; }
        public String EventAdress { get; set; }
        public DateTime CommenceDate { get; set; }
        public DateTime EndDate { get; set; }
        [Required(ErrorMessageResourceName = "UserFirstNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserFirstNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Display(Name = "UserFirstName", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeName { get; set; }

        [Required(ErrorMessageResourceName = "UserMailRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckMailFormat(ErrorMessageResourceName = "EmailValidation", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserMail", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeMail { get; set; }


        [MinLength(10, ErrorMessageResourceName = "UserCellMinLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(20, ErrorMessageResourceName = "UserCellMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsCellAcceptable(ErrorMessageResourceName = "IsCellAcceptable", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "UserCell", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeGSM { get; set; }

        public List<EventLayout> EventLayouts { get; set; }
        [Required]
        [Range(1,100)]
        public int ticketCount { get; set; }

        public bool conditionsAgree { get; set; }


        public EventTicketDetailModel(int eventIDRef)
        {
            // TODO: Complete member initialization
            this.eventIDRef = eventIDRef;

            var eventAIO = EventService.Instance.GetByID(eventIDRef);

            this.EventName = eventAIO.UserEvent.Name;
            this.EventMonth = getMonth(eventAIO.UserEvent.CommenceDate.Month);
            this.EventDay = eventAIO.UserEvent.CommenceDate.Day.ToString();
            this.EventDate = eventAIO.UserEvent.CommenceDate.ToString();
            this.EventAdress = eventAIO.UserVenue.Address;
            this.CommenceDate = eventAIO.UserEvent.CommenceDate;
            this.EndDate = eventAIO.UserEvent.EndDate;
            var eventLayoutList = LayoutService.Instance.GetByEvent(eventAIO.UserEvent.EventID);
            List<EventLayout> temp = new List<EventLayout>();


            foreach (var layout in eventLayoutList)
            {
                layout.PriceWithoutCommission = 1000;
                layout.commissionPayer = CommissionPayer.byOrganization;
                EventLayout eventLayout = new EventLayout();
                if (!layout.IsBoothTicket)
                {
                    eventLayout.LayoutName = layout.Name;
                    eventLayout.LayoutPrice = ((double)layout.TotalPrice / 100).ToString();
                    eventLayout.LayoutMaxTicketCount = layout.MaxTicket.ToString();
                    eventLayout.LayoutId = layout.LayoutID.ToString();
                    eventLayout.TicketFeeStr = layout.TotalPrice.ToString();
                    eventLayout.StartSaleDate = layout.SaleStartDate;
                    eventLayout.EndSaleDate = layout.SaleEndDate;
                  
                    temp.Add(eventLayout);
                }

            }

            this.EventLayouts = temp;
        }
        public EventTicketDetailModel()
        {
        }
        private string getMonth(int month)
        {
            string str = "";

            switch (month)
            {
                case 1:
                    str = "Ocak";
                    break;
                case 2:
                    str = "Şubat";
                    break;

                case 3:
                    str = "Mart";
                    break;

                case 4:
                    str = "Nisan";
                    break;

                case 5:
                    str = "Mayıs";
                    break;
                case 6:
                    str = "Haziran";
                    break;
                case 7:
                    str = "Temmuz";
                    break;
                case 8:
                    str = "Ağustos";
                    break;
                case 9:
                    str = "Eylül";
                    break;

                case 10:
                    str = "Ekim";
                    break;

                case 11:
                    str = "Kasım";
                    break;

                case 12:
                    str = "Aralık";
                    break;
            }
            return str;
        }
    }
}
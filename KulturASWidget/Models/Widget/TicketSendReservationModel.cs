﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services;
using System.ComponentModel.DataAnnotations;
using KulturASWidget.Common.ValidationAttributes;

namespace KulturASWidget.Models.Widget
{
    public enum TicketSendType
    {
        email,
        sms,
        passbook
    }

    public class TicketUserInfoModel
    {
        public TicketUserInfoModel() { }
        public TicketUserInfoModel(int userID, int layoutID, int count)
        {
            var reservationList = ReservationService.Instance.QueueGetUserQueue(userID);
            var layout = LayoutService.Instance.GetByID(layoutID);
            var eventAIO = EventService.Instance.GetByID(layout.EventID);


            this.EventName = eventAIO.UserEvent.Name;
            this.EventDate = eventAIO.UserEvent.CommenceDate.ToString();
            this.EventAddress = eventAIO.UserVenue.Address;

            this.TicketName = layout.Name;
            this.TicketPrice = layout.TotalPrice.ToString();
            this.TicketCount = count.ToString();
            this.TicketPriceSum = (layout.TotalPrice * count).ToString();
        }
        public String EventName { get; set; }
        public String EventDay { get; set; }
        public String EventMonth { get; set; }
        public String EventDate { get; set; }
        public String EventAddress { get; set; }

        public String TicketName { get; set; }
        public String TicketPrice { get; set; }
        public String TicketCount { get; set; }
        public String TicketPriceSum { get; set; }

        [Required(ErrorMessageResourceName = "UserFirstNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserFirstNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Display(Name = "UserFirstName", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeName { get; set; }

        [Required(ErrorMessageResourceName = "UserMailRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckMailFormat(ErrorMessageResourceName = "EmailValidation", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "UserMail", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeMail { get; set; }

        [MinLength(10, ErrorMessageResourceName = "UserCellMinLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(20, ErrorMessageResourceName = "UserCellMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsCellAcceptable(ErrorMessageResourceName = "IsCellAcceptable", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "UserCell", ResourceType = typeof(Resources.PageResources))]
        public String AttendeeGSM { get; set; }

        public TicketSendType sendType { get; set; }

    }
}
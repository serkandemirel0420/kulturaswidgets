﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Models.Marketing
{
    public class MarketingTrackingListPartialViewModel
    {
        private int userID;

        public MarketingTrackingListPartialViewModel(int pUserID)
        {
            this.userID = pUserID;
        }
        public MarketingTrackingListPartialViewModel() { }

        [MaxLength(16, ErrorMessageResourceName = "TrackingTracingCodeMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "TrackingRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String tracingCode { get; set; }

        //[Required(ErrorMessageResourceName = "TrackingExplanationRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String explanation { get; set; }

        public String traceURL { get; set; }

        public String eventName { get; set; }

        [Required(ErrorMessageResourceName = "TrackingEventIDRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public int eventID { get; set; }

        public int visitCount { get; set; }

        public String totalSales { get; set; }
        public int totalSalesCount { get; set; }

        public bool status { get; set; }
        public List<MarketingTrackingListPartialViewModel> getUserTracingCodes()
        {
            List<MarketingTrackingListPartialViewModel> userTracingCodesList = new List<MarketingTrackingListPartialViewModel>();

            //userın tüm eventlerini al
            var userEvents = EventService.Instance.GetByUserID(userID);
            //tracing code olanları bul
            foreach (var userEvent in userEvents)
            {
                if (userEvent.UserEvent.Tracing.IsExists())
                {
                    if (userEvent.UserEvent.Tracing.Count > 0)
                    {
                        var tracingCodeList = userEvent.UserEvent.Tracing;

                        // listedekileri view'ın modeline map et

                        foreach (var tracingCode in tracingCodeList)
                        {
                            var eventReservationList = ReservationService.Instance.GetByEvent(userEvent.UserEvent.EventID);

                            int salesCount = 0;
                            double totalRevenue = 0;
                            foreach (var reservation in eventReservationList)
                            {
                                if (reservation.TrackingCode == tracingCode.tracingCode)
                                {
                                    salesCount++;
                                    var layout = LayoutService.Instance.GetByID(reservation.LayoutID);
                                    totalRevenue += ((double)layout.TotalPrice / 100);
                                }


                            }
                            tracingCode.evAIO = userEvent;
                            var userTracingView = AutoMapper.Mapper.Map<MarketingTrackingCodesModel, MarketingTrackingListPartialViewModel>(tracingCode);
                            userTracingView.totalSalesCount = salesCount;
                            userTracingView.totalSales = String.Format("{0} TL", totalRevenue);
                            userTracingView.eventName = userEvent.UserEvent.Name;
                            userTracingCodesList.Add(userTracingView);
                        }
                    }
                }
            }

            //listeye ekle
            return userTracingCodesList;
        }
    }
}
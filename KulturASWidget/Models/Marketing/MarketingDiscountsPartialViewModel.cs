﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services;
using KulturASWidget.Objects;
using AutoMapper;

namespace KulturASWidget.Models.Marketing
{
    public class MarketingDiscountsPartialViewModel
    {
        public int userID { get; set; }

        public MarketingDiscountsPartialViewModel()
        {

        }
        public MarketingDiscountsPartialViewModel(int pUserID)
        {
            this.userID = pUserID;
        }

        public string code { get; set; }
        public string information { get; set; }
        public string calculationValue { get; set; }
        public string discountLimit { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int layoutID { get; set; }
        public bool status { get; set; }
        public int appliedDiscountNumber { get; set; }
        public double totalDiscountAmount { get; set; }
        public DiscountCalculationType type { get; set; }
        public List<MarketingDiscountsPartialViewModel> getUserDiscounts()
        {
            //kullanıcının eventlerini bul
            var eventAIOList = EventService.Instance.GetByUserID(this.userID, true);
            //eventlerin layoutlarını bul
            List<Layout> layoutList = new List<Layout>();
            foreach (var eventAIO in eventAIOList)
            {
                var layouts = LayoutService.Instance.GetByEvent(eventAIO.UserEvent.EventID);
                foreach (var layout in layouts)
                {
                    layoutList.Add(layout);
                }
            }
            //layoutlardan indirimli olanları bul
            List<MarketingDiscountsPartialViewModel> userDiscountsPartialModel = new List<MarketingDiscountsPartialViewModel>();

            foreach (var layout in layoutList)
            {
                if (layout.Discounts.IsExists())
                {
                    foreach (var discount in layout.Discounts)
                    {
                        var discountPartialModel = Mapper.Map<MarketingDiscountsPartialViewModel>(discount);

                        discountPartialModel.layoutID = layout.LayoutID;

                        discountPartialModel.startDate = layout.SaleStartDate.ToString("d");
                        discountPartialModel.endDate = layout.SaleEndDate.ToString("d");

                        discountPartialModel.totalDiscountAmount = 0;
                        discountPartialModel.appliedDiscountNumber = 0;
                       
                        var reservationList = ReservationService.Instance.GetByLayout(layout.LayoutID);

                        foreach (var reservationObj in reservationList)
                        {
                            if (reservationObj.DiscountCode == discount.code)
                            {
                                discountPartialModel.appliedDiscountNumber++;
                                discountPartialModel.totalDiscountAmount += Convert.ToDouble(reservationObj.DiscountAmount);
                            }
                        }
                        discountPartialModel.totalDiscountAmount = discountPartialModel.totalDiscountAmount;
                        userDiscountsPartialModel.Add(discountPartialModel);
                    }
                }
            }

            return userDiscountsPartialModel;
        }
    }
}
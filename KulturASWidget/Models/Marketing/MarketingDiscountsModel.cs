﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.WebPages.Html;
using KulturASWidget.Objects;
using Newtonsoft.Json;
using KulturASWidget.Services;

namespace KulturASWidget.Models.Marketing
{
    public enum DiscountLimits
    {
        unlimited,                          //      Limitsiz
        limitNumberOfTickets,               //      Her bir kullanıcının yararlanabileceği maksimum indirimli bilet sayısı 
        limitNumberOfTotalTickets,          //      İndirim uygulayacağınız toplam bilet sayısı

    }
    public enum DiscountCalculationType
    {
        byRate,
        byAmount
    }

    public class MarketingDiscountsModel
    {
        [JsonIgnore]
        public int CurrentUserOrganizationID { get; set; }
        [JsonIgnore]
        public String LayoutDates { get; set; }

        public String selectedEvent { get; set; }

        public int discountCreatedByUserID { get; set; }

        public DateTime discountCreateDate { get; set; }

        [Required(ErrorMessageResourceName = "DiscountLayoutIDRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String layoutID { get; set; }

        // indirimlerden herkes faydalanabilir mi?
        public bool IsAvailableToEveryone { get; set; }

        // indirim uygulanabilecek domain isimleri
        public String discountDomains { get; set; }
        public List<string> discountDomainsList { get; set; }

        // indirim uygulanabilmesi için kullanılacak kod
        //[MaxLength(16, ErrorMessageResourceName = "DiscountCodeMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        //[Required(ErrorMessageResourceName = "DiscountCodeRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String code { get; set; }

        // indirim hakkında bilgi
        [Required(ErrorMessageResourceName = "DiscountInformationRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String information { get; set; }

        // toplam tutar üzerinden oran olarak mı düşülecek? yoksa miktar mı?
        public DiscountCalculationType type { get; set; }

        // oran ya da miktar
        [Range(1.0, 10000, ErrorMessageResourceName = "DiscountCalculationValueRange", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "DiscountCalculationValueRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public double calculationValue { get; set; }

        // indirimin etkinleşeceği tarih
        [Required(ErrorMessageResourceName = "DiscountStartDateRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public DateTime startDate { get; set; }

        //İndirim aktif mi değil mi 
        public bool status { get; set; }

        [Required(ErrorMessageResourceName = "DiscountEndDateRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public DateTime endDate { get; set; }

        public DiscountLimits discountLimit { get; set; }

        public String numberOfTickets { get; set; }

        public List<SelectListItem> eventsDDL()
        {
            List<EventAIO> userEvents = EventService.Instance.GetByUserID(CurrentUserOrganizationID, true);
            List<SelectListItem> ddlEventNames = new List<SelectListItem>();

            foreach (var eventObj in userEvents)
            {
                bool IsFree = true;
                List<Layout> eventLayouts = LayoutService.Instance.GetByEvent(eventObj.UserEvent.EventID);

                foreach (var layout in eventLayouts)
                {
                    if (layout.TotalPrice > 0)
                    {
                        IsFree = false;
                    }
                }

                if (!IsFree)
                {
                    ddlEventNames.Add(new SelectListItem() { Text = eventObj.UserEvent.Name, Value = eventObj.UserEvent.EventID.ToString() });
                }
            }

            return ddlEventNames;
        }
        public List<SelectListItem> layoutsDDL()
        {
            List<EventAIO> userEvents = KulturASWidget.Services.EventService.Instance.GetByUserID(CurrentUserOrganizationID, false);
            List<SelectListItem> ddlLayoutNames = new List<SelectListItem>();

            if (userEvents.Count > 0)
            {
                List<Layout> userLayouts = KulturASWidget.Services.LayoutService.Instance.GetByEvent(userEvents.First().UserEvent.EventID);
                bool IsFree = true;
                foreach (var layout in userLayouts)
                {
                    if (layout.TotalPrice > 0)
                    {
                        ddlLayoutNames.Add(new SelectListItem() { Text = layout.Name, Value = layout.LayoutID.ToString() });
                    }
                }
                if (!IsFree)
                {
                    return ddlLayoutNames;
                }
            }

            return (ddlLayoutNames);
        }

        public MarketingDiscountsModel(int pOrganizationID)
        {
            this.CurrentUserOrganizationID = pOrganizationID;

        }
        public MarketingDiscountsModel() { }


        public MarketingDiscountsModel findLayoutAndValidate(int layoutID, string code)
        {
            //layoutId nin organizasyonun eventlerine ait olup olmadığına bak

            //Kullanıcının tüm etkinliklerini çek
            var eventAIOList = EventService.Instance.GetByUserID(this.CurrentUserOrganizationID);

            //Eşleşen layout varsa o layoutun içindeki indirimlerle kodu uyuşan varsa döndür
            foreach (var eventAIO in eventAIOList)
            {
                //event in layoutlarını bul
                var eventLayoutList = LayoutService.Instance.GetByEvent(eventAIO.UserEvent.EventID);
                //layoutIDsi eşlenenin içindeki kodlarla uyuşan modeli döndür
                foreach (var layout in eventLayoutList)
                {
                    //ilgili layout bulunduysa
                    if (layout.LayoutID == layoutID)
                    {
                        this.LayoutDates = String.Format("{0}-{1}", layout.SaleStartDate, layout.SaleEndDate);

                        //discoutnlar içinde uyuşan kod varsa döndür
                        var discountList = layout.Discounts;
                        foreach (var discount in discountList)
                        {
                            if (discount.code == code)
                            {
                                discount.CurrentUserOrganizationID = this.CurrentUserOrganizationID;
                                return discount;
                            }
                        }
                    }
                }
            }
            return this;
        }

        public void addToDiscounts()
        {
            var layout = LayoutService.Instance.GetByID(Convert.ToInt32(this.layoutID));
            var layoutDiscounts = layout.Discounts;
            if (!layoutDiscounts.IsExists())
            {
                layoutDiscounts = new List<MarketingDiscountsModel>();
            }
            //eğer update edilecekse layoutDiscounts içinden kod'a göre ara varsa onu update et
            bool IsUpdateHappened = false;

            for (int i = 0; i < layoutDiscounts.Count; i++)
            {
                var layDisc = layoutDiscounts.ElementAt(i);
                if (layDisc.code == this.code)
                {
                    layDisc = this;
                    IsUpdateHappened = true;
                }
            }

            //update edilmeyecekse yeni bir indirimdir
            if (!IsUpdateHappened)
            {
                layoutDiscounts.Add(this);
                layout.Discounts = layoutDiscounts;
            }
            //discount update ya da create. her iki durumda da update et layout u
            layout = KulturASWidget.Services.LayoutService.Instance.UpdateLayout(layout);
        }





        public List<string> validate()
        {
            List<string> errorList = new List<string>();

            bool dateValidation = this.dateIsValid();
            bool priceValidation = this.priceIsValid();
            bool dateForEventValidation = this.dateIsValidForEvent();
            bool codeValidation = this.codeIsValid();
            bool domainValidation = this.domainIsValid();

            if (!dateValidation)
            {
                errorList.Add("İndirim bitiş tarihi, başlangıç tarihinden önce olmamalıdır!");
            }

            if (!priceValidation)
            {
                errorList.Add("İndirim miktarı, bilet fiyatından fazla olmamalıdır!");
            }

            if (!dateForEventValidation)
            {
                var layout = LayoutService.Instance.GetByID(Convert.ToInt32(this.layoutID));
                String errorMessage = String.Format("İndirimin {0} ile {1} arasında geçerli olabilir!.", layout.SaleStartDate, layout.SaleEndDate);
                errorList.Add(errorMessage);
            }

            if (!codeValidation)
            {
                errorList.Add("Bu kod ilgili bilet için daha önce tanımlanmış!");
            }

            if (!domainValidation)
            {
                errorList.Add("Yanlış domain adı girildi!");
            }

            return errorList;
        }

        private bool domainIsValid()
        {
            if (!this.IsAvailableToEveryone)
            {
                String domains = this.discountDomains;
                List<String> domainList = domains.Split(',').ToList();
                this.discountDomainsList = domainList;
            }
            return true;
        }

        public bool dateIsValid()
        {
            //başlangıç tarihi bitiş tarihinden önce olmalıdır. önce ise true değilse false döndür
            DateTime discountStartDate = this.startDate;
            DateTime discountEndDate = this.endDate;

            if (discountStartDate <= discountEndDate)
            {
                return true;
            }
            return false;
        }
        public bool priceIsValid()
        {
            //eğer indirim uygulandıktan sonra biletin fiyatı 0 un üstünde ise valid döndür
            var layout = LayoutService.Instance.GetByID(Convert.ToInt32(this.layoutID));
            if (this.type == DiscountCalculationType.byAmount)
            {
                if ((layout.TotalPrice - layout.Commission - (this.calculationValue * 100)) < 0)
                {
                    return false;
                }
            }
            else
            {
                double priceAfterDiscount = ((100 - this.calculationValue) * (layout.TotalPrice - layout.Commission)) / 100;
                if (priceAfterDiscount < layout.Commission)
                {
                    return false;
                }
            }

            return true;
        }
        public bool dateIsValidForEvent()
        {
            //  etkinlik başlangıç tarihinden önce başlamamlıdır ve bitiş tarihinden soonra bitmemelidir
            //  layoutID ile layout'u çek
            var layout = LayoutService.Instance.GetByID(Convert.ToInt32(this.layoutID));
            //  layout.eventID ile event'i çek
            //var eventAIO = EventService.Instance.GetByID(layout.EventID);

            //  başlangıç ve bitiş tarihlerinin event'in içinde olduğunu kontrol et
            DateTime layoutStartDate = layout.SaleStartDate;
            DateTime layoutEndDate = layout.SaleEndDate;

            DateTime discountStartDate = this.startDate;
            DateTime discountEndDate = this.endDate;


            if (discountStartDate <= discountEndDate &&
                layoutStartDate <= discountStartDate &&
                discountStartDate <= layoutEndDate)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public bool codeIsValid()
        {
            // layout id ile discountları çek
            var layout = LayoutService.Instance.GetByID(Convert.ToInt32(this.layoutID));
            // o code ile bir kod varsa, valid değildir de false de gönder
            var discountList = layout.Discounts;
            if (discountList.IsExists())
            {
                foreach (var discount in discountList)
                {
                    if (discount.code == this.code)
                    {
                        return false;
                    }
                }
            }
            // eğer bulunamazsa true döndür
            return true;
        }
    }
}
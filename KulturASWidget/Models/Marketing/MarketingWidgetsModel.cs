﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using System.Web.Mvc;
using KulturASWidget.Services;

namespace KulturASWidget.Models.Marketing
{
    public class MarketingWidgetsModel
    {
        [JsonIgnore]
        public Objects.User CurrentOrganization { get; set; }


        [JsonIgnore]
        public Objects.EventAIO userEvent { get; set; }


        public String selectedEvent { get; set; }
        public String trackingCode { get; set; }

        public List<System.Web.Mvc.SelectListItem> EventsDropDownList
        {
            get
            {
                List<EventAIO> userEvents = KulturASWidget.Services.EventService.Instance.GetByUserID(CurrentOrganization.UserID, true);
                List<System.Web.Mvc.SelectListItem> ddlEventNames = new List<System.Web.Mvc.SelectListItem>();

                foreach (var eventObj in userEvents)
                {
                    ddlEventNames.Add(new SelectListItem() { Text = eventObj.UserEvent.Name, Value = eventObj.UserEvent.EventURL });
                }

                return ddlEventNames;
            }
        }


        public List<System.Web.Mvc.SelectListItem> TrackingCodesDropDownList
        {
            get
            {
                List<EventAIO> userEvents = KulturASWidget.Services.EventService.Instance.GetByUserID(CurrentOrganization.UserID, false);
                List<System.Web.Mvc.SelectListItem> ddlTrackingCodes = new List<System.Web.Mvc.SelectListItem>();
                EventAIO eventAIO = userEvents.First();

                if (eventAIO.IsExists())
                {
                    var trackingCodeList = eventAIO.UserEvent.Tracing;

                    if (trackingCodeList.IsExists())
                    {
                        ddlTrackingCodes.Add(new SelectListItem() { Text = "-Seçiniz-", Value = "" });
                        foreach (var trackingCodeObj in trackingCodeList)
                        {
                            ddlTrackingCodes.Add(new SelectListItem() { Text = trackingCodeObj.tracingCode, Value = trackingCodeObj.tracingCode });
                        }
                    }
                }

                return ddlTrackingCodes;
            }
        }

        public MarketingWidgetsModel() { }

        public MarketingWidgetsModel(Objects.User currentOrganization)
        {
            this.CurrentOrganization = currentOrganization;
            List<EventAIO> userEvents = KulturASWidget.Services.EventService.Instance.GetByUserID(CurrentOrganization.UserID, true);
            if (!userEvents.IsEmpty())
            {
                this.userEvent = userEvents.First();
            }
        }

        public MarketingWidgetsModel(Objects.User currentOrganization,Objects.EventAIO userEvent)
        {
            this.CurrentOrganization = currentOrganization;
            this.userEvent = userEvent;
        }


       



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.WebPages.Html;
using KulturASWidget.Objects;
using Newtonsoft.Json;
using KulturASWidget.Services;
using KulturASWidget.Common;

namespace KulturASWidget.Models.Marketing
{
    public class MarketingTrackingCodesModel
    {
        [JsonIgnore]
        public int CurrentUserOrganizationID { get; set; }

        [MaxLength(16, ErrorMessageResourceName = "TrackingTracingCodeMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "TrackingRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String tracingCode { get; set; }
        //[Required(ErrorMessageResourceName = "DiscountLayoutIDRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String explanation { get; set; }

        public EventAIO evAIO { get; set; }

        public int totalSalesCount { get; set; }
        

        [Required(ErrorMessageResourceName = "TrackingEventIDRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public int eventID { get; set; }
        public int visitCount { get; set; }
        public bool status { get; set; }

        public List<SelectListItem> eventsDDL()
        {
            List<EventAIO> userEvents = KulturASWidget.Services.EventService.Instance.GetByUserID(CurrentUserOrganizationID, true);
            List<SelectListItem> ddlEventNames = new List<SelectListItem>();

            foreach (var eventObj in userEvents)
            {
                ddlEventNames.Add(new SelectListItem() { Text = eventObj.UserEvent.Name, Value = eventObj.UserEvent.EventID.ToString() });
            }

            return ddlEventNames;
        }

        public MarketingTrackingCodesModel() { }
        public MarketingTrackingCodesModel(int pUserID)
        {
            this.CurrentUserOrganizationID = pUserID;
            this.visitCount = 0;
           
        }

        public MarketingTrackingCodesModel findTrackingObjAndValidate(int eventID, string trackingCode)
        {
            var eventObj = EventService.Instance.GetByID(eventID);
            var trackingList = eventObj.UserEvent.Tracing;

            foreach (var trackingObj in trackingList)
            {
                if (trackingObj.tracingCode == trackingCode)
                {

                    trackingObj.CurrentUserOrganizationID = this.CurrentUserOrganizationID;
                    trackingObj.evAIO = eventObj;
                    return (trackingObj);
                }
            }

            return (new MarketingTrackingCodesModel());
        }

        public MarketingTrackingCodesModel addToTrackingList()
        {
            var eventAIO = EventService.Instance.GetByID(this.eventID);


            var trackingList = eventAIO.UserEvent.Tracing;
            //daha önce hiç tracingCode eklenemiş ise
            if (!trackingList.IsExists())
            {
                trackingList = new List<MarketingTrackingCodesModel>();
            }

            bool IsUpdateHappened = false;

            for (int i = 0; i < trackingList.Count; i++)
            {
                var layTracking = trackingList.ElementAt(i);
                if (layTracking.tracingCode == this.tracingCode)
                {
                    layTracking = this; //update edildi
                    IsUpdateHappened = true;
                }
            }

            //update edilmeyecekse yeni bir indirimdir
            if (!IsUpdateHappened)
            {
                trackingList.Add(this);
                eventAIO.UserEvent.Tracing = trackingList;
            }

            this.status = true;
            //trackingList.Add(this);
            eventAIO.UserEvent.Tracing = trackingList;
            EventService.Instance.UpdateEvent(eventAIO.UserEvent);

            return this;
        }

        public bool trackingCodeIsValid()
        {
            var eventAIO = EventService.Instance.GetByID(this.eventID);
            var trackingList = eventAIO.UserEvent.Tracing;
            //daha önce hiç tracingCode eklenemiş ise
            if (!trackingList.IsExists())
            {
                return true;
            }

            for (int i = 0; i < trackingList.Count; i++)
            {
                var layTracking = trackingList.ElementAt(i);
                if (layTracking.tracingCode == this.tracingCode)
                {
                    layTracking = this; //update edildi
                    //IsUpdateHappened = true;
                }
            }

            return true;
        }
    }
}
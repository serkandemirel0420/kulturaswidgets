﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using KulturASWidget.Common.Attributes.ValidationAttributes;
using KulturASWidget.Common.ValidationAttributes;
using KulturASWidget.Common;
using KulturASWidget.Objects;

namespace KulturASWidget.Models.Event
{
    public class EventUserInfoModel
    {
        public Dictionary<Layout, int> LayoutCountDic { get; set; }
        public Layout Layout { get; set; }
        public EventUserInfoModel()
        {
            this.AttendeeList = new List<AttendeeInfo>();
            this.EventAIO = new EventAIO();
            this.Layout = new Layout();
            this.LayoutCountDic = new Dictionary<Layout, int>();
            this.UserInfo = new UserInfo();
        }
        public UserInfo UserInfo { get; set; }
        public EventAIO EventAIO { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsPassbook { get; set; }
        public bool IsSendTypeSelected
        {
            get
            {
                if (this.IsEmail == false && this.IsSms == false && this.IsPassbook == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }
        public List<AttendeeInfo> AttendeeList { get; set; }
        
        public bool IsSubscriber { get; set; }
        public Dictionary<string,string[]> QuestionAnswers { get; set; }
        public List<string> Questions { get; set; }
       
    }
    public class UserInfo
    {
        [Required(ErrorMessageResourceName = "UserFirstNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserFirstNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckIfAllLetters(ErrorMessageResourceName = "UserFirstNameCharsMustBeLetter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String FirstName { get; set; }

        [Required(ErrorMessageResourceName = "UserLastNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        //[CharacterCheck(ErrorMessageResourceName = "UserLastNameCharacter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserLastNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckIfAllLetters(ErrorMessageResourceName = "UserLastNameCharsMustBeLetter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Display(Name = "UserLastName", ResourceType = typeof(Resources.PageResources))]
        public String LastName { get; set; }

        [Required(ErrorMessageResourceName = "UserMailRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        //[IsMailEnrolled(ErrorMessageResourceName = "IsMailEnrolled", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckMailFormat(ErrorMessageResourceName = "EmailValidation", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.EmailAddress)]
        public String Mail { get; set; }

        [Required(ErrorMessageResourceName = "UserCellRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MinLength(10, ErrorMessageResourceName = "UserCellMinLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(20, ErrorMessageResourceName = "UserCellMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsCellAcceptable(ErrorMessageResourceName = "IsCellAcceptable", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.PhoneNumber)]
        public String Cell { get; set; }

    }
    public class AttendeeInfo
    {
        public AttendeeInfo()
        {
            this.QuestionAnswers = new Dictionary<string, string>();
            this.UserInfo = new UserInfo();
        }
        public UserInfo UserInfo { get; set; }
        [Required(ErrorMessageResourceName = "UserQuestionAnswersRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public Dictionary<String, String> QuestionAnswers { get; set; }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Models.Event
{
    public class EventDashboardLayoutModel
    {
        [Required]
        public int eventID { get; set; }
       
        public int layoutID { get; set; }
        public string layoutName { get; set; }
        public string detail { get; set; }
        public string maxcapacity { get; set; }
        public double price { get; set; }
        public CommissionPayer commissionBy { get; set; }
        public string salestartdate { get; set; }
        public string saleenddate { get; set; }
        public int minticket { get; set; }
        public int maxticket { get; set; }
    }
}
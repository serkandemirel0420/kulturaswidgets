﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Services;


namespace KulturASWidget.Models.Event
{
    public class EventPaymentModel
    {
        private Layout layout;
        private Objects.User CurrentUser;

        public void findDiscount(Layout layout, Objects.User CurrentUser, int count, string DiscountCode)
        {
            var layoutPriceWithoutDiscount = layout.TotalPrice;
            var discountList = layout.Discounts;
            var layoutPrice = (double)layout.TotalPrice;
            String discountReason = String.Empty;
            if (discountList.IsExists())
            {
                discountList = (from dis in discountList where dis.code == DiscountCode && dis.status select dis).ToList();
                if (!discountList.IsEmpty())
                {
                    var discount = discountList.First();

                    bool discountFoundFlag = false;
                    if (discount.IsAvailableToEveryone)
                    {
                        discountFoundFlag = true;
                        discountReason = "Genel İndirim";
                    }
                    else
                    {
                        string mailDomain = CurrentUser.Mail.Split('@')[1];
                        var domainsAvailableForDiscount = discount.discountDomainsList;

                       /* if (discount.discountDomainsList.Contains(mailDomain))
                        {
 
                        }*/

                        foreach (var domain in domainsAvailableForDiscount)
                        {
                            string domainName = domain.Remove(0, 1);
                            if (mailDomain == domainName)
                            {
                                discountFoundFlag = true;
                            }
                            discountReason = "Alan adına özel indirim";
                        }
                    }


                    if (discountFoundFlag)
                    {
                        layoutPrice -= layout.Commission;
                        if (discount.type == Marketing.DiscountCalculationType.byAmount)
                        {
                            layoutPrice -= (discount.calculationValue * 100);

                            if (layoutPrice < 0)
                            {
                                return;
                            }

                        }
                        else
                        {
                            layoutPrice = (layoutPrice * (100 - discount.calculationValue)) / 100;

                            if (layoutPrice < 0)
                            {
                                return;
                            }
                        }

                        layoutPrice += layout.Commission;

                        layoutPrice = layoutPrice * count;

                    }

                    DateTime discountEndDate = discount.endDate;
                    if (discountEndDate < DateTime.Now)
                    {
                        return;
                    }

                    var ticketLimit = Convert.ToInt32(discount.numberOfTickets);
                    if (discount.discountLimit == Marketing.DiscountLimits.limitNumberOfTotalTickets)
                    {
                        //TODO: asıl miktarlar gösterilmiyor
                        var ReservationList = ReservationService.Instance.GetByLayout(Convert.ToInt32(discount.layoutID));
                        if (ticketLimit < (count))
                        {
                            return;
                        }
                    }
                    else if (discount.discountLimit == Marketing.DiscountLimits.limitNumberOfTickets)
                    {
                        var reservationList = ReservationService.Instance.Search(CurrentUser.UserID, layout.LayoutID);
                        if (ticketLimit < (count))
                        {
                            return;
                        }
                    }

                    var discountModel = new EventDiscountModel();
                    discountModel.DiscountPrice = ((double)(layoutPriceWithoutDiscount * count - layoutPrice) / 100);
                    discountModel.DiscountReason = discountReason;
                    this.Discount = discountModel;
                    double price = Double.Parse(this.Sum);
                    price -= discountModel.DiscountPrice;
                    this.Sum = price.ToString();
                }
               
            }
        }

        public EventPaymentModel()
        { }
        public EventPaymentModel(Layout layout, Objects.User CurrentUser, int count)
        {
            this.layout = layout;
            this.CurrentUser = CurrentUser;


            //Event Info
            var eventLayoutObj = new EventLayoutModel();
            eventLayoutObj.LayoutName = layout.Name;
            eventLayoutObj.LayoutPrice = (((double)(layout.TotalPrice - layout.Commission) / 100)).ToString();
            eventLayoutObj.LayoutServiceCommission = (((double)layout.Commission / 100)).ToString();
            eventLayoutObj.LayoutSum = ((((double)layout.TotalPrice * count) / 100)).ToString();
            this.Sum = eventLayoutObj.LayoutSum;
            this.EventLayout = eventLayoutObj;

            var eventAIO = EventService.Instance.GetByID(layout.EventID);
            this.EventStartDate = eventAIO.UserEvent.CommenceDate.ToString();
            this.EventName = eventAIO.UserEvent.Name;
            //this.EventInfo = eventAIO.UserEvent.Detail;
            this.EventLocation = eventAIO.UserVenue.Address;

            //Buyer Info
            this.BuyerNameSurname = CurrentUser.FullName;
            this.BuyerEmail = CurrentUser.Mail;
            this.BuyerPhoneNumber = CurrentUser.Cell;


        }

        public String EventStartDate { set; get; }
        public String EventName { get; set; }
        public String EventInfo { get; set; }
        public String EventLocation { get; set; }
        public EventLayoutModel EventLayout { get; set; }
        public String Discountcode { get; set; }
        public String Sum { get; set; }

        //Buyer Info
        public String BuyerNameSurname { get; set; }
        public String BuyerEmail { get; set; }
        public String BuyerPhoneNumber { get; set; }

        //Credit Cart Info
        public String CreditCartNameSurname { get; set; }
        public String CreditCartNumber { get; set; }
        public String CreditCartCVC2 { get; set; }
        public String CreditCartValidationMonth { get; set; }
        public String CreditCartValidationYear { get; set; }

        //Discount
        public EventDiscountModel Discount { get; set; }
        /*
        //logo
        public string OrganizationLogoPictureUrl
        {
            get
            {
                return String.Format("{0}/User/{1}/OriginalLogo.png?u={2:hhmmss}", ApplicationConfiguration.ImagesUrlPath, EventService.Instance.GetByID(this.layout.EventID).User.DefaultOrganizationID, DateTime.Now);
            }
        }*/

        public Boolean LegalConditions { get; set; }

        public List<string> IsValidFor(string attendeeNameSurname, string AttendeeMail, string AttendeeCell)
        {
            List<string> errorList = new List<string>();

            if (!attendeeNameSurname.IsExists())
            {
                errorList.Add("Katılımcı İsim soyisim alanı zorunludur!");
            }
            if (!AttendeeMail.IsExists())
            {
                errorList.Add("Katılımcı Mail alanı zorunludur!");
            }
            if (!AttendeeCell.IsExists())
            {
                errorList.Add("Katılımcı Telefon alanı zorunludur!");
            }

            return errorList;
        }
    }
    public class EventDiscountModel
    {
        public Double DiscountPrice
        {
            get;
            set;
        }
        public String DiscountReason { get; set; }
    }

    public class EventLayoutModel
    {
        public String LayoutName { get; set; }
        public String LayoutPrice { get; set; }
        public String LayoutServiceCommission { get; set; }
        public String LayoutSum { get; set; }
    }
}
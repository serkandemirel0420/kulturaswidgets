﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using KulturASWidget.Objects;
using KulturASWidget.Common.Attributes.ValidationAttributes;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AutoMapper;
using System.Drawing;
using System.Security.Policy;
using System.Globalization;
using KulturASWidget.Common;
using KulturASWidget.Services;
using System.Text.RegularExpressions;
using KulturASWidget.Resources;


namespace KulturASWidget.Models.Event
{
    public class EventCreateModel
    {
        [Display(Name = "EventEventName", ResourceType = typeof(Resources.PageResources))]

        [StringLength(100, ErrorMessageResourceName = "EventEventNameLength", MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventEventNameRequried", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventName { get; set; }

        [Display(Name = "EventEventURL", ResourceType = typeof(Resources.PageResources))]

        [StringLength(100, ErrorMessageResourceName = "EventEventURLLength", MinimumLength = 6, ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventEventURLRequried", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [NoWhitespace(ErrorMessageResourceName = "EventEventURLNoWhitespace", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsAplhanumeric(ErrorMessageResourceName = "EventEventURLOnlyAlphanumeric", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [NoNumber(ErrorMessageResourceName = "EventEventURLNoNumber", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [NoTurkishChars(ErrorMessageResourceName = "EventEventURLNoTurkish", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [NoUpperCase(ErrorMessageResourceName = "EventEventURLNoUpperCase", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventURL { get; set; }


        [Display(Name = "EventDetail", ResourceType = typeof(Resources.PageResources))]
        [AllowHtml]
        [StringLength(8000, ErrorMessageResourceName = "EventEventDetailLength", MinimumLength = 1, ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventDetailRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventDetail { get; set; }

        [Display(Name = "EventCommence", ResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventCommenceRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public DateTime EventStartDate { get; set; }

        [Display(Name = "EventCommenceHour", ResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventCommenceHourRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventStartHour { get; set; }

        [Required(ErrorMessageResourceName = "EventEndRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [Display(Name = "EventEnd", ResourceType = typeof(Resources.PageResources))]
        public DateTime EventEndDate { get; set; }

        [Display(Name = "EventEndHour", ResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventEndHourRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventEndHour { get; set; }

        [Display(Name = "EventTags", ResourceType = typeof(Resources.PageResources))]
        [Required(ErrorMessageResourceName = "EventTagsRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [ValidateJSON(ErrorMessageResourceName = "EventTagsNotValid", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String EventTags
        {
            get
            {
                return JsonConvert.SerializeObject(this.EventTagList);
            }
            set
            {
                EventTagList = JsonConvert.DeserializeObject<List<String>>(value);
                EventTagList = EventTagList.Select(x => x.Replace("&amp;", "&")).ToList();
            }
        }

        public List<String> EventTagList { get; set; }

        public String EventPassword { get; set; }
        public bool IsPrivate { get; set; }
        public bool ModerationRequired { get; set; }

        [Required(ErrorMessageResourceName = "VenueNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueName { get; set; }
        [Required(ErrorMessageResourceName = "VenueAddressRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueAddress { get; set; }
        [Required(ErrorMessageResourceName = "CityNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueCity { get; set; }
        public String VenueLat { get; set; }
        public String VenueLon { get; set; }

        public VenueSourceType VenueType { get; set; }

        [ValidateJSON(ErrorMessageResourceName = "PictureDataNotValid", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String ImageIds
        {
            get
            {
                
                return JsonConvert.SerializeObject(ImageList);
            }
            set
            {
                ImageList = JsonConvert.DeserializeObject<List<Image>>(value);
            }
        }

        public List<Image> ImageList { get; set; }

        [YoutubeLinkValidation(ErrorMessageResourceName = "YoutubeUrlNotValid", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String YoutubeURL { get; set; }

        [Required]
        [ValidateJSON(ErrorMessageResourceName = "LayoutListNotValid", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String LayoutList
        {
            get
            {
                return JsonConvert.SerializeObject(this.LayoutListObj);
            }
            set
            {
                this.LayoutListObj = JsonConvert.DeserializeObject<List<EventCreateLayout>>(value);
                layoutRawJSON = value;
            }
        }
        string layoutRawJSON { get; set; }
        private List<EventCreateLayout> LayoutListObj { get; set; }

        public List<EventQuestion> EventQuestions { get; set; }
        public String EventQuestionDummy { get; set; }
        public EventCreateFormAction FormAction { get; set; }

        public EventCreateModel()
        {
            EventQuestions = new List<EventQuestion>();
            EventTagList = new List<string>();
        }

        public bool BuildQuestions(FormCollection collection)
        {
            EventQuestion questionModel = new EventQuestion();

            EventQuestions = new List<EventQuestion>();

            foreach (var key in collection.AllKeys)
            {
                if (key.StartsWith("question_"))
                {
                    if (!questionModel.Question.IsEmpty())
                    {
                        EventQuestions.Add(questionModel);
                    }
                    questionModel = new EventQuestion();
                    questionModel.Question = collection[key];
                }
                else if (key.StartsWith("answer_"))
                {
                    if (!collection[key].IsEmpty() && !questionModel.Question.IsEmpty())
                    {
                        questionModel.AnswerList.Add(collection[key]);
                    }
                }
                else if (key.StartsWith("showInModeration_"))
                {
                    if (!collection[key].IsEmpty() && !questionModel.Question.IsEmpty())
                    {
                        questionModel.ShowInModeration = true;
                    }
                }
                else if (!collection[key].IsEmpty() && !questionModel.Question.IsEmpty())
                {
                    EventQuestions.Add(questionModel);
                    break;
                }
            }
            return true;
        }

        public Objects.Event GetEvent(Objects.User currentOrganization, Objects.Venue userVenue)
        {
            var userEvent = Mapper.Map<Objects.Event>(this);
            userEvent.UserID = currentOrganization.UserID;
            userEvent.VenueID = userVenue.VenueID;
            return userEvent;
        }

        public List<Objects.Layout> GetLayoutList(Objects.Event userEvent)
        {
            List<Objects.Layout> layoutLst = new List<Layout>();
            foreach (var lay in this.LayoutListObj)
            {
                Layout layout = Mapper.Map<EventCreateLayout, Layout>(lay);

                layout.EventID = userEvent.EventID;
                layoutLst.Add(layout);
            }
            return layoutLst;
        }

        public Objects.Venue GetVenue(Objects.User currentOrganization)
        {
            var userVenue = Mapper.Map<Objects.Venue>(this);
            userVenue.UserID = currentOrganization.UserID;
            return userVenue;
        }

        public bool IsValid(ModelStateDictionary modelState)
        {
            bool eventStartEndHourError = false;
            DateTime eventEndUTC = new DateTime();
            DateTime eventStartUTC = new DateTime();
            if (!modelState.HasError("EventStartDate") && !modelState.HasError("EventStartHour") )
            {
                eventStartUTC = this.EventStartDate.Date;
                eventStartUTC = eventStartUTC.Add(TimeSpan.Parse(this.EventStartHour));
                eventStartUTC = TimeZoneInfo.ConvertTimeToUtc(eventStartUTC, ApplicationConfiguration.CurrentTimezone);
            }
            else
            {
                eventStartEndHourError = true;
            }

            if (!modelState.HasError("EventEndDate") && !modelState.HasError("EventEndHour"))
            {
                eventEndUTC = this.EventEndDate.Date;
                eventEndUTC = eventEndUTC.Add(TimeSpan.Parse(this.EventEndHour));
                eventEndUTC = TimeZoneInfo.ConvertTimeToUtc(eventEndUTC, ApplicationConfiguration.CurrentTimezone);
            }
            else
            {
                eventStartEndHourError = true;
            }

            if (!eventStartEndHourError)
            {
                if (eventStartUTC < DateTime.UtcNow)
                {
                    modelState.AddModelError("EventStartDate", "Etkinlik başlangıç zamanı geçmiş zaman olamaz.");
                }
                if (eventStartUTC > eventEndUTC)
                {
                    modelState.AddModelError("EventStartDate", "Etkinlik başlangıç zamanı bitiş zamanından sonra olamaz");
                }
            }

            this.LayoutsValidation(modelState);
            this.EventQuestionsValidation(modelState);
         
            if(this.EventTagList.IsEmpty())
            {
                modelState.AddModelError("EventTags", PageResources.EventTagRequried);
            }
            /*
            if (this.ImageList.IsExists())
            {
                foreach (var item in this.ImageList)
                {
                    if (!ImageService.Instance.ImageStorage.ContainsKey(item.name))
                    {
                        var keys = ImageService.Instance.ImageStorage.Keys.Aggregate((a, b) => {
                            return a + ", " + b;
                        });
                        var obj = new { message = "InvalidImageData",keys=keys,requestedKey = item.name};
                        var ex = new Exception(JsonConvert.SerializeObject(obj));
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                        modelState.AddModelError("ImageIds", "Geçersiz Resim Bilgisi. Lütfen Tekrar Deneyiniz.");
                        this.ImageIds = String.Empty;
                        if (this.ImageList.IsExists())
                        {
                            this.ImageList.Clear();
                        }
                    
                        break;
                    }
                }
            }
            */

            return modelState.IsValid;
        }


        private void LayoutsValidation(ModelStateDictionary modelState)
        {
            if (!this.LayoutListObj.IsExists() || this.LayoutListObj.Count == 0)
            {
                modelState.AddModelError("LayoutList", "En az bir bilet eklemeniz gerekmektedir.");
            }
            else
            {
                foreach (var item in this.LayoutListObj)
                {
                    Layout lay = Mapper.Map<EventCreateLayout, Layout>(item);
                    if (lay.SaleStartDate == new DateTime())
                    {
                        lay.SaleStartDate = Methods.UtcToday ;
                    }

                    if (lay.SaleEndDate == new DateTime())
                    {
                        lay.SaleEndDate = this.EventStartDate.Date;
                    }

                    if (lay.SaleStartDate < Methods.UtcToday)
                    {
                        modelState.AddModelError("LayoutList", "Bilet satış başlangıç zamanı geçmiş zaman olamaz.");
                    }
                    if (lay.SaleStartDate > lay.SaleEndDate)
                    {
                        modelState.AddModelError("LayoutList", "Bilet satış başlangıç zamanı bitiş zamanından sonra olamaz");
                    }
                    if (item.name.IsEmpty())
                    {
                        modelState.AddModelError("LayoutList", "Bilet adı gereklidir.");
                    }
                    else if (item.name.Length > 50)
                    {
                        modelState.AddModelError("LayoutList", "Bilet adı 50 karakterden uzun olamaz.");
                    }
                    if (item.price < 0)
                    {
                        modelState.AddModelError("LayoutList", "Geçerli ücret giriniz.");
                    }
                }
            }
        }
        private void ImagesValidation(ModelStateDictionary modelState)
        {
        }
        private void EventQuestionsValidation(ModelStateDictionary modelState)
        {
            foreach (var item in this.EventQuestions)
            {
                if (item.AnswerList.IsExists() && item.AnswerList.Count == 1)
                {
                    modelState.AddModelError("EventQuestionDummy", "Etkinlik sorularında bir adet cevap olamaz.");
                }
                if (!item.AnswerList.IsExists() && item.AnswerList.Count > 0 && item.Question.IsEmpty())
                {
                    modelState.AddModelError("EventQuestionDummy", "Lütfen soruları tam giriniz");
                }
            }
        }

       
    }


    public class Image
    {
        public string name { get; set; }
        public ImageCropData data { get; set; }
        [JsonIgnore]
        public bool CropDataAvaliable
        {
            get
            {
                return data.IsExists();
            }
        }
    }

    public class EventCreateLayout
    {
        public string name { get; set; }
        public string detail { get; set; }
        public string maxcapacity { get; set; }
        public double price { get; set; }
        public CommissionPayer commissionBy { get; set; }
        public string salestartdate { get; set; }
        public string saleenddate { get; set; }
        public int minticket { get; set; }
        public int maxticket { get; set; }
        
    }

    

    public class ImageCropData
    {
        public double x { get; set; }
        public double y { get; set; }
        public double x2 { get; set; }
        public double y2 { get; set; }
        public double w { get; set; }
        public double h { get; set; }
        public double boundx { get; set; }
        public double boundy { get; set; }
        public ImageCropData preview { get; set; }
        public Rectangle GetRectangle(System.Drawing.Image img)
        {
            return new Rectangle((int)(img.Width * x), (int)(img.Height * y), (int)(img.Width * w), (int)(img.Height * h));
        }
    }
}
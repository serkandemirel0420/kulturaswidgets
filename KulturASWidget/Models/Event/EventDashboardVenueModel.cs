﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Models.Event
{
    public class EventDashboardVenueModel
    {
      
        public int VenueID { get; set; }
        [Required(ErrorMessageResourceName = "VenueNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueName { get; set; }
        [Required(ErrorMessageResourceName = "VenueAddressRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueAddress { get; set; }
        [Required(ErrorMessageResourceName = "CityNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String VenueCity { get; set; }
        public String VenueLat { get; set; }
        public String VenueLon { get; set; }

        public VenueSourceType VenueType { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using KulturASWidget.Common.Attributes.ValidationAttributes;
using KulturASWidget.Common.ValidationAttributes;
using KulturASWidget.Common.Attributes;
using KulturASWidget.Common;
using KulturASWidget.Objects;

namespace KulturASWidget.Models.Event
{
    public class EventSendReservationModel
    {
        public EventAIO EventAIO { get; set; }

        [CheckMailFormat(ErrorMessageResourceName = "EmailValidation", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.EmailAddress)]
        public String TicketEmail { get; set; }

        [MinLength(10, ErrorMessageResourceName = "UserCellMinLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(20, ErrorMessageResourceName = "UserCellMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsCellAcceptable(ErrorMessageResourceName = "IsCellAcceptable", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String TicketCell { get; set; }

        [Required(ErrorMessageResourceName = "UserFirstNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserFirstNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckIfAllLetters(ErrorMessageResourceName = "UserFirstNameCharsMustBeLetter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String FirstName { get; set; }

        [Required(ErrorMessageResourceName = "UserLastNameRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CharacterCheck(ErrorMessageResourceName = "UserLastNameCharacter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(40, ErrorMessageResourceName = "UserLastNameMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckIfAllLetters(ErrorMessageResourceName = "UserLastNameCharsMustBeLetter", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String LastName { get; set; }

        [Required(ErrorMessageResourceName = "UserMailRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [CheckMailFormat(ErrorMessageResourceName = "EmailValidation", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [DataType(DataType.EmailAddress)]
        public String Mail { get; set; }

        [Required(ErrorMessageResourceName = "UserCellRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MinLength(10, ErrorMessageResourceName = "UserCellMinLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [MaxLength(20, ErrorMessageResourceName = "UserCellMaxLength", ErrorMessageResourceType = typeof(Resources.PageResources))]
        [IsCellAcceptable(ErrorMessageResourceName = "IsCellAcceptable", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String Cell { get; set; }


        public Dictionary<String, String> QuestionAnswers { get; set; }
        public bool IsSms { get; set; }
        public bool IsEmail { get; set; }
        public bool IsPassbook { get; set; }


        public bool IsSendTypeSelected
        {
            get
            {
                if (this.IsEmail == false && this.IsSms == false && this.IsPassbook == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
        }

       
    }
}
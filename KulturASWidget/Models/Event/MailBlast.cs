﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Models.Event
{
    public class MailBlast
    {
        [Required]
        public int EventID { get; set; }
        [Required(ErrorMessageResourceName = "MailSubjectRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String Subject { get; set; }
        [Required(ErrorMessageResourceName = "MailFromRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String From { get; set; }
        [Required(ErrorMessageResourceName = "MailContentRequired", ErrorMessageResourceType = typeof(Resources.PageResources))]
        public String Content { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Models.Marketing;
using KulturASWidget.Services;
using KulturASWidget.Common;


namespace KulturASWidget.Models.Event
{
    public class EventDetailModel
    {
        //etkinlik adı
        public EventAIO EventAIO { get; set; }
        public List<LayoutDetail> LayoutList { get; set; }
        public EventDetailModel()
        {
            this.LayoutList = new List<LayoutDetail>();
        }
        
    }


    public class LayoutDetail
    {
        public Int32 LayoutID { get; set; }
        public String Name { get; set; }
        public Int32 TotalPrice { get; set; }
        public String DisplayTotalPrice
        {
            get
            {
                return String.Format("{0} TL", ((double)this.TotalPrice) / 100);
            }
        }
        public Int32 UsedCapacity { get; set; }
        public Int32 MaxCapacity { get; set; }
        public Int32 MaxTicket { get; set; }
        public Int32 MinTicket { get; set; }
        public String DiscountMessage { get; set; }
        public DateTime SaleStartDate { get; set; }
        public DateTime SaleEndDate { get; set; }
        public String Detail { get; set; }
        public bool IsDiscount { get; set; }



    }

}
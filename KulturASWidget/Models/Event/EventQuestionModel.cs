﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Models.Event
{
    public class EventQuestionModel
    {
        public String Question { get; set; }
        public IList<String> AnswerList { get; set; }
        public EventQuestionModel()
        {
            AnswerList = new List<String>();
        }
    }
}
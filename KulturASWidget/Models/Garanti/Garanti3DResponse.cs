﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Specialized;
using KulturASWidget.Services.Security;

namespace KulturASWidget.Models.Garanti
{
    public class Garanti3DResponse
    {
        public string MDStatus;
        public string MDStatusText;
        public string SuccessURL;
        public string ErrorURL;
        public string strType;
        public string hashDataFromServer;
        public string secureKey;
        public string provPassword;
        public GVPSRequest request;

        public Garanti3DResponse(NameValueCollection Form,string secureKey,string provPassword)
        {
            this.strType = Form.Get("txntype");
            this.secureKey = secureKey;
            this.provPassword = provPassword;

            this.SuccessURL = Form.Get("successurl");
            this.ErrorURL = Form.Get("errorurl");

            this.MDStatus = Form.Get("mdstatus"); ;
            this.MDStatusText = Form.Get("mderrormessage");

            this.request = new GVPSRequest();

            this.request.Mode = Form.Get("mode");

            var order = new Order();
            order.OrderID = Form.Get("oid");
            this.request.Order = order;

            var card = new Card() { };
            card.CVV2 = "";
            card.ExpireDate = "";
            card.Number = "";
            this.request.Card = card;
        
            var secure3d = new KulturASWidget.Models.Garanti.Secure3D();
            secure3d.AuthenticationCode = HttpUtility.UrlEncode(Form.Get("cavv"));
            secure3d.Md = HttpUtility.UrlEncode(Form.Get("md"));
            secure3d.SecurityLevel = HttpUtility.UrlEncode(Form.Get("eci"));
            secure3d.TxnID = HttpUtility.UrlEncode(Form.Get("xid"));

            var transaction = new KulturASWidget.Models.Garanti.Transaction();
            transaction.InstallmentCnt = Form.Get("txninstallmentcount");
            transaction.Amount = Form.Get("txnamount");
            transaction.CardholderPresentCode = "13";
            transaction.Secure3D = secure3d;
            this.request.Transaction = transaction;

            var terminal = new Terminal();
            terminal.ID = Form.Get("clientid");
            terminal.MerchantID = Form.Get("terminalmerchantid");
            terminal.ProvUserID = Form.Get("terminalprovuserid");
            terminal.UserID = Form.Get("terminaluserid");

            this.request.Terminal = terminal;
            terminal.HashData = GarantiService.Instance.GetSHA1(order.OrderID + terminal.ID + card.Number + transaction.Amount + SecurityData).ToUpper(); ;
            

            var customer = new Customer();
            customer.EmailAddress = Form.Get("customeremailaddress");
            customer.IPAddress = Form.Get("customeripaddress");

            this.request.Customer = customer;



            //Hashdata kontrolü için bankadan dönen secure3dhash değeri alınıyor.
            hashDataFromServer = Form.Get("secure3dhash");
            
         
        
        }
        private string SecurityData
        {
            get
            {
                return GarantiService.Instance.GetSHA1(provPassword + request.Terminal.ID.ToString().PadLeft(9, '0')).ToUpper();
            }
        }


        public string ValidateHashData
        {
            get 
            {
                return GarantiService.Instance.GetSHA1(request.Terminal.ID + request.Order.OrderID + request.Transaction.Amount + SuccessURL + ErrorURL + strType + request.Transaction.InstallmentCnt + secureKey + SecurityData).ToUpper();
            }
        }

        public string XML
        {
            get
            {
                return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                       + "<GVPSRequest>"
                           + "<Mode>" + request.Mode + "</Mode>"
                           + "<Version>" + request.Version + "</Version>"
                           + "<ChannelCode></ChannelCode>"
                           + "<Terminal>"
                               + "<ProvUserID>" + request.Terminal.ProvUserID + "</ProvUserID>"
                               + "<HashData>" + request.Terminal.HashData + "</HashData>"
                               + "<UserID>" + request.Terminal.UserID + "</UserID>"
                               + "<ID>" + request.Terminal.ID + "</ID>"
                               + "<MerchantID>" + request.Terminal.MerchantID + "</MerchantID>"
                           + "</Terminal>"
                           + "<Customer>"
                               + "<IPAddress>" + request.Customer.IPAddress + "</IPAddress>"
                               + "<EmailAddress>" + request.Customer.EmailAddress + "</EmailAddress>"
                           + "</Customer>"
                           + "<Card><Number></Number><ExpireDate></ExpireDate><CVV2></CVV2></Card>"
                           + "<Order>"
                               + "<OrderID>" + request.Order.OrderID + "</OrderID>"
                               + "<GroupID></GroupID>"
                               + "<AddressList>"
                                   + "<Address><Type>B</Type><Name></Name><LastName></LastName><Company></Company><Text></Text><District></District><City></City><PostalCode></PostalCode><Country></Country><PhoneNumber></PhoneNumber></Address>"
                               + "</AddressList>"
                           + "</Order>"
                           + "<Transaction>"
                               + "<Type>" + request.Transaction.Type + "</Type>"
                               + "<InstallmentCnt>" + request.Transaction.InstallmentCnt + "</InstallmentCnt>"
                               + "<Amount>" + request.Transaction.Amount + "</Amount>"
                               + "<CurrencyCode>" + request.Transaction.CurrencyCode + "</CurrencyCode>"
                               + "<CardholderPresentCode>" + request.Transaction.CardholderPresentCode + "</CardholderPresentCode>"
                               + "<MotoInd>" + request.Transaction.MotoInd + "</MotoInd>"
                               + "<Secure3D>"
                                   + "<AuthenticationCode>" + request.Transaction.Secure3D.AuthenticationCode + "</AuthenticationCode>"
                                   + "<SecurityLevel>" + request.Transaction.Secure3D.SecurityLevel + "</SecurityLevel>"
                                   + "<TxnID>" + request.Transaction.Secure3D.TxnID + "</TxnID>"
                                   + "<Md>" + request.Transaction.Secure3D.Md + "</Md>"
                               + "</Secure3D>"
                           + "</Transaction>"
                       + "</GVPSRequest>";
            }
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Models.Garanti
{
    public class GVPSRequest
    {
        public String Mode = "PROD";
        public String Version = "v0.01";
        public Customer Customer { get; set; }
        public Terminal Terminal { get; set; }
        public Card Card { get; set; }
        public Order Order { get; set; }
        public Transaction Transaction { get; set; }
    }

    public class Transaction
    {
        public String Type = "sales";
        public String InstallmentCnt { get; set; }
        public String Amount { get; set; }
        public String CurrencyCode = "949";
        public String CardholderPresentCode = "0";
        public String MotoInd = "N";
        public Secure3D Secure3D { get; set; }
    }

    public class Address
    {
        public String Type { get; set; }
        public String Name { get; set; }
        public String LastName { get; set; }
        public String Company { get; set; }
        public String Text { get; set; }
        public String District { get; set; }
        public String City { get; set; }
        public String PostalCode { get; set; }
        public String Country { get; set; }
        public String PhoneNumber { get; set; }
    }

    public class Order
    {
        public String OrderID { get; set; }
        public String GroupID = String.Empty;
        public Address[] AddressList { get; set; }
    }

    public class Card
    {
        public String Number { get; set; }
        public String ExpireDate { get; set; }
        public String CVV2 { get; set; }
    }

    public class Customer
    {
        public String IPAddress { get; set; }
        public String EmailAddress { get; set; }
    }

    public class Terminal
    {
        public String ProvUserID = "PROVAUT";
        public String HashData { get; set; }
        public String UserID { get; set; }
        public String ID { get; set; }
        public String MerchantID { get; set; }

    }

    public class Secure3D
    {
        public String AuthenticationCode {get;set;}
        public String SecurityLevel {get;set;}
        public String TxnID {get;set;}
        public String Md {get;set;}
    }

}
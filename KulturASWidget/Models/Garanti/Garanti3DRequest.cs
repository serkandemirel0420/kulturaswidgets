﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services.Security;
using KulturASWidget.Common;


namespace KulturASWidget.Models.Garanti
{
    public class Garanti3DRequest
    {
        public Garanti3DRequest(string successReturnURL,
            string errorReturnURL,
            string _3DKey,
            string provPassword,
            double amount,
            string orderID,
            string customerIPAddress,
            string customerEmailAddress,
            string terminalID,
            string terminalUserID,
            string terminalMerchantID)
        {
            this.SuccessURL = successReturnURL;
            this.ErrorURL = errorReturnURL;
            this.StoreKey = _3DKey;
            this.ProvisionPassword = provPassword;
            this.Amount = ((int)(amount * 100)).ToString();
            this.OrderID = orderID;
            this.Customeripaddress = customerIPAddress;
            this.customeremailaddress = customeremailaddress;
            this.TerminalID = terminalID;
            this.TerminalUserID = terminalUserID;
            this.TerminalMerchantID = terminalMerchantID;

        }

        public string Mode = "PROD"; //PROD olacak //------!!--------
        public string ApiVersion = "v0.01";
        public string TerminalProvUserID = "PROVAUT";
        public string Type = "sales";
        public string CurrencyCode = "949";
        public string InstallmentCount = ""; //Taksit Sayısı. Boş gönderilirse taksit yapılmaz

        public string Amount { get; set; } //İşlem Tutarı 1.00 TL için 100 gönderilmeli
        public string TerminalUserID { get; set; } //----------------!!----------- ????????????
        public string OrderID { get; set; } //----------------!!--------------- ?????????????
        public string Customeripaddress { get; set; } //Kullanıcının IP adresini alır
        public string customeremailaddress { get; set; } //----------------!!-----------
        public string TerminalID { get; set; } //8 Haneli TerminalID yazılmalı. ----------------!!-----------
        private string _TerminalID
        {
            get
            {
                return TerminalID.ToString().PadLeft(9, '0');
            }
        }
        public string TerminalMerchantID { get; set; } //Üye İşyeri Numarası ----------------!!-----------

        public string StoreKey { get; set; } //3D Secure şifresi -------------------------------------------
        private string ProvisionPassword { get; set; }  //TerminalProvUserID şifresi ----------------!!-----------
        public string SuccessURL { get; set; }
        public string ErrorURL { get; set; }

        public string SecurityData
        {
            get { return GarantiService.Instance.GetSHA1(ProvisionPassword + _TerminalID).ToUpper(); }
        }
        public string HashData
        {
            get { return GarantiService.Instance.GetSHA1(TerminalID + OrderID + Amount + SuccessURL + ErrorURL + Type + InstallmentCount + StoreKey + SecurityData).ToUpper(); }
        }

    }
}
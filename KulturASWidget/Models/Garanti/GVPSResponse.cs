﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Models.Garanti;

namespace KulturASWidget.Models.Garanti.Response
{
    public class GVPSResponse
    {
        public Transaction Transaction { get; set; }
        public Customer Customer { get; set; }
        public Order Order { get; set; }
        /*
         
            <GVPSResponse><Transaction><Response>
         *  <ErrorCode>0001</ErrorCode>
         *      <ErrorMessage>Giriş yaptığınız değerleri kontrol ediniz</ErrorMessage>
         *      <Source>GVPS</Source><Code>92</Code><ReasonCode>0001</ReasonCode><Message>Declined</Message>
         *      <ErrorMsg>Giriş yaptığınız değerleri kontrol ediniz</ErrorMsg><SysErrMsg>Giriş yaptığınız değerleri kontrol ediniz</SysErrMsg></Response></Transaction></GVPSResponse>
         
         */


    }


    public class Transaction
    {
        public Response Response { get; set; }
        public string RetrefNum { get; set; }
        public string AuthCode { get; set; }
        public string BatchNum { get; set; }
        public string SequenceNum { get; set; }
        public string CardNumberMasked { get; set; }
        public string CardHolderName { get; set; }
        public string ProvDate { get; set; }
        public string HashData { get; set; }
    }

    public class Response
    {
        public String ErrorCode { get; set; }
        public String ErrorMessage { get; set; }
        public String Source { get; set; }
        public String Code { get; set; }
        public String ReasonCode { get; set; }
        public String Message { get; set; }
        public String ErrorMsg { get; set; }
        public String SysErrMsg { get; set; }

        public String ErrorMessageFromCode
        {
            get
            {
                string message = "";
                switch (this.ReasonCode)
                {
                    /*case "05":
                        message = "İşlem Onaylanmadı";
                        break;
                    case "06":
                        message = "İşlem Kabul Edilmedi";
                        break;*/
                    /*case "09":
                        message = "Kart Yenilenmiş";
                        break;*/
                    case "12":
                        message = "Geçersiz İşlem. Bankanızla İrtibata Geçiniz.";
                        break;
                    /*case "13":
                        message = "Geçersiz Tutar";
                        break;*/
                    case "14":
                        message = "Kart Numarası Hatalı. Bankanızla İrtibata Geçiniz.";
                        break;
                    case "15":
                        message = "Kart Bankası Bulunamadı. Bankanızla İrtibata Geçiniz.";
                        break;
                    case "16":
                        message = "Bakiye Yetersiz. Bankanızla İrtibata Geçiniz.";
                        break;
                    /*case "18":
                        message = "Kapalı Kart";
                        break;*/
                    case "33":
                        message = "Kartınızın Süresi Dolmuştur. Bankanızla İrtibata Geçiniz.";
                        break;
                   /* case "41":
                        message = "Kayıp Kart";
                        break;
                    case "57":
                        message = "Teslim Edilemediğinden Kapalı  ";
                        break;
                    case "58":
                        string sysError = SysErrMsg;
                        if (sysError.Contains('-'))
                        {
                            sysError = sysError.Substring(sysError.LastIndexOf('-') + 1);
                        }

                        message = sysError;
                        break;*/
                    default:
                        message = "İşleminiz Onaylanmadı. Bankanızla İrtibata Geçiniz";
                        break;
                }
                return message;
            }
        }
    }






}
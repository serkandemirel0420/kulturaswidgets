﻿/// <reference path="jquery-1.7.2.js" />
/// <reference path="Utils.js" />

biletino.controllers.MapController = function (mapView) {

    var instance = this;
    var map;
    var markers;
    var geocoder;
    var currentMarker;

    this.init = function (lat, lng, eventTitle) {
        instance.markers = new Array();
        if (!lat || !lng) {
            lat = 41.03;
            lng = 28.95;
        }
        var latlng = new google.maps.LatLng(lat, lng);
        var myOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        instance.map = new google.maps.Map(mapView.mapCanvas(), myOptions);
        //instance.bindUserDetailButtons();
        //instance.loadUsers();
        instance.map.setCenter(latlng);
        instance.geocoder = new google.maps.Geocoder();

        google.maps.event.addListener(instance.map, 'click', function (event) {
            //instance.initCurrentMarker(event.latLng, eventTitle);
        });

        instance.initCurrentMarker(latlng, eventTitle);

    };


    this.initCurrentMarker = function (latLng, eventTitle) {
        if (instance.currentMarker) {
            instance.currentMarker.setMap(null);
            instance.currentMarker = null;
        }
        instance.currentMarker = new google.maps.Marker({
            position: latLng,
            draggable: true,
            animation: google.maps.Animation.DROP,
            title: eventTitle
        });

        google.maps.event.addListener(instance.currentMarker, 'dragend', function () {
            var center = instance.currentMarker.getLatLng();
            instance.currentMarker.openInfoWindowHtml("<font color=black>" + myHtml + "<br>" + center.toString() + "</font>");
        });

        google.maps.event.addListener(instance.currentMarker, 'dragstart', function () {
            instance.map.closeInfoWindow();
        });
        google.maps.event.addListener(instance.currentMarker, 'click', function () {

            if (instance.currentMarker.getAnimation() != null) {
                instance.currentMarker.setAnimation(null);
            } else {
                instance.currentMarker.setAnimation(google.maps.Animation.BOUNCE);
            }
        });


        mapView.Latitude().val(latLng.lat());
        mapView.Longitude().val(latLng.lng());
        instance.currentMarker.setMap(instance.map);
        this.showAddressInfo(latLng);
    };

   


    this.showAddressInfo = function (latlng) {
        var reverseGeoCode = instance.geocoder.geocode({ 'latLng': latlng },
            function (result, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var addressComponents = result[1].address_components;
                    for (i = 0; i < addressComponents.length; i++) {
                        if (addressComponents[i].types[0] == "administrative_area_level_1") {
                            $("#City").val(addressComponents[i].long_name);
                            break;
                        }
                    }
                    //if (result[1]) { $("#Address").html(result[1].formatted_address); }
                }
            });

    };

    this.search = function (address, callback, errorText) {
        instance.geocoder = new google.maps.Geocoder();
        instance.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var position = results[0].geometry.location;
                instance.init(position.lat(), position.lng(), address);

                instance.initCurrentMarker(position);
                callback(position);
            } else {
                alert(errorText);
            }
        });
    };
//        this.searchLL = function (latlng, callback, errorText) {
//        instance.geocoder = new google.maps.Geocoder();
//        instance.geocoder.geocode({ 'latlng': latlng }, function (results, status) {
//            if (status == google.maps.GeocoderStatus.OK) {
//                var addressComponents = result[1].address_components;
//                for (i = 0; i < addressComponents.length; i++) {
//                    if (addressComponents[i].types[0] == "administrative_area_level_1") {
//                        $("#City").val(addressComponents[i].long_name);
//                        instance.initCurrentMarker(position);
//                        
//                        break;
//                    }
//                }
//                //if (result[1]) { $("#Address").html(result[1].formatted_address); }
//            }
//        });


 //   };

};
biletino.views.MapView = new function () {
    this.mapCanvas = function () {
        return document.getElementById("map_canvas");
    };

    this.Latitude = function () {
        return $("#Latitude");
    };

    this.Longitude = function () {
        return $("#Longitude");
    };

};
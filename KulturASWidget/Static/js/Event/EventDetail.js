﻿$(function () {
    $(".tickets-select").change(function () {
        var selected = $(this).val();
        if (selected == "Adet") {
            $(this).val("1");
        } else {
            $(this).closest(".tbody").find(".tickets-select").val("Adet");
            $(this).val(selected);
        }
    });
    $(".tickets-select").first().val("1");
}); //end ready    
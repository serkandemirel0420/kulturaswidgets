﻿biletino.views.event.eventcreate = new function () {

    var instance = this;
    this.options = {
        haserror: false,
        timePickerOptions: {
            timeFormat: 'H:i',
            minTime: '00:00',
            maxTime: '24:00'
        },
        language: 'tr',
        ckeditor: {
            height: 200,
            width: 634
        },
        tooltipPlacement: 'right'


    };

    this.init = function (options) {

        $.extend(this.options, options);
        instance = this;


        $("#commenceHourPicker").timepicker(instance.options.timePickerOptions);
        $("#endHourPicker").timepicker(instance.options.timePickerOptions);

        CKEDITOR.config.charcount_limit = 8000;
        CKEDITOR.replace('event_description', {
            toolbar:
        [
        { name: 'basicstyles', items: ['Bold', '-', 'Italic', '-', 'Underline', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'insert', items: ['Image'] }
        ],
            removePlugins: 'elementspath,maximize,devtools', //,devtools
            skin: 'moono-dark',
            language: instance.options.language,
            resize_enabled: true,
            width: instance.options.ckeditor.width,
            height: instance.options.ckeditor.height

        });
    };
};






  
﻿$(function () {
    $(".tags .tag").each(function (index, element) {
        $(element).click(function () {
            tagFilter($(this));
        });
    });
    $("#monthSelect").change(function () {
        monthFilter($(this).val());
    });

    $("#citySelect").change(function () {
        cityFilter($(this).val());
    });


    selectedFilters = new Array();
    selectedMonthFilter = "";
    selectedCityFilter = "";
});


var selectedFilters;
function tagFilter(item) {


    var tag = item.attr('tag');
    var tagFilterStr = "";
    tagFilterStr = "tag" + tag;

    if ($(item).hasClass("active")) {

        addFilterToList(tagFilterStr);
    }
    else {

        removeFilterFromList(tagFilterStr);
    }

    applyFilters();
};

var selectedMonthFilter;

function monthFilter(month) {

    if (month != "") {
        selectedMonthFilter = ".mnt" + month;
    }
    else {
        selectedMonthFilter = "";
    }
    applyFilters();
}

var selectedCityFilter;


function cityFilter(city) {

    if (city != "") {
        selectedCityFilter = ".ct" + city;
    }
    else {
        selectedCityFilter = "";
    }
    applyFilters();
}


function applyFilters() {
    if (selectedFilters.length == 0 && selectedMonthFilter == "" && selectedCityFilter == "") {
        showAllEvents();
    }
    else {
        hideAllEvents();
        var item;
        var selectorText = "";
        var i = 0;
        var lenght = selectedFilters.length;
        for (i = 0; i < lenght; i++) {
            if (i != lenght - 1) {
                selectorText += "." + selectedFilters[i] + ",";
            }
            else {
                selectorText += "." + selectedFilters[i];
            }
        }
        var items;
        if(lenght > 0)
        {
            items = $(selectorText);
        }
        else
        {
            items = getAllEvents();
        }
        if (selectedMonthFilter != "") {

            items = $(items).filter(selectedMonthFilter);
        }

        if (selectedCityFilter != "") {
            items = $(items).filter(selectedCityFilter);
        }

        $(items).each(function () {
            $(this).show();
        });
    }

}

function addFilterToList(filter) {

    selectedFilters.push(filter);
}

function removeFilterFromList(filter) {

    var index = $.inArray(filter, selectedFilters);
    if (index > -1) {
        selectedFilters.splice(index, 1);
    }
}

function showAllEvents() {
    getAllEvents().each(function (index, element) {
        $(element).show();
    });
}

function hideAllEvents() {
    getAllEvents().each(function (index, element) {
        $(element).hide();
    });
}

function toggleAllEvents() {
   getAllEvents().each(function (index, element) {
        $(element).toggle();
    });

};

function getAllEvents()
{
    return $(".event_list .event");
}
﻿$(function () {
    //// Fill the tags of Edit Event
    if ($('#strTags').length != 0) {
        var strTagsValue = $('#strTags').val();
        strtags = strTagsValue;

        $(".tags a").each(function (index, element) {
            var tag = $(element).attr("tag");
            if (strTagsValue.indexOf(tag) != -1) {
                $(element).addClass("active");
            }

        });
    }
    /********* Create Event Tags Fill **************************/
    var strtags = ":";
    var fill = function fillTag(e) {
        $('#scrollTag').removeClass('invalid');
        $('#scrollTag').addClass('valid');

        if (typeof e == 'number') {
            var con = $(this).hasClass('active');
        } else {
            var con = $(this).hasClass('active') == false;
            e.preventDefault();
        }

        var tag = $(this).attr("tag");
        if (con) {
            var c = $(this).css('border-left-color');
            $(this).css('background', c);
            strtags += tag + ":";
            $("#strTags").val(strtags);

            if (!$(this).hasClass('active'))
                $(this).addClass('active');
        } else {
            $(this).css('background', '#FFF');
            $(this).removeClass('active');
            strtags = strtags.replace(tag + ":", "");
            $("#strTags").val(strtags);
        }

    };

    $('.tags .active').each(fill);
    $('.tags .tag').click(fill);
    /************************************************************/




});


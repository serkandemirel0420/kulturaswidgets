﻿biletino.views.user.usertickets = new function () {

    var instance = this;
    this.options = {
        haserror: false,
        errorReservationID: 0,
        messages: {
            noPhoneNumber: "Phone Number Is Needed!"
        }
    };

    this.init = function (options) {

        $.extend(instance.options, options);
        instance = this;
        $(".showResend").each(function (index, item) {
            $(this).click(function () {
                var resid = $(this).attr("resid");
                var residSelector = '.resendTarget[resid="' + resid + '"]';
                $(residSelector).siblings(".resendTarget").hide();
                $(residSelector).toggle();
            });
        });
        $(".showAnswers").each(function (index, item) {
            $(this).click(function () {
                $(this).closest("td").find(".questionAnswers").show();
            });
        });


        $('.sendMethodSMS').each(function (index, item) {
        
            $(this).change(function () {
                var phoneNumberTextBox = $(this).closest("form").find('[name="AttendeePhone"]');
                var phoneNumberValue = $(phoneNumberTextBox).val();
                if (phoneNumberValue.length == 0) {
                    $(phoneNumberTextBox).closest(".questionAnswers").show();
                    $(phoneNumberTextBox).focus();
                    var errorhtml = '<div class="span4">' + utils.buildMessageHTML("error", instance.options.messages.noPhoneNumber) + '</div>';
                    $(errorhtml).insertAfter($(phoneNumberTextBox).parent());
                }
            });
        });
        if (instance.options.haserror) {
            $('html, body').animate({
                scrollTop: $('tr[resid="' + instance.options.errorReservationID + '"]').offset().top - $(".header_fixed").height() - 15
            }, 1000);
        }
    };
};


﻿//Event Create*****************************
var dateCount = 1;
$(function () {
    $("#publicRadio").click(function () {
        $("#eventPassTxt").attr("style", "display:none;");
    });
    $("#privateRadio").click(function () {
        $("#eventPassTxt").attr("style", "display:block;");
    });
    $("#commenceHourPicker").timepicker({
        'timeFormat': 'H:i',
        'minTime': '00:00',
        'maxTime': '24:00'
    });
    $("#endHourPicker").timepicker({
        'timeFormat': 'H:i',
        'minTime': '00:00',
        'maxTime': '24:00'
    });
    $("#iconid").click(function (e) {
        if (dateCount != 1) {
            $(this).closest('tr').remove();
            dateCount--;
        } else {
            e.preventDefault();
        }
    });
    /******** CKEDITOR CONFIG *****************************************************************************************************************/
    try {
       /* CKEDITOR.stylesSet.add('my_custom_style', [
			{ name: 'My Custom Block', element: 'h3', styles: { color: 'blue'} },
			{ name: 'My Custom Inline', element: 'span', attributes: { 'class': 'mine'} }
		  ]);*/

        CKEDITOR.replace('event_description', {
            toolbar:
            [
            { name: 'basicstyles', items: ['Bold', '-', 'Italic', '-', 'Underline', '-', 'RemoveFormat'] },
            { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
            { name: 'insert',items:['Image']}
            ],
            removePlugins: 'elementspath,maximize,devtools',//,devtools
            skin:'moono-dark',
            language: 'tr',
            resize_enabled: true,
            width: 634,
            height: 200,
            
        });

        var ckeditor1 = CKEDITOR.instances['event_description'];
        ckeditor1.on('focus', function () {
            $('.create_event_yb .arrow').animate({
                top: 202
            }, 400);
            $('.event_name_yb_content').hide();
            $('.event_description_yb_content').show().children('.hidden').hide();
        });

        $('.event_name input').focus(function () {
            $('.create_event_yb .arrow').animate({
                top: 67
            }, 400);
            $('.event_name_yb_content').show().children('.hidden').hide();
            $('.event_description_yb_content').hide();
        });

    } catch (e) {
        //console.log(e);
    }
    /*****************************************************************************************************************************************/


});
function configureDates(className, to, from) {
    $('.' + className + '').datepicker({
        dateFormat: 'dd/mm/yy',
        dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        dayNamesMin: ['PZ', 'PT', 'SL', 'ÇŞ', 'PŞ', 'CM', 'CT'],
        dayNamesShort: ['PAZ', 'PTS', 'SAL', 'ÇRŞ', 'PRŞ', 'CUM', 'CTS'],
        prevText: 'Önceki',
        nextText: 'Sonraki',
        monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        monthNamesShort: ['OCA', 'ŞUB', 'MAR', 'NİS', 'MAY', 'HAZ', 'TEM', 'AĞU', 'EYL', 'EKİ', 'KAS', 'ARA'],
        firstDay: 1,
        currentText: 'Bugun',
        showButtonPanel: true,
        duration: 0,
        closeText: 'Tamam',
        onSelect: function (dateText, inst) {
            var dateObject = $(this).val();
            if (inst.id === to) {
                $('.datepickers').filter($('input#' + from + '')).datepicker("option", "minDate", dateText);
            }
        }
    });
}
function randString(n) {
    if (!n) {
        n = 5;
    }

    var text = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (var i = 0; i < n; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
}
var previousTos = new Array();
var previousFrom = new Array();
previousTos.push("to");
previousFrom.push("from");
function addDate() {
    dateCount++;
    var className = randString(5);
    var to = randString(4);
    var from = randString(4);
    var iconid = randString(5);
    var commencetime = randString(5);
    var endtime = randString(5);
    var date = '<tr>' +
                                        '<td>' +
                                            '<div>' +
                                                '<div>' +
                                                    '<input type="text" class="datepickers" id="' + to + '" />' +
                                                     '<input type="text" id="' + commencetime + '" class="hour_picker" /> ' +
                                                 '</div>' +
                                            '</div>' +
                                        '</td>' +
                                        '<td>' +
                                            '<div>' +
                                                '<div>' +
                                                    '<input type="text" class="datepickers" id="' + from + '" />' +
                                                    '<input type="text" id="' + endtime + '" class="hour_picker" />' +
                                            '</div>' +
                                        '</td>' +
                                        '<td>' +
                                            '<i class="icon-remove" id="' + iconid + '"></i>' +
                                        '</td>' +
                                    '</tr>';
    $("#dateTbody").append(date);
    configureDates("datepickers", to, from);
    $("#" + commencetime + "").timepicker({
        'timeFormat': 'H:i',
        'minTime': '00:00',
        'maxTime': '24:00'
    });
    $("#" + endtime + "").timepicker({
        'timeFormat': 'H:i',
        'minTime': '00:00',
        'maxTime': '24:00'
    });
    for (var i = 0; i < previousTos.length; i++) {
        $("#" + previousTos[i] + "").attr("class", "datepickers");
        $("#" + previousFrom[i] + "").attr("class", "datepickers");
        configureDates("datepickers", previousTos[i], previousFrom[i]);
    }
    previousTos.push(to);
    previousFrom.push(from);
    $("#" + iconid + "").click(function () {
        if (dateCount != 1) {
            $(this).closest('tr').remove();
            dateCount--;
        }
    });
}

$(function () {
    $(".icon-remove").click(function () {
        if (this.id != "iconid") {
            $(this).parent().parent().parent().remove();
        }
    });
    $("[tooltip=removeQuesTtip]").tooltip({
        placement: 'right',
        title: 'Soruyu kaldırmak için tıklayın...'
    });
    $("[tooltip=removeAnsTtip]").tooltip({
        placement: 'right',
        title: 'Bu cevabı kaldırmak için tıklayın...'
    });
});
function removeAns(a) {
    $(a).parent().parent().remove();
}
function addQuestion() {
    $("#questionAddingBtn").find('.br').after(generateQuestionArea());
    $(".icon-remove").click(function () {
        $(this).parent().parent().parent().remove();

    });
}
function addAnswer(a) {
    $(a).parent().parent().before(generateAnswerArea());
    $("[tooltip=removeAnsTtip]").tooltip({
        placement: 'right',
        title: 'Bu cevabı kaldırmak için tıklayın...'
    });
}
function generateAnswerArea() {
    var html = '<div class="row-fluid">' +
                                        '<div class="span2">' +
                                             '<i class="icon-remove" tooltip="removeAnsTtip" onclick="removeAns(this);" style="position: absolute; margin-left: 696px; margin-top: 8px;">' +
                                             '</i>' +
                                        '</div>' +
                                        '<div class="span10">' +
                                            '<input type="text" class="answer" placeholder="Cevap Giriniz..." class="text" style="width: 100%;" />' +
                                        '</div>' +
                                    '</div>';

    return html;
}
function generateQuestionArea() {
    var html = '<div class="well no-box-shadow">' +
                                        '<div class="row-fluid">' +
                                            '<div class="span1">' +
                                                '<label style="float: right; margin-top: 5px;">' +
                                                    'Soru:</label>' +
                                            '</div>' +
                                            '<div class="span10">' +
                                                '<i class="icon-remove" style="position: absolute; margin-left: 740px; margin-top: -10px;">' +
                                                '</i>' +
                                                    '<div class="row-fluid">' +
                                                        '<div class="span12">' +
                                                            '<input type="text" class="question" placeholder="Soru Giriniz..." style="width: 100%;">' +
                                                        '</div>' +
                                                    '</div>' +
                                                    '<div class="row-fluid">' +
                                                        '<div class="span1">' +
                                                            '<i class="icon-plus-sign"></i>' +
                                                        '</div>' +
                                                        '<div class="span10" style="margin-left: -25px;">' +
                                                            '<label class="text-info" onclick="addAnswer(this);" style="float: left;">' +
                                                                'Cevap Ekle</label>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>';

    return html;

}

//Event Create*****************************
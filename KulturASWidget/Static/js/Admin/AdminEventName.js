﻿biletino.views.admin.eventNameEdit = new function () {
    var instance = this;
    this.options = {
        editEventNameURL: '',
        eventID: ''
    };

    this.btnEditEvent = "#editEventHref";
    this.btnEditEventCancel = "#btnEditEventNameCancel";
    this.btnEditEventNameSave = "#btnEditEventNameSave";
    this.loading = "#eventNameLoading";

    this.init = function (options) {
        $.extend(this.options, options);

        $(this.btnEditEvent).click(this.btnEditNameClick);
        $(this.btnEditEventCancel).click(this.btnEditNameCancelClick);
        $(this.btnEditEventNameSave).click(this.btnEditNameSaveClick);

    };

    this.btnEditNameClick = function () {
        $(this).closest(".row-fluid").hide();
        $("#EventNameEdit").show();
    };
    this.btnEditNameCancelClick = function () {
        $(instance.btnEditEvent).closest(".row-fluid").show();
        $("#EventNameEdit").hide();
    };
    this.btnEditNameSaveClick = function () {

        $(instance.loading).show();
        $(instance.btnEditEventNameSave).hide();
        $(instance.btnEditEventCancel).hide();
        var name = $("#EventNameEdit").find("input").val();
        $("#EventNameEdit").find("input").attr("disabled", "disabled");
        utils.makeGetRequest(instance.options.editEventNameURL, { eventID: instance.options.eventID, name: name }, function (data) {
            $("#eventName").html(name);

            $("#EventNameEdit").hide();
            $(instance.btnEditEvent).closest(".row-fluid").show();
            $("#EventNameEdit").find("input").removeAttr("disabled");
            $(this.loading).hide();
        });
    };
};

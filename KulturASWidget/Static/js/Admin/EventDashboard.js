﻿biletino.views.admin.eventDashboard = new function () {

    var instance = this;
    this.options = {
        language: 'tr',
        ckeditor: {
            height: 200,
            width: 476
        },
        eventID: '',
        editDetailURL: '',

        editDatesURL: '',
        editSpecialURL: '',
        editTagsURL: '',
        editQuestionsURL: '',

        dateError: '',
        specialError: '',
        tagsError: '',
        detailError: ''
    };

    this.eventDetailLoading = "#eventDetailLoading";
    this.eventDatesLoading = "#eventDatesLoading";
    this.eventTagsLoading = "#eventTagsLoading";
    this.eventSpecialLoading = "#eventSpecialLoading";

    this.init = function (options) {
        $.extend(this.options, options);

        CKEDITOR.config.charcount_limit = 8000;
        CKEDITOR.replace('txtEventDashboardDetails', {
            toolbar:
        [
        { name: 'basicstyles', items: ['Bold', '-', 'Italic', '-', 'Underline', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'] },
        { name: 'insert', items: ['Image'] }
        ],
            removePlugins: 'elementspath,maximize,devtools', //,devtools
            skin: 'moono-dark',
            language: instance.options.language,
            resize_enabled: true,
            width: instance.options.ckeditor.width,
            height: instance.options.ckeditor.height
        });
        utils.datetimePickerRangeInit("#CommenceDate", "#EndDate", {});
        $("#CommenceDate").datepicker("option", "minDate", "01/01/2012");

        $("#commenceHourPicker").timepicker({
            'timeFormat': 'H:i',
            'minTime': '00:00',
            'maxTime': '24:00'
        });
        $("#endHourPicker").timepicker({
            'timeFormat': 'H:i',
            'minTime': '00:00',
            'maxTime': '24:00'
        });

        $("#editEventDetailHref").click(this.btnEditDetailClick);
        $("#btnEditDetailCancel").click(this.btnEditDetailCancelClick);
        $("#btnEditDetailSave").click(this.btnEditDetailSaveClick);

        $("#editEventDateHref").click(this.btnEditDates);
        $("#btnEditDatesCancel").click(this.btnEditDatesCancel);
        $("#btnEditDatesSave").click(this.btnEditDatesSave);

        $("#editEventSpecialHref").click(this.btnEditSpecialClick);
        $("#btnEditSpecialCancel").click(this.btnEditSpecialCancelClick);
        $("#btnEditSpecialSave").click(this.btnEditSpecialSaveClick);

        $("#editEventTagHref").click(this.btnEditTagsClick);
        $("#btnEditTagsCancel").click(this.btnEditTagsCancelClick);
        $("#btnEditTagsSave").click(this.btnEditTagsSaveClick);

    };

    this.btnEditDetailClick = function () {
        $(this).hide();
        $("#eventDetail").show();
        $("#detailShort").hide();
        $("#editDetailButtons").show();
    };
    this.btnEditDetailSaveClick = function () {

        $(instance.eventDetailLoading).show();
        $("#btnEditDetailSave").hide();
        $("#btnEditDetailCancel").hide();


        var detail = "";
        if (CKEDITOR.instances.txtEventDashboardDetails) {
            detail = CKEDITOR.instances.txtEventDashboardDetails.getData();
        }
        utils.makeGetRequest(instance.options.editDetailURL, { eventID: instance.options.eventID, detail: detail },
        function (data) {
            $("#editEventDetailHref").show();
            $("#eventDetail").hide();
            $("#detailShort").html(detail);
            $("#detailShort").show();
            $("#editDetailButtons").hide();
            $("#btnEditDetailSave").show();
            $("#btnEditDetailCancel").show();
            $(instance.eventDetailLoading).hide();
            $(instance.options.detailError).hide();
        }, function (data) {
            $("#btnEditDetailSave").show();
            $("#btnEditDetailCancel").show();
            $(instance.eventDetailLoading).hide();
            $(instance.options.detailError).show();
            $(instance.options.detailError).html(data);

        });
    };
    this.btnEditDetailCancelClick = function () {
        $("#editEventDetailHref").show();
        $("#eventDetail").hide();
        $("#editDetailButtons").hide();
        $("#detailShort").show();
    };

    this.btnEditDates = function () {
        $(this).hide();
        $("#dateShort").hide();
        $("#editDateButtons").show();
        $("#dateTable").show();
    };
    this.btnEditDatesCancel = function () {
        $("#editEventDateHref").show();
        $("#editDateButtons").hide();
        $("#dateShort").show();
        $("#dateTable").hide();
    };
    this.btnEditDatesSave = function () {

        $("#btnEditDatesCancel").hide();
        $("#btnEditDatesSave").hide();
        $(instance.eventDatesLoading).show();
        var commenceDate = $("#CommenceDate").val();
        var commenceHour = $("#commenceHourPicker").val();
        var endDate = $("#EndDate").val();
        var endHour = $("#endHourPicker").val();
        utils.makeGetRequest(instance.options.editDatesURL, {
            eventID: instance.options.eventID,
            commenceDate: commenceDate,
            commenceHour: commenceHour,
            endDate: endDate,
            endHour: endHour
        }, function (data) {
            $("#btnEditDatesCancel").show();
            $("#btnEditDatesSave").show();
            $(instance.options.dateError).hide();
            $(instance.eventDatesLoading).hide();
            $("#editDateButtons").hide();
            $("#editEventDateHref").show();
            $("#dateTable").hide();
            var datas = JSON.parse('[' + data + ']')[0];
            $("#dateShort").html("Başlangıç: " + datas.start + ", Bitiş: " + datas.end);
            $("#dateShort").show();
        }, function (data) {
            $("#btnEditDatesCancel").show();
            $("#btnEditDatesSave").show();
            $(instance.eventDatesLoading).hide();

            $(instance.options.dateError).html(data);
            $(instance.options.dateError).show();
        });
    };




    this.btnEditSpecialClick = function () {
        $(this).hide();
        $("#specialShort").hide();
        $("#editSpecialButtons").show();
        $("#specialCheckBoxes").show();




    };

    this.btnEditSpecialCancelClick = function () {
        $("#editEventSpecialHref").show();
        $("#editSpecialButtons").hide();
        $("#specialShort").show();
        $("#specialCheckBoxes").hide();
        $("#passDiv").hide();

    };

    this.btnEditSpecialSaveClick = function () {
        $(instance.eventSpecialLoading).show();

        var active = "False";
        var private = "False";
        var moderationRequired = "False";
        var password = "";
        var eventURL = "";
        if ($("#chkActive").attr("checked") == "checked") {
            active = "True";
        }
        if ($("#privacyCheckBox").attr("checked") == "checked") {
            private = "True";
            password = $("#password").val();
        }
        if ($("#chkModerationRequired").attr("checked") == "checked") {
            moderationRequired = "True";
        }

        eventURL = $("#eventURL").val();

        $("#btnEditSpecialCancel").hide();
        $("#btnEditSpecialSave").hide(); $("#specialAreaError").hide();
        //chkActive privacyCheckBox chkModerationRequired
        utils.makeGetRequest(instance.options.editSpecialURL, {
            eventID: instance.options.eventID,
            active: active,
            isPrivate: private,
            password: password,
            moderationRequired: moderationRequired,
            eventURL: eventURL
        }, function (data) {
            var data = JSON.parse(data);
            $("#specialShort").html(data.display);
            $(instance.eventSpecialLoading).hide();
            $("#specialShort").show();
            $("#specialCheckBoxes").hide();
            $("#editSpecialButtons").hide();
            $("#btnEditSpecialCancel").show();
            $("#btnEditSpecialSave").show();
            $("#editEventSpecialHref").show();
            //TODO
            $("#eventURL").val(data.eventURL)
            $("#btnGotoEventDetail").attr("href", data.detailPageURL);
            $(instance.options.specialError).hide();

        }, function (data) {
            $(instance.eventSpecialLoading).hide();
            $("#btnEditSpecialCancel").show();
            $("#btnEditSpecialSave").show();
            $(instance.options.specialError).show();
            $(instance.options.specialError).html(data);

        });

    };


    this.btnEditTagsClick = function () {
        $(this).hide();
        $("#tagShort").hide();
        $("#tagSelect").show();
    };
    this.btnEditTagsCancelClick = function () {
        $("#editEventTagHref").show();
        $("#tagShort").show();
        $("#tagSelect").hide();
    };
    this.btnEditTagsSaveClick = function () {

        $("#btnEditTagsCancel").hide();
        $("#btnEditTagsSave").hide();
        $(instance.eventTagsLoading).show();
        var tags = $("#StrTags").val();
        utils.makeGetRequest(instance.options.editTagsURL, {
            eventID: instance.options.eventID,
            tags: tags
        }, function (data) {
            $("#tagShort").html(data);
            $("#tagSelect").hide();
            $("#editEventTagHref").show();
            $("#tagShort").show();
            $("#btnEditTagsCancel").show();
            $("#btnEditTagsSave").show();
            $(instance.eventTagsLoading).hide();
        }, function (data) {
            $(instance.eventTagsLoading).hide();
            $("#btnEditTagsCancel").show();
            $("#btnEditTagsSave").show();
            $(instance.options.tagsError).html(data);
            $(instance.options.tagsError).show();

        });
    };

};


$(function () {

    $(".icon-remove").click(function () {
        if (this.id != "iconid") {
            $(this).parent().parent().parent().remove();
        }
    });
    $("[tooltip=removeQuesTtip]").tooltip({
        placement: 'right',
        title: 'Soruyu kaldırmak için tıklayın...'
    });
    $("[tooltip=removeAnsTtip]").tooltip({
        placement: 'right',
        title: 'Bu cevabı kaldırmak için tıklayın...'
    });



    $('#privacyCheckBox').change(function () {
        if ($('#privacyCheckBox').attr("checked") == "checked") {
            $('#passDiv').show();
        } else {
            $('#passDiv').hide();
            $('#isprivateTxt').val(false);
        }
    });

    /*
    var tagStorage = '#StrTags';
    $('.eventTag').click(function () {
    var tag = $(this).html();
    if ($(this).hasClass('active')) {
    $(this).removeClass('active');
    tag += ':';
    var storageVal = $(tagStorage).val().replace(tag, '');
    $(tagStorage).val(storageVal);
    } else {
    $(this).addClass('active');
    tag += ':';
    storageVal = $(tagStorage).val() + tag;
    $(tagStorage).val(storageVal);
    }
    });*/
});




///////////

/////




function removeAns(a) {
    $(a).parent().parent().remove();
}
function addQuestion() {
    $("#questionAddingBtn").find('.br').after(generateQuestionArea());
    $(".icon-remove").click(function () {
        $(this).parent().parent().parent().remove();

    });
}
function addAnswer(a) {
    $(a).parent().parent().before(generateAnswerArea());
    $("[tooltip=removeAnsTtip]").tooltip({
        placement: 'right',
        title: 'Bu cevabı kaldırmak için tıklayın...'
    });
}
function generateAnswerArea() {
    var html = '<div class="row-fluid">' +
                                        '<div class="span2">' +
                                             '<i class="icon-remove" tooltip="removeAnsTtip" onclick="removeAns(this);" style="position: absolute; margin-left: 696px; margin-top: 8px;">' +
                                             '</i>' +
                                        '</div>' +
                                        '<div class="span10">' +
                                            '<input type="text" class="answer" placeholder="Cevap Giriniz..." class="text" style="width: 100%;" />' +
                                        '</div>' +
                                    '</div>';

    return html;
}
function generateQuestionArea() {
    var html = '<div class="well no-box-shadow">' +
                                        '<div class="row-fluid">' +
                                            '<div class="span1">' +
                                                '<label style="float: right; margin-top: 5px;">' +
                                                    'Soru:</label>' +
                                            '</div>' +
                                            '<div class="span10">' +
                                                '<i class="icon-remove" style="position: absolute; margin-left: 740px; margin-top: -10px;">' +
                                                '</i>' +
                                                    '<div class="row-fluid">' +
                                                        '<div class="span12">' +
                                                            '<input type="text" class="question" placeholder="Soru Giriniz..." style="width: 100%;">' +
                                                        '</div>' +
                                                    '</div>' +
                                                    '<div class="row-fluid">' +
                                                        '<div class="span1">' +
                                                            '<i class="icon-plus-sign"></i>' +
                                                        '</div>' +
                                                        '<div class="span10" style="margin-left: -25px;">' +
                                                            '<label class="text-info" onclick="addAnswer(this);" style="float: left; cursor:pointer;">' +
                                                                'Cevap Ekle</label>' +
                                                        '</div>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</div>';

    return html;

}
               
/**
* @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    config.language = 'tr';
    // config.uiColor = '#AADC6E';
    config.skin = 'moono';
    config.extraPlugins = 'charcount,pastefromword';
    config.charcount_format = "%count% / %limit%";
   
};

CKEDITOR.on('dialogDefinition', function (ev) {
    // Take the dialog name and its definition from the event data.
    var dialogName = ev.data.name;
    var dialogDefinition = ev.data.definition;

    // Check if the definition is from the dialog we're
    // interested in (the 'image' dialog). This dialog name found using DevTools plugin
    if (dialogName == 'image') {
        // Remove the 'Link' and 'Advanced' tabs from the 'Image' dialog.
        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');

        dialogDefinition.contents.push({
            label: "Resim Y&#252;kleme",
            title: "title",
            id: "imageUpload",
            elements: [
                {
                    id: 'htmlinfo',
                    type: 'html',
                    label: 'HTML FIELD',
                    html: '<h4 style="font-size: 35px;text-align: center;">Yak&#305;nda!</h4>'
                }
            ]
        });

        // Get a reference to the 'Image Info' tab.
        //var infoTab = dialogDefinition.getContents('info');

        // Remove unnecessary widgets/elements from the 'Image Info' tab.         
        //infoTab.remove('txtHSpace');
        //infoTab.remove('txtVSpace');
    }
});

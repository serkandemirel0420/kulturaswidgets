﻿var utils;

$(document).ready(function () {
    $("#submitBtn").click(function () {
            $("#submitBtn").button('loading');
        });
    utils = new Utils();
});


Utils = function () {

    var instance = this;
    this.options = {
        tooltipSelector: '.popupTooltip',
        tooltipPlacement: 'right'
    };
    this.init = function () {
        this.arrangeErrors();
        this.initOnlyCapital();
        this.initTooltips(this.options.tooltipSelector);
    };

    this.integerRegex = function () {
        return "([0-9-]+)";
    };

    this.defaultDateFormat = function () {
        return "dd/mm/yy";
    };

    this.defaultTimeFormat = function () {
        return "hh:mm";
    };


    this.getElement = function (id) {
        return $("#" + id);
    };

    this.getElementsByClass = function (className) {
        return $("." + className);
    };

    this.getElementsByType = function (type) {
        return $(type);
    };

    this.getElementValue = function (id) {
        return instance.getElement(id).val();
    };

    this.getElementSelectedValue = function (id) {
        return $("#" + id + " :selected").val();
    };

    this.getElementHtml = function (id) {
        return instance.getElement(id).html();
    };

    this.bindClickEventByElement = function (element, callback) {
        element.unbind("click");
        element.click(callback);
    };

    this.bindChangeEvent = function (id, callback) {
        var item = instance.getElement(id);
        item.unbind("click");
        item.change(callback);
    };


    this.bindClickEvent = function (id, callback) {
        var item = instance.getElement(id);
        item.unbind("click");
        item.click(callback);
    };

    this.bindClickEventClass = function (id, callback) {
        var item = instance.getElementsByClass(id);
        item.unbind("click");
        item.click(callback);
    };



    this.makeGetRequest = function (url, parameters, callback,error) {
        if (url.substring(0, 4) != "http") {
            url = appRoot + url;
        }
        var ajaxCallObject = instance.createAjaxCallObject("GET", url, parameters, callback,error);
        $.ajax(ajaxCallObject);
    };

    this.makePostRequest = function (url, parameters, callback) {
        if (url.substring(0, 4) != "http") {
            url = appRoot + url;
        }
        var ajaxCallObject = instance.createAjaxCallObject("POST", url, parameters, callback);
        $.ajax(ajaxCallObject);
    };

    this.createAjaxCallObject = function (type, url, parameters, callback,error) {
        var ajaxCallObject = {
            type: type,
            url: url,
            success: function (data) {
                callback(data);
            },
            error:function(data,type,statusText)
                {
                    if(error != null && error != undefined)
                    {
                        error(data.responseText);
                    }
                },
            data: parameters,
            statusCode: {
                401: function () {
                    biletino.common.alertError("Session Expired",
							function () {
							    window.location = appRoot + "/login";
							});
                }
            }
        };

        return ajaxCallObject;
    };
    this.replaceTurkishChars= function (result){
            result = result.replace(/&Ouml;/g, "Ö");
            result = result.replace(/&#214;/g, "Ö");
            result = result.replace(/&ouml;/g, "ö");
            result = result.replace(/&#246;/g, "ö");
            result = result.replace(/&Uuml;/g, "Ü");
            result = result.replace(/&#220;/g, "Ü");
            result = result.replace(/&uuml;/g, "ü");
            result = result.replace(/&#252;/g, "ü");
            result = result.replace(/&Ccedil;/g, "Ç");
            result = result.replace(/&#199;/g, "Ç");
            result = result.replace(/&ccedil;/g, "ç");
            result = result.replace(/&#231;/g, "ç");
            result = result.replace(/&nbsp;/g, " ");
            result = result.replace(/&#304;/g, "İ");
            result = result.replace(/&#305;/g, "ı");
            result = result.replace(/&#286;/g, "Ğ");
            result = result.replace(/&#287;/g, "ğ");
            result = result.replace(/&#350;/g, "Ş");
            result = result.replace(/&#351;/g, "ş");
            result = result.replace(/&#305;/g, "ı");
            return result;
    };

    this.convertToEnglishChars = function (sData){
 
	var result = sData;
	result = result.replace(/Ãœ/g,"U");
	result = result.replace(/Å/g,"S");
	result = result.replace(/Ä/g,"G");
	result = result.replace(/Ã‡/g,"C");
	result = result.replace(/Ä°/g,"I");
	result = result.replace(/Ã–/g,"O");
	result = result.replace(/Ã¼/g,"u");
	result = result.replace(/ÅŸ/g,"s");
	result = result.replace(/Ã§/g,"c");
	result = result.replace(/Ä±/g,"i");
	result = result.replace(/Ã¶/g,"o");
	result = result.replace(/ÄŸ/g,"g");

	result = result.replace(/Ü/g,"U;");
	result = result.replace(/Ş/g,"S");
	result = result.replace(/Ğ/g,"G");
	result = result.replace(/Ç/g,"C");
	result = result.replace(/İ/g,"I");
	result = result.replace(/Ö/g,"O");
	result = result.replace(/ü/g,"u");
	result = result.replace(/ş/g,"s");
	result = result.replace(/ç/g,"c");
	result = result.replace(/ı/g,"i");
	result = result.replace(/ö/g,"o");
	result = result.replace(/ğ/g,"g");

	result = result.replace(/%u015F/g,"s");
	result = result.replace(/%E7/g,"c");
	result = result.replace(/%FC/g,"u");
	result = result.replace(/%u0131/g,"i");
	result = result.replace(/%F6/g,"o");
	result = result.replace(/%u015E/g,"S");
	result = result.replace(/%C7/g,"C");
	result = result.replace(/%DC/g,"U");
	result = result.replace(/%D6/g,"O");
	result = result.replace(/%u0130/g,"I");
	result = result.replace(/%u011F/g,"g");
	result = result.replace(/%u011E/g,"G");

return result;
}

    this.generateRandomAlphanumeric = function (length) {

        if (!length) {
            length = 5;
        }

        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (var i = 0; i < n; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    };

    this.initTooltips = function (selector) {
        $(selector).each(function () {
            var text = $(this).attr("tooltip");
            $(this).tooltip({
                placement: instance.options.tooltipPlacement,
                title: text
            });

        });

    };


    //default datetimepicker options
    this.datepickerOptions = {
        dateFormat: 'dd/mm/yy',
        dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        dayNamesMin: ['PZ', 'PT', 'SL', 'ÇŞ', 'PŞ', 'CM', 'CT'],
        dayNamesShort: ['PAZ', 'PTS', 'SAL', 'ÇRŞ', 'PRŞ', 'CUM', 'CTS'],
        prevText: 'Önceki',
        nextText: 'Sonraki',
        monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        monthNamesShort: ['OCA', 'ŞUB', 'MAR', 'NİS', 'MAY', 'HAZ', 'TEM', 'AĞU', 'EYL', 'EKİ', 'KAS', 'ARA'],
        firstDay: 1,
        currentText: 'Bugün',
        duration: 0,
        minDate: new Date(),
        showOtherMonths: true
    };

    //initializes 2 datetimepickers from-to
    this.datetimePickerRangeInit = function (from, to, options) {

        var fromDatePickerOptions = {};
        $.extend(fromDatePickerOptions, this.datepickerOptions, {
            onClose: function (selectedDate) {
                $(to).datepicker("option", "minDate", selectedDate);
                var fromOnClose = options.fromOnClose;
                if (fromOnClose != null && fromOnClose != undefined) {
                    fromOnClose(selectedDate);
                }
            }
        });
        $(from).datepicker(fromDatePickerOptions);

        var toDatePickerOptions = {};
        $.extend(toDatePickerOptions, this.datepickerOptions, {
            onClose: function (selectedDate) {
                $(from).datepicker("option", "maxDate", selectedDate);
                var toOnClose = options.toOnClose;
                if (toOnClose != null && toOnClose != undefined) {
                    toOnClose(selectedDate);
                }
            }
        });
        $(to).datepicker(toDatePickerOptions);
    };

    //info,error,danger,success
    this.buildMessageHTML = function (status, message) {
        return '<div class="validation_message">' +
                    '<div class="alert alert-' + status + '" style="margin-bottom: 0px;padding: 3px;">' +
                        '<p style="margin-bottom:0px">' +
                            '<span class="field-validation-error">' +
                                message +
                            '</span>' +
                        '</p>' +
                    '</div>' +
               '</div>';
    };

    this.templateForImage = function () {
        return '<div class="img_block clear">' +
                        '<span class="img_icon"></span>' +
                        '<p class="text">' +
                            'Resim Yükle</p>' +
                    '</div>';
    };

    this.arrangeErrors = function () {
        $(".field-validation-error").each(function (index, item) {
            $(this).closest(".validation_message").removeClass("valid");
        });
        $(".field-validation-valid").each(function (index, item) {
            $(this).closest(".validation_message").addClass("valid");
        });
    };
    this.initOnlyCapital = function () {
        $(".onlyCapital").each(function (item, index) {
            $(this).keydown(function (event) {
                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
                // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                    return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
            });
        });
    }


};

var biletino;
if (biletino == undefined || biletino === null) {
    biletino = {};
}

if (biletino.common == undefined || biletino.common === null) {
    biletino.common = {};
}

if (biletino.views == undefined || biletino.views === null) {
    biletino.views = {};
}

if (biletino.views.user == undefined || biletino.views.user === null) {
    biletino.views.user = {};
}

if (biletino.views.marketing == undefined || biletino.views.marketing === null) {
    biletino.views.marketing = {};
}
if (biletino.views.event == undefined || biletino.views.event === null) {
    biletino.views.event = {};
}
if (biletino.views.admin == undefined || biletino.views.admin === null) {
    biletino.views.admin = {};
}

if (biletino.services == undefined || biletino.services === null) {
    biletino.services = {};
}
if (biletino.views.imageupload == undefined || biletino.views.imageupload === null) {
    biletino.views.imageupload = {};
}


biletino.services.AjaxService = new function () {
    this.execute = function (callObject) {
        $.ajax(callObject);
    };
};


biletino.imageupload = new function () {
    var instance = this;

    this.options = {
        maxUploadCount: 999,
        ImageInfoInput: "",
        jcropAspectRatio:null
    };

    this.init = function (options) {
        $.extend(this.options, options);

        window.ImageUploadOptions = {
            uploadCompleteCallback: instance.UploadCompleted,
            removeImageCallback:instance.removeImageInfo,
            showCropWindow: instance.ShowCropWindow,
            getUploadedFiles: instance.getPreUploadedFiles,
            getImageInfo: instance.getImageInfo,

            maxUploadCount: instance.options.maxUploadCount //TODO
        };
    };


    this.storedInfo = null;
    this.boundx = 0;
    this.boundy = 0;

    this.jcropCurrent = "";
    this.jcrop_api;
    this.UploadCompleted = function (name) {
        instance.addImageInfo(name, null);
    };

    this.btnCropOK = "#cropDone";

    this.image = function (name, data) {
        this.name = name;
        this.data = data;
        return this;
    }

    this.ShowCropWindow = function (imageSource, callback) {
        
        if (instance.jcrop_api) {
            instance.jcrop_api.destroy();
        }
        $("#target").attr("style", "");
        $("#target").attr("src", imageSource);
        $("#preview").attr("src", imageSource);



        var sp = imageSource.split('/');

        jcropCurrent = sp[sp.length - 1];
        var selected = this.getImageInfo(jcropCurrent);
        var selectInfo = undefined;
        if (selected) {
            if (selected.data) {
                var c = selected.data.preview;
                if(c)
                {
                    selectInfo = [c.x, c.y, c.x2, c.y2];
                }
            }
        }


        var jcropOptions = {
            onChange: updatePreview,
            onSelect: updatePreview,
            boxWidth: 400,
            boxHeight: 400,
            aspectRatio:instance.options.jcropAspectRatio
        };

        $('#target').Jcrop(jcropOptions,function(){
             instance.jcrop_api = this;
             var bounds = this.getBounds();
             boundx = bounds[0];
             boundy = bounds[1];
             if(selectInfo)
             {
                this.setSelect(selectInfo);
             }
             
        });
        /*
        $('#target').Jcrop(, function () {
            // Use the API to get the real image size
            var bounds = this.getBounds();
            boundx = bounds[0];
            boundy = bounds[1];
            // Store the API in the jcrop_api variableS
            instance.jcrop_api = this;
        });*/

       


        $(instance.btnCropOK).unbind("click");
        $(instance.btnCropOK).click(function () {
            callback(storedInfo.preview);
            $("#fancybox-close").click();
            var info = instance.getImageInfo(jcropCurrent);
            if (info) {
                info.data = storedInfo;
            }
            else {
                addImageInfo(jcropCurrent, storedInfo);
            }
            $(instance.options.ImageInfoInput).val(JSON.stringify(imageCropInfo));

        });
        $("#showFancy").fancybox().trigger('click');

        function updatePreview(c) {
            if (parseInt(c.w) > 0) {
                var contW = 100;
                var contH = contW * c.h / c.w;
                var rx = contW / c.w;
                var ry = contH / c.h;

                $('#preview').css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * c.x) + 'px',
                    marginTop: '-' + Math.round(ry * c.y) + 'px'
                });

                $("#cropContainer").css({
                    width: contW + 'px',
                    height: contH + 'px'
                });
                var x = 0;
                var y = 0;
                if (c.x > 0) {
                    x = c.x / boundx;
                }
                if (c.y > 0) {
                    y = c.y / boundy;
                }
                var previewObj = { 
                    boundx: boundx,
                    boundy: boundy,
                };

                $.extend(previewObj,c);

                storedInfo = {
                    x: x,
                    y: y,
                    w: c.w / boundx,
                    h: c.h / boundy,
                    preview: previewObj
                };


            }
        };
    };

    this.imageCropInfo = [];

    this.addImageInfo = function (name, info) {
        imageCropInfo.push(new this.image(name, info));
        $(instance.options.ImageInfoInput).val(JSON.stringify(imageCropInfo));
    }

    this.getImageInfo = function (name) {
        for (var i = 0; i < imageCropInfo.length; i++) {
            var item = imageCropInfo[i];
            if (item.name == name) {
                return item;
            }
        }
        return null;
    }
    this.removeImageInfo = function (name) {
        for (var i = 0; i < imageCropInfo.length; i++) {
            var item = imageCropInfo[i];
            if (item.name == name) {
                imageCropInfo.splice(i, 1);
            }
        }
        $(instance.options.ImageInfoInput).val(JSON.stringify(imageCropInfo));
    }

    this.getPreUploadedFiles = function () {
        var imgInfoStr = $(instance.options.ImageInfoInput).val();
        try
        {
            imageCropInfo = JSON.parse(imgInfoStr);
        }
        catch(err)
        {
            imageCropInfo = [];
        }
       
        
        
        if (imageCropInfo) {
            return imageCropInfo;
        }
        imageCropInfo = [];
        return null;
    };

};

biletino.common.counter = new function(){
    var instance = this;
    this.options = {
         miliseconds:0,
         remainingTimeText:'',
         element:'',
         onfinish : function(){},
         limitseconds: 10,
         onlimit:function(){}
    };
    this.init = function(options){
      instance = this;

      $.extend(this.options,options);

      var currentDate = new Date(),
          finished = false,
          availiableExamples = {
              set3daysFromNow: 3 * 24 * 60 * 60 * 1000,
              set2daysFromNow: 2 * 24 * 60 * 60 * 1000,
              set5minFromNow: 5 * 60 * 1000,
              set1minFromNow: 1 * 60 * 1000,
              set10minFromNow: 10 * 60 * 1000
          };
            var d, h, m, s;
            var isLimitCallback = false;
            var first = true;
            function callback(event) {
                var timeFormat = instance.options.remainingTimeText + " %m:%s"
                $this = $(this);
                if (finished) {
                    $this.fadeTo(0, 1);
                    finished = false;
                }
                switch (event.type) {
                    case "days":
                        d = event.value;
                        break;
                    case "hours":
                        h = event.value;
                        break;
                    case "minutes":
                        m = event.value;
                        break;
                    case "seconds":
                        s = event.value;
                        break;
                    case "finished":
                        //$this.fadeTo('slow', 0.5);
                        finished = true;
                        instance.options.onfinish();
                        break;
                }
                // Assemble time format
                if (d > 0) {
                    timeFormat = timeFormat.replace(/\%d/, d);
                    timeFormat = timeFormat.replace(/\(s\)/, Number(d) == 1 ? '' : 's');
                } else {
                    timeFormat = timeFormat.replace(/\%d day\(s\)/, '');
                }
                //timeFormat = timeFormat.replace(/\%h/, h);
                timeFormat = timeFormat.replace(/\%m/, m);
                timeFormat = timeFormat.replace(/\%s/, s);
                if (!isLimitCallback && !first) {
                 var totalSeconds = 0;
                 if (m) {
                    totalSeconds = parseInt(m) * 60;
                 }
                 if (s) {
                    totalSeconds += parseInt(s);
                 }
                            if(totalSeconds <= instance.options.limitseconds)
                            {
                                instance.options.onlimit();
                                isLimitCallback = true;
                            }                
                }
                if(first)
                {
                first = false;
                }               
                // Display
                $this.html(timeFormat);
            }

            $(this.options.element).countdown(instance.options.miliseconds + currentDate.valueOf(), callback);
  };  
};

/*
biletino.common = new function () {

var instance = this;

this.init = function () {

};

this.createCallObject = function (type, url, data, callback) {
return {
type: type,
url: url,
success: function (data) {
callback(data);
},
data: data,
statusCode: {
401: function () {
common.alertError("Session Expired",
function () {
window.location = appRoot + "/user/userlogon";
});
}
}
}
};

this.alertSuccess = function (text, callBack) {
var alertSuccessDiv = $(document.createElement("div"));
alertSuccessDiv.attr("class", "alert alert-success");
alertSuccessDiv.html(text);
$.jqDialog.alert(alertSuccessDiv, function () {
callBack();
});
};

this.alertInfo = function (text, callBack) {
var alertSuccessDiv = $(document.createElement("div"));
alertSuccessDiv.attr("class", "alert alert-success");
alertSuccessDiv.html(text);
$.jqDialog.alert(alertSuccessDiv, function () {
callBack();
});
};

this.alertError = function (text, callBack) {
var alertErrorDiv = $(document.createElement("div"));
alertErrorDiv.attr("class", "alert alert-error");
alertErrorDiv.html(text);
$.jqDialog.alert(alertErrorDiv, function () {
callBack();
});
};
/*
this.clone = function (obj) {
// Handle the 3 simple types, and null or undefined
if (null == obj || "object" != typeof obj) return obj;

// Handle Date
if (obj instanceof Date) {
var copy = new Date();
copy.setTime(obj.getTime());
return copy;
}

// Handle Array
if (obj instanceof Array) {
var copy = [];
for (var i = 0, len = obj.length; i < len; i++) {
copy[i] = this.clone(obj[i]);
}
return copy;
}

// Handle Object
if (obj instanceof Object) {
var copy = {};
for (var attr in obj) {
if (obj.hasOwnProperty(attr)) copy[attr] = this.clone(obj[attr]);
}
return copy;
}

return null;
}*

    
};
*/
/*
 * jQuery File Upload Plugin Localization Example 6.5
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2012, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

window.locale = {
    "fileupload": {
        "errors": {
            "maxFileSize": "Dosya çok büyük",
            "minFileSize": "Dosya çok küçük",
            "acceptFileTypes": "Desteklenmeyen dosya türü",
            "maxNumberOfFiles": "Yüklenebilecek resim sayısı geçildi. En fazla 4 resim yüklenebilmektedir.",
            "uploadedBytes": "Dosya çok büyük",
            "emptyResult": "Boş dosya"
        },
        "error": "Hata",
        "start": "Başlat",
        "cancel": "İptal Et",
        "destroy": "Sil"
    }
};

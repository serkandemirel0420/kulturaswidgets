﻿biletino.views.imageupload.EventImageUploaderIframe = new function () {

    var instance = this;
    this.options = {
        fileuploadform: "#fileupload",
        fileuploadOptions: {
            maxFileSize: 500000000,
            resizeMaxWidth: 1920,
            resizeMaxHeight: 1200,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            //autoUpload: true,
            maxNumberOfFiles: 4
        },
        uploadCompleteCallback: function () { },
        showCropWindow: function () { },
        getUploadedFiles: function () { },
        getImageInfo: function () { },
        removeImageCallback: function () { }

    };

    this.init = function (options) {
        instance = this;
        $.extend(this.options, options);
        if (options.maxUploadCount) {
            this.options.fileuploadOptions.maxNumberOfFiles = options.maxUploadCount;
        }
        $(this.options.fileuploadform).fileupload();
        $(this.options.fileuploadform).fileupload('option', this.options.fileuploadOptions);

        $(this.options.fileuploadform).bind('fileuploaddestroyed', function (e, data) {
            var sp = data.url.split('=');
            sp = sp[sp.length - 1];
            instance.options.removeImageCallback(sp);
        });

        $(this.options.fileuploadform).bind('fileuploadcompleted', function (event, object) {
            for (var i = 0; i < object.context.length; i++) {
                var obj = object.context[i];
                var name = $(obj).find(".name").find("a").attr("title");

                var preloadedInfo = instance.options.getImageInfo(name);

                var complete = false;
                if (preloadedInfo) {
                    if (preloadedInfo.data) {
                        instance.ShowCropPreview(obj, preloadedInfo.data.preview);
                        complete = true;
                    }
                }
                if (!complete) {
                    instance.options.uploadCompleteCallback(name);
                }

                $(obj).find(".preview").click(function () {
                    var preview = $(this).closest(".template-download");
                    var imgSrc = $(this).find("img").attr("src");
                    instance.options.showCropWindow(imgSrc, function (data) {

                        instance.ShowCropPreview(preview, data);
                    });
                    /* parent.CropImageData(imgSrc, function (data) {
                    instance.options.ShowCropPreview(preview, data);
                    });*/
                });
            }
        });

        var imgInfo = instance.options.getUploadedFiles();
        if (imgInfo) {
            var dataStr = JSON.stringify(imgInfo);

            $.ajax({
                // Uncomment the following to send cross-domain cookies:
                //xhrFields: {withCredentials: true},
                url: "/FileUploader/ImageUploader.ashx?l=" + dataStr,
                dataType: 'json',
                context: $(instance.options.fileuploadform)[0]
            }).done(function (result) {
                $(this).fileupload('option', 'maxNumberOfFiles', instance.options.fileuploadOptions.maxNumberOfFiles - result.length);
                $(this).fileupload('option', 'done')
                .call(this, null, { result: result });
            });
        }
    };

    this.ShowCropPreview = function (cont, data) {
        if (data) {
            var container = $(cont).find(".imgContainer");
            var img = $(cont).find("img");
            if (parseInt(data.w) > 0) {
                var contW = 100;
                var contH = contW * data.h / data.w;
                var rx = contW / data.w;
                var ry = contH / data.h;
                var boundx = data.boundx;
                var boundy = data.boundy;
                $(img).css({
                    width: Math.round(rx * boundx) + 'px',
                    height: Math.round(ry * boundy) + 'px',
                    marginLeft: '-' + Math.round(rx * data.x) + 'px',
                    marginTop: '-' + Math.round(ry * data.y) + 'px'
                });

                $(container).css({
                    width: contW + 'px',
                    height: contH + 'px'
                });
            }
        }
    };
};

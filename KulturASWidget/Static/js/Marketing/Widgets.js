﻿biletino.views.marketing.widgets = new function () {
    var instance = this;
    this.options = {
        getEventTrackingCodesURL: "",
        getEventWidgetCodesURL: "",
        eventID: "",
        getEventWidgetCodesWithTrackingCodeURL: ""
    };

    this.init = function (options) {
        $.extend(this.options, options);
        instance = this;

        $(function () {

            $("#trackingCode").change(function () {
                var value = $(this).val();
                if (value.length != 0) {
                    utils.makeGetRequest(instance.options.getEventWidgetCodesWithTrackingCodeURL, { eventURL: $("#selectedEvent").val(), trackingCode: value }, function (response) {
                        var codes = jQuery.parseJSON(response);
                        $("#displayCode").text(codes.Display.toString());
                        $("#sellCode").text(codes.Sell.toString());
                    });
                }
                else {
                    utils.makeGetRequest(instance.options.getEventWidgetCodesURL, { eventURL: $("#selectedEvent").val() }, function (response) {
                        var codes = jQuery.parseJSON(response);
                        $("#displayCode").text(codes.Display.toString());
                        $("#sellCode").text(codes.Sell.toString());
                    });
                }

            });

            $("#selectedEvent").change(function () {

                utils.makeGetRequest(instance.options.getEventWidgetCodesURL, { eventURL: $(this).val() }, function (response) {
                    var codes = jQuery.parseJSON(response);
                    $("#displayCode").text(codes.Display.toString());
                    $("#sellCode").text(codes.Sell.toString());
                });


                utils.makeGetRequest(instance.options.getEventTrackingCodesURL, { eventURL: $(this).val() }, function (response) {

                    var products = JSON.parse(response);
                    var ddlSelectedProduct = $("#trackingCode");

                    // clear all previous options 
                    $("#trackingCode > option").remove();

                    // populate the products 

                    for (i = 0; i < products.length; i++) {

                        ddlSelectedProduct.append($("<option />").val(products[i].Value).text(products[i].Text));
                    }

                    if (products.length == 0) {
                        $("#trackingCodeDiv").hide();
                    } else {
                        $("#trackingCodeDiv").show();
                    }
                });

            });
        })

    }
}
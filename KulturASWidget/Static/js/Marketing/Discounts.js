﻿biletino.views.marketing.discounts = new function () {
    var instance = this;
    this.options = {
        ajaxURL: "",
        showForm: false
    };

    this.init = function (options) {
        $.extend(this.options, options);
        instance = this;
        $(function () {
            if (instance.options.showForm == 'True') {
                var position = $("#discountCreateFORM").position();
                scroll(0, position.top - 40);
            }
        });
        $("#discountCreateBTN").click(function () { $("#discountCreateFORM").slideToggle("slow"); });

        $("#selectedEvent").change(function () {

            utils.makeGetRequest(instance.options.ajaxURL, { id: $(this).val() }, function (response) {

                var products = JSON.parse(response);
                var ddlSelectedProduct = $("#layoutID");

                // clear all previous options 
                $("#layoutID > option").remove();

                // populate the products 

                for (i = 0; i < products.length; i++) {

                    ddlSelectedProduct.append($("<option />").val(products[i].Value).text(products[i].Text));
                }
            });
        });
        utils.datetimePickerRangeInit("#CommenceDate", "#EndDate", {});
        $("#CommenceDate").val("");
        $("#EndDate").val("");

        $("#whoCanBenefitAll").click(function () {
            $("#whoCanBenefitTxt").attr("style", "display: none;");
        });
        $("#whoCanBenefitAllCustom").click(function () {
            $("#whoCanBenefitTxt").attr("style", "width: 300px; display: block;");
        });

        $("#discountRatioRadio").attr("checked", true);


        $("#discountRatioRadio").change(function () {
            $("#discountType").attr("style", "position: absolute;");
            $("#discountType").html("%");
        });
        $("#discountAmountRadio").change(function () {
            $("#discountType").attr("style", "position: absolute;margin-left: 90px;");
            $("#discountType").html("TL");
        });

        $("#discountCountLimitlessRadio").attr("checked", true);
        $("#discountCountLimitlessRadio").change(function () {
            $("#discountAmountText").hide();
        });
        $(".discountTextRadio").change(function () {
            var box = $("#discountAmountText");
            $(box).remove();
            $(box).insertAfter($(this).closest(".radio"));
            $(box).show();
        });

    }
}
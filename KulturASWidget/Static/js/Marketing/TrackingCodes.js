﻿biletino.views.marketing.trackingcodes = new function () {
    var instance = this;
    this.options = {
        showForm: false
    };

    this.init = function (options) {
        $.extend(this.options, options);
        instance = this;

        $(function () {
            if (instance.options.showForm == 'True') {
                var position = $("#createTrackingFORM").position();
                scroll(0, position.top - 40);
            }
        });
    }
}
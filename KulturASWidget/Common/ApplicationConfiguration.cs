﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Globalization;
using KulturASWidget.Resources;

namespace KulturASWidget.Common
{
    public class ApplicationConfiguration
    {
        public static readonly String RestServerUrl;
        public static readonly String FacebookAppID;
        public static readonly String FacebookAppSecret;
        public static readonly String FacebookProfileUrl;
        public static readonly String AppRoot;
        public static readonly String Path;
        public static readonly String DomainName;
        public static readonly String CookieDomain;
        public static readonly String FacebookAdminIDs;
        public static readonly String FacebookPageID;
        public static readonly String FacebookPermissionScope;
        public static readonly String FacebookCreateEventPermissionScope;
        public static readonly String DefaultShortDateFormat;
        public static readonly String DefaultLongDateFormat;

        public static readonly String SearchShortDateFormat;

        public static readonly String ShowEventDateFormat;
        public static readonly String GoogleApiKey;
        public static readonly String GoogleApiSecret;

        public static readonly String ImagesUrlDevPath;
        public static readonly String ImagesUrlPath;
        public static readonly String ImagePhysicalPath;

        public static readonly String ContactMail;
        public static readonly String AppKey;
        public static readonly TimeZoneInfo TurkeyTimeZoneInfo;
        public static readonly TimeZoneInfo ServerTimeZoneInfo;

        public static readonly int PromotionEventID;
        public static readonly bool IsDevelopment;
        public static readonly bool IsTestEnviroment;
        public static readonly string OfficeIPAddress;
        //captcha
        public static readonly String CaptchaPublic;
        public static readonly String CaptchaPrivate;


        //

        public static readonly string Garanti3DSecureKey;
        public static readonly string GarantiProvisionPassword;
        public static readonly string GarantiTerminalID;
        public static readonly string GarantiMarchantID;
        public static readonly string Garanti3DSecureURL;
        public static readonly string GarantiProvURL;
        //
        public static readonly string AmazonPublicKey;
        public static readonly string AmazonSecretKey;
        public static readonly string AmazonBucketName;
        public static readonly string DynamoDBOrganizerMapTable;
        public static readonly string DynamoDBOrganizationRolesTable;
        //
        public static readonly string FoursquareAppID;
        public static readonly string FoursquareAppSecret;

        public static readonly string TumblrKey;
        public static readonly string TumblrSecret;
        //paypal
        public static readonly string BusinessAccountKey;
        public static readonly string CurrencyCode;
        public static readonly string PaypalReturnURL;
        public static readonly string PaypalCancelURL;
        public static readonly string PaypalNotifyURL;
        public static readonly string UseSandbox;


        public static readonly int LoadMoreButtonCount;
        public static readonly string GoogleAnalyticsID;
        public static readonly CultureInfo TurkishCultureInfo;
        private const String TurkishCultureName = "tr-TR";

        public static readonly List<string> TagList;

        public static readonly TimeZoneInfo CurrentTimezone;

        static ApplicationConfiguration()
        {
            BusinessAccountKey = ConfigurationManager.AppSettings["BusinessAccountKey"];
            CurrencyCode = ConfigurationManager.AppSettings["CurrencyCode"];
            PaypalReturnURL = ConfigurationManager.AppSettings["PaypalReturnURL"];
            PaypalCancelURL = ConfigurationManager.AppSettings["PaypalCancelURL"];
            PaypalNotifyURL = ConfigurationManager.AppSettings["PaypalNotifyURL"];
            UseSandbox = ConfigurationManager.AppSettings["UseSandboxPaypal"];
            //
            FoursquareAppID = ConfigurationManager.AppSettings["FoursquareAppID"];
            FoursquareAppSecret = ConfigurationManager.AppSettings["FoursquareAppSecret"];
            //
            AmazonPublicKey = ConfigurationManager.AppSettings["AWSAccessKey"];
            AmazonSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
            AmazonBucketName = ConfigurationManager.AppSettings["AWSBucket"];
            DynamoDBOrganizerMapTable = ConfigurationManager.AppSettings["DynamoDB_OrganizerMapTable"];
            DynamoDBOrganizationRolesTable = ConfigurationManager.AppSettings["DynamoDB_OrganizationRolesTable"];
            //
            TumblrKey = ConfigurationManager.AppSettings["TumblrKey"];
            TumblrSecret = ConfigurationManager.AppSettings["TumblrSecret"];
            //
            RestServerUrl = ConfigurationManager.AppSettings["RestServerUrl"];
            FacebookAdminIDs = ConfigurationManager.AppSettings["FacebookAdminIDs"];
            FacebookAppID = ConfigurationManager.AppSettings["FacebookAppID"];
            FacebookPageID = ConfigurationManager.AppSettings["FacebookPageID"];
            FacebookAppSecret = ConfigurationManager.AppSettings["FacebookAppSecret"];
            FacebookProfileUrl = ConfigurationManager.AppSettings["FacebookProfileUrl"];
            FacebookPermissionScope = ConfigurationManager.AppSettings["FacebookPermissionScope"];
            FacebookCreateEventPermissionScope = ConfigurationManager.AppSettings["FacebookCreateEventPermissionScope"];
            AppRoot = ConfigurationManager.AppSettings["AppRoot"];
            Path = ConfigurationManager.AppSettings["Path"];
            DomainName = ConfigurationManager.AppSettings["DomainName"];
            CookieDomain = ConfigurationManager.AppSettings["CookieDomain"];
            DefaultShortDateFormat = ConfigurationManager.AppSettings["DefaultShortDateFormat"];
            DefaultLongDateFormat = ConfigurationManager.AppSettings["DefaultLongDateFormat"];
            SearchShortDateFormat = ConfigurationManager.AppSettings["SearchShortDateFormat"];

            ShowEventDateFormat = ConfigurationManager.AppSettings["ShowEventDateFormat"];
            GoogleApiKey = ConfigurationManager.AppSettings["GoogleApiKey"];
            GoogleApiSecret = ConfigurationManager.AppSettings["GoogleApiSecret"];
            ImagesUrlPath = ConfigurationManager.AppSettings["ImagesPath"];
            ImagesUrlDevPath = ConfigurationManager.AppSettings["ImagesDevPath"];
            ImagePhysicalPath = ConfigurationManager.AppSettings["ImagePhysicalPath"];

            ContactMail = ConfigurationManager.AppSettings["ContactMailAddress"];
            AppKey = ConfigurationManager.AppSettings["BiletinoAppKey"];

            LoadMoreButtonCount = Int32.Parse(ConfigurationManager.AppSettings["LoadMoreButtonCount"]);

            Garanti3DSecureKey = ConfigurationManager.AppSettings["Garanti3DSecureKey"];
            GarantiProvisionPassword = ConfigurationManager.AppSettings["GarantiProvisionPassword"];
            GarantiTerminalID = ConfigurationManager.AppSettings["GarantiTerminalID"];
            GarantiMarchantID = ConfigurationManager.AppSettings["GarantiMerchantID"];
            Garanti3DSecureURL = ConfigurationManager.AppSettings["Garanti3DSecureURL"];
            GarantiProvURL = ConfigurationManager.AppSettings["GarantiProvisionURL"];

            TurkeyTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["TurkeyTimeZone"]);
            ServerTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(ConfigurationManager.AppSettings["ServerTimeZone"]);
            //TEMP
            CurrentTimezone = TurkeyTimeZoneInfo;
            //TEMP
            IsDevelopment = ConfigurationManager.AppSettings["isDevelopment"] == "true";
            IsTestEnviroment = ConfigurationManager.AppSettings["isTestEnviroment"] == "true";
            OfficeIPAddress = ConfigurationManager.AppSettings["OfficeIPAddress"];

            CaptchaPublic = ConfigurationManager.AppSettings["CaptchaPublicKey"];
            CaptchaPrivate = ConfigurationManager.AppSettings["CaptchaPrivateKey"];

            GoogleAnalyticsID = ConfigurationManager.AppSettings["GoogleAnalyticsID"];

            TurkishCultureInfo = new CultureInfo(TurkishCultureName);

            TagList = new List<string>();
            TagList.AddRange(new string[]{
                PageResources.Concert,
                PageResources.Cinema,
                PageResources.Conference,
                PageResources.Meeting,
                PageResources.Entertainment,
                PageResources.Business,
                PageResources.FoodDrink,
                PageResources.Trip,
                PageResources.Sport,
                PageResources.Music,
                PageResources.Education,
                PageResources.Art
            });
        }
        public static DateTime FromUnixTime(long unixTime)
        {
            long temp = unixTime;
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(unixTime);
        }

    }
}
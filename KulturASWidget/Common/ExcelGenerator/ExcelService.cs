﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using ExcelGenerator.SpreadSheet;
using ExcelGenerator.SpreadSheet.Styles;
using ExcelGenerator.SpreadSheet.Styles.Attributes;
using KulturASWidget.Services;
using KulturASWidget.Common.ExcelGenerator.SpreadSheet;


namespace KulturASWidget.Common.ExcelGenerator
{
    public class ExcelService
    {
        private static volatile ExcelService instance;
        private static object syncRoot = new Object();

        public ExcelService()
        {

        }

        public static ExcelService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ExcelService();
                    }
                }

                return instance;
            }
        }

        public ExcelResult GenerateEventReports(EventAIO Event)
        {
            IList<Reservation> reservationList = ReservationService.Instance.GetByEvent(Event.UserEvent.EventID);

            List<Layout> eventLayouts = LayoutService.Instance.GetByEvent(Event.UserEvent.EventID);
            Dictionary<int, Layout> layouts = new Dictionary<int, Layout>();
            foreach (var item in eventLayouts)
            {
                layouts.Add(item.LayoutID, item);
            }

            Workbook workbook = new Workbook();

            Worksheet paymentWorksheet = new Worksheet(KulturASWidget.Resources.PageResources.UserEventReportsPaymentInfo);
            Worksheet buyerWorksheet = new Worksheet(KulturASWidget.Resources.PageResources.UserEventReportsBuyerInfo);
            Worksheet entryWorksheet = new Worksheet(KulturASWidget.Resources.PageResources.UserEventReportsEntryInfo);
            Worksheet boothTicketWorksheet = new Worksheet(KulturASWidget.Resources.PageResources.UserEventReportsBoothTicketInfo);

            Style header_style = new Style("header_style");
            header_style.font.bold = true;
            header_style.alignment.horizontal = Align.Left;
            header_style.font.size = "13";
            header_style.border.bottom.weight = Weight.Medium;
            StylesManager.addStyle(header_style);

            Style cell_style = new Style("cell_style");

            cell_style.alignment.horizontal = Align.Left;
            cell_style.font.size = "11";
            cell_style.border.bottom.weight = Weight.Thin;
            cell_style.border.top.weight = Weight.Thin;
            StylesManager.addStyle(cell_style);

            Row paymentRow = new Row(32);
            paymentRow.Cells.Add(new Cell("Adı Soyadı", "header_style"));
            paymentRow.Cells.Add(new Cell("Bilet Adı", "header_style"));
            paymentRow.Cells.Add(new Cell("Bilet Kodu", "header_style"));
            paymentRow.Cells.Add(new Cell("Toplam Ödenen", "header_style"));
            paymentRow.Cells.Add(new Cell("Alış Tarihi", "header_style"));
            paymentWorksheet.Rows.Add(paymentRow);

            Row boothTicketRow = new Row(32);
            boothTicketRow.Cells.Add(new Cell("Adı Soyadı", "header_style"));
            boothTicketRow.Cells.Add(new Cell("Bilet Adı", "header_style"));
            boothTicketRow.Cells.Add(new Cell("Bilet Kodu", "header_style"));
            boothTicketRow.Cells.Add(new Cell("Toplam Ödenen", "header_style"));
            boothTicketRow.Cells.Add(new Cell("Alış Tarihi", "header_style"));
            boothTicketWorksheet.Rows.Add(boothTicketRow);

            Row buyerRow = new Row(32);
            buyerRow.Cells.Add(new Cell("Adı Soyadı", "header_style"));
            buyerRow.Cells.Add(new Cell("E-Posta", "header_style"));
            buyerRow.Cells.Add(new Cell("Telefon No", "header_style"));


            for (int i = 0; i < Event.UserEvent.Questions.Count; i++)
            {
                buyerRow.Cells.Add(new Cell(Event.UserEvent.Questions.ElementAt(i).Question, "cell_style"));
            }
            buyerWorksheet.Rows.Add(buyerRow);

            Row entryRow = new Row(32);
            entryRow.Cells.Add(new Cell("Adı Soyadı", "header_style"));
            entryRow.Cells.Add(new Cell("Bilet Adı", "header_style"));
            entryRow.Cells.Add(new Cell("Bilet Kodu", "header_style"));
            entryRow.Cells.Add(new Cell("Giriş Tarihi", "header_style"));
            entryRow.Cells.Add(new Cell("Giriş Saati", "header_style"));
            entryWorksheet.Rows.Add(entryRow);

            DateTime minDateTime = new DateTime(1970, 1, 1);
            foreach (var res in reservationList)
            {
                if (res.PaymentID > 0)
                {
                    
                    if (layouts[res.LayoutID].IsBoothTicket)
                    {
                        boothTicketRow = new Row(32);
                        boothTicketRow.Cells.Add(new Cell(res.AttendeeName, "cell_style"));
                        boothTicketRow.Cells.Add(new Cell(layouts[res.LayoutID].Name, "cell_style"));
                        boothTicketRow.Cells.Add(new Cell(res.VerificationPNR, "cell_style"));
                        boothTicketRow.Cells.Add(new Cell(String.Format("{0} TL", ((double)layouts[res.LayoutID].TotalPrice) / 100), "cell_style"));
                        boothTicketRow.Cells.Add(new Cell(res.CreationDate.ToString(), "cell_style"));
                        boothTicketWorksheet.Rows.Add(boothTicketRow);
                    }
                  

                    paymentRow = new Row(32);
                    paymentRow.Cells.Add(new Cell(res.AttendeeName, "cell_style"));
                    paymentRow.Cells.Add(new Cell(layouts[res.LayoutID].Name, "cell_style"));
                    paymentRow.Cells.Add(new Cell(res.VerificationPNR, "cell_style"));
                    paymentRow.Cells.Add(new Cell(String.Format("{0} TL", ((double)layouts[res.LayoutID].TotalPrice) / 100), "cell_style"));
                    paymentRow.Cells.Add(new Cell(res.CreationDate.ToString(), "cell_style"));
                    paymentWorksheet.Rows.Add(paymentRow);

                    buyerRow = new Row(32);
                    buyerRow.Cells.Add(new Cell(res.AttendeeName, "cell_style"));
                    buyerRow.Cells.Add(new Cell(res.AttendeeEmail, "cell_style"));
                    buyerRow.Cells.Add(new Cell(res.AttendeePhone, "cell_style"));

                    for (int i = 0; i < res.QuestionAnswers.Count; i++)
                    {
                        buyerRow.Cells.Add(new Cell(res.QuestionAnswers.ElementAt(i).Value, "cell_style"));
                    }

                    buyerWorksheet.Rows.Add(buyerRow);

                    entryRow = new Row(32);
                    entryRow.Cells.Add(new Cell(res.AttendeeName, "cell_style"));
                    entryRow.Cells.Add(new Cell(layouts[res.LayoutID].Name, "cell_style"));
                    entryRow.Cells.Add(new Cell(res.VerificationPNR, "cell_style"));
                    entryRow.Cells.Add(new Cell(res.EntryDate == minDateTime ? "-" : res.EntryDate.ToShortDateString(), "cell_style"));
                    entryRow.Cells.Add(new Cell(res.EntryDate == minDateTime ? "-" : res.EntryDate.ToShortTimeString(), "cell_style"));
                    entryWorksheet.Rows.Add(entryRow);
                }
            }

            paymentWorksheet.Columns = new List<Column>();
            paymentWorksheet.Columns.Add(new Column(275, 1));
            paymentWorksheet.Columns.Add(new Column(275, 2));
            paymentWorksheet.Columns.Add(new Column(275, 3));
            paymentWorksheet.Columns.Add(new Column(275, 4));
            paymentWorksheet.Columns.Add(new Column(275, 5));

            boothTicketWorksheet.Columns = new List<Column>();
            boothTicketWorksheet.Columns.Add(new Column(275, 1));
            boothTicketWorksheet.Columns.Add(new Column(275, 2));
            boothTicketWorksheet.Columns.Add(new Column(275, 3));
            boothTicketWorksheet.Columns.Add(new Column(275, 4));
            boothTicketWorksheet.Columns.Add(new Column(275, 5));


            buyerWorksheet.Columns = new List<Column>();
            buyerWorksheet.Columns.Add(new Column(275, 1));
            buyerWorksheet.Columns.Add(new Column(275, 2));
            buyerWorksheet.Columns.Add(new Column(275, 3));
            buyerWorksheet.Columns.Add(new Column(275, 4));
            buyerWorksheet.Columns.Add(new Column(275, 5));
            buyerWorksheet.Columns.Add(new Column(275, 6));
            buyerWorksheet.Columns.Add(new Column(275, 7));
            buyerWorksheet.Columns.Add(new Column(275, 8));
            buyerWorksheet.Columns.Add(new Column(275, 9));
            buyerWorksheet.Columns.Add(new Column(275, 10));

            entryWorksheet.Columns = new List<Column>();
            entryWorksheet.Columns.Add(new Column(275, 1));
            entryWorksheet.Columns.Add(new Column(275, 2));
            entryWorksheet.Columns.Add(new Column(275, 3));
            entryWorksheet.Columns.Add(new Column(275, 4));
            entryWorksheet.Columns.Add(new Column(275, 5));

            workbook.Worksheets.Add(paymentWorksheet);
            workbook.Worksheets.Add(buyerWorksheet);
            workbook.Worksheets.Add(entryWorksheet);
            workbook.Worksheets.Add(boothTicketWorksheet);

            return new ExcelResult(workbook.getBytes(), Event.UserEvent.Name);

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KulturASWidget.Common.ExcelGenerator.SpreadSheet;

namespace ExcelGenerator.SpreadSheet
{
    public class Worksheet
    {
        private StringBuilder sb;
        private string _title;

        public List<Row> Rows;
        public List<Column> Columns;
        public Worksheet(String title)
        {
            Rows = new List<Row>();
            Columns = new List<Column>();

            sb = new StringBuilder();

            _title = title;
        }

        public string title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }


        public StringBuilder encodeCells()
        {
            foreach (Column col in Columns)
            {
                sb.Append("<Column ss:Index=\"" + col._index + "\" ss:Width=\"" + col._width + "\"/>");
            }
            foreach (Row row in Rows)
            {
                //inicia nova linha
                sb.Append("<Row Height=\"" + row.height + "\">");
                //checa se a linha possui span maior que 1
                if (row.span > 1)
                {
                    Cell cell = row.Cells[0];
                    sb.Append("<Cell ss:MergeAcross=\"" + (row.span - 1) + "\" ss:StyleID=\"" + cell.style + "\"><Data ss:Type=\"String\">" + cell.Value + "</Data></Cell>");
                }
                else
                {
                    //Insere as celulas
                    foreach (Cell cell in row.Cells)
                    {
                        sb.Append("<Cell ss:MergeAcross=\"" + (cell.span - 1) + "\" ss:StyleID=\"" + cell.style + "\"><Data ss:Type=\"String\">" + cell.Value + "</Data></Cell>");
                    }
                }
                //Fecha a linha
                sb.Append("</Row>");
            }

            return sb;
        }
    }
}

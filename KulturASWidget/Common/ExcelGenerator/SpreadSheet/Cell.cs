﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelGenerator.SpreadSheet.Styles;

namespace ExcelGenerator.SpreadSheet
{
    public class Cell
    {
        
        private object _value;
        private string _style;
        private int _span;

        /// <summary>
        /// Creates an empty new cell
        /// </summary>
        public Cell(){
            _span = 1;
        }

        /// <summary>
        /// Creates a new cell
        /// </summary>
        /// <param name="value">Value to this cell data</param>
        /// <param name="style">Style to apply in this cell, if not set the "Default" style will be used</param>
        /// <param name="style">Specifies the number of adjacent cells across (right) from the current cell to merge.</param>
        public Cell(String value, int span, String style = "Default")
        {
            _value = value;
            _style = "Default";
            _span = span;
        }

        /// <summary>
        /// Creates a new cell
        /// </summary>
        /// <param name="value">Value to this cell data</param>
        /// <param name="style">Style to apply in this cell, if not set the "Default" style will be used</param>
        public Cell(String value, String style = "Default")
        {
            if (!StylesManager.containsStyle(style))
            {
                throw new ArgumentException("Trying to create a cell with a style not defined", "style");
            }

            _value = value;
            _style = style;
            _span = 1;
        }

        /// <summary>
        /// Value to this cell data
        /// </summary>
        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        /// <summary>
        /// Style to apply in this cell
        /// </summary>
        public string style
        {
            get
            {
                return _style;
            }
            set
            {
                if (!StylesManager.containsStyle(value))
                {
                    throw new ArgumentException("Trying to create a cell with a style not defined", "style");
                }
                else
                {
                    _style = value;
                }
            }
        }

        /// <summary>
        /// Specifies the number of adjacent cells across (right) from the current cell to merge.
        /// </summary>
        public int span
        {
            get
            {
                return _span;
            }
            set
            {
                if (value <= 0)
                {
                    _span = 1;
                }
                else
                {
                    _span = value;
                }    
            }
        }
        
    }
}

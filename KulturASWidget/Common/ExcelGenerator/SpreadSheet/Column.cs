﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Common.ExcelGenerator.SpreadSheet
{
    public class Column
    {
        public int _width;
        public int _index;

        public Column(int width,int index)
        {
            _width = (int)(width * 0.75);
            _index = index;
        }
    }
}
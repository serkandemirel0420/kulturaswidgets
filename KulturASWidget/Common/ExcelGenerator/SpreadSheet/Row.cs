﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet
{
    public class Row
    {
        /// <summary>
        /// List of cells from this row
        /// </summary>
        public List<Cell> Cells;
        /// <summary>
        /// Number of colums to merge
        /// </summary>
        public int span { get; set; }
        public int height { get; set; }
        /// <summary>
        /// Create a new row
        /// </summary>
        public Row(int p_height)
        {
            span = 1;
            height = (int)(p_height * 0.75);
            Cells = new List<Cell>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles
{
    /// <summary>
    /// Defines the manager for dealing with the Styles.
    /// </summary>
    public static class StylesManager
    {
        private static Dictionary<String, Style> Styles = new Dictionary<String, Style>()
        {
            {"Default", new Style("Default")}
        };

        /// <summary>
        /// Add or update a style in the Style's list
        /// </summary>
        /// <param name="style">Object to insert/update in the list</param>
        public static void addStyle(Style style)
        {
            Styles.Remove(style.name);
            Styles[style.name] = style;
        }

        /// <summary>
        /// Returns a style from the Style's list
        /// </summary>
        /// <param name="key">The name of this style</param>
        /// <returns>Style</returns>
        public static Style getStyle(String key)
        {
            return Styles[key];
        }

        /// <summary>
        /// Verify if the style is already defined
        /// </summary>
        /// <param name="key">The name of this style</param>
        /// <returns>True if the style's list contains it</returns>
        public static bool containsStyle(String key)
        {
            return Styles.ContainsKey(key);
        }

        /// <summary>
        /// Verify if the style is already defined
        /// </summary>
        /// <param name="key">The object representing the style</param>
        /// <returns>True if the style's list contains it</returns>
        public static bool containsStyle(Style style)
        {
            return Styles.ContainsValue(style);
        }

        internal static Dictionary<String, Style> getAllStyle()
        {
            return Styles;
        }
    }
}

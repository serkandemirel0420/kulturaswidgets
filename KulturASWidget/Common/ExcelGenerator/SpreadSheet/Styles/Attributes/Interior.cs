﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles.Attributes
{
    /// <summary>
    /// Options available for cell's Pattern property
    /// </summary>
    public enum Pattern
    {
        None,
        Solid,
        Gray75,
        Gray50,
        Gray25,
        Gray125,
        Gray0625,
        HorzStripe,
        VertStripe,
        ReverseDiagStripe,
        DiagStripe,
        DiagCross,
        ThickDiagCross,
        ThinHorzStripe,
        ThinVertStripe,
        ThinReverseDiagStripe,
        ThinDiagStripe,
        ThinHorzCross,
        ThinDiagCross
    }


    /// <summary>
    /// Defines the fill properties to use in this style. Each attribute that is specified is considered an override from the default.
    /// </summary>
    public class Interior
    {
        /// <summary>
        /// Specifies the fill color of the cell. This value must be a 6-hexadecimal digit number in "#rrggbb" format. This string is case insensitive. If this attribute is not specified within an element, the default(White) is assumed.
        /// </summary>
        public String color { get; set; }
        
        /// <summary>
        /// Specifies the fill pattern in the cell. The fill pattern determines how to blend the Color and PatternColor attributes to produce the cell's appearance. If this attribute is not specified within an element, the default(Solid) is assumed.
        /// </summary>
        public Pattern pattern { get; set; }

        /// <summary>
        /// Specifies the secondary fill color of the cell when Pattern does not equal Solid. This value must be a 6-hexadecimal digit number in "#rrggbb" format. This string is case insensitive. If this attribute is not specified within an element, the default(Black) is assumed.
        /// </summary>
        public String patternColor { get; set; }

        public Interior(string color, Pattern pattern, String patternColor)
        {
            this.color = color;
            this.pattern = pattern;
            this.patternColor = patternColor;
        }

        internal String render()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<Interior ss:Color=\"");
            sb.Append(color);
            sb.Append("\" ss:Pattern=\"");
            sb.Append(pattern.ToString());
            sb.Append("\" ss:PatternColor=\"");
            sb.Append(patternColor);
            sb.Append("\" />");

            return sb.ToString();
        }

    }
}

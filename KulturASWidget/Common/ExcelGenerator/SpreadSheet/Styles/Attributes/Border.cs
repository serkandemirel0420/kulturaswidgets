﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles.Attributes
{
    /// <summary>
    /// Defines a the collection of the four borders the cell owns.
    /// </summary>
    public class Border
    {
        /// <summary>
        /// Specifies whether any border should be displayed. False means that no border will be displayed for this style.
        /// </summary>
        public bool display { get; set; }
        /// <summary>
        /// Defines the bottom border
        /// </summary>
        public BorderStyle bottom { get; set; }
        /// <summary>
        /// Defines the left border
        /// </summary>
        public BorderStyle left { get; set; }
        /// <summary>
        /// Defines the right border
        /// </summary>
        public BorderStyle right { get; set; }
        /// <summary>
        /// Defines the top border
        /// </summary>
        public BorderStyle top { get; set; }

        public Border()
        {
            display = true;
            this.bottom = new BorderStyle(Position.Bottom, LineStyle.Continuous, Weight.Hairline, "#000000");
            this.left = new BorderStyle(Position.Left, LineStyle.Continuous, Weight.Hairline, "#000000");
            this.right = new BorderStyle(Position.Right, LineStyle.Continuous, Weight.Hairline, "#000000");
            this.top = new BorderStyle(Position.Top, LineStyle.Continuous, Weight.Hairline, "#000000");
        }

        internal String render(){
            if (!display)
            {
                return "";
            }
            
            StringBuilder sb = new StringBuilder();
            
            sb.Append("<Borders>");
            
            if (left != null && left.position == Position.Left)
            {
                sb.Append(left.render());
            }
            if (right != null && right.position == Position.Right)
            {
                sb.Append(right.render());
            }
            if (top != null && top.position == Position.Top)
            {
                sb.Append(top.render());
            }
            if (bottom != null && bottom.position == Position.Bottom)
            {
                sb.Append(bottom.render());
            }

            sb.Append("</Borders>");

            return sb.ToString();

        }
    }
}

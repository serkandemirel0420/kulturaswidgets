﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles.Attributes
{
    /// <summary>
    /// Options available for font underline property
    /// </summary>
    public enum Underline
    {
        None,
        Single,
        Double,
        SingleAccounting,
        DoubleAccounting
    }

    /// <summary>
    /// Defines the font attributes to use in this style. Each attribute that is specified is considered an override from the default.
    /// </summary>
    public class Font
    {
        /// <summary>
        /// Specifies the name of the font. This string is case insensitive. If this attribute is not specified within an element, the default(Arial) is assumed.
        /// </summary>
        public String name { get; set; }
        /// <summary>
        /// Specifies the size of the font in points. This value must be strictly greater than 0. If this attribute is not specified within an element, the default(11) is assumed.
        /// </summary>
        public String size { get; set; }
        /// <summary>
        /// Specifies the color of the font. This value must be a 6-hexadecimal digit number in "#rrggbb" format. This string is case insensitive. If this attribute is not specified within an element, the default(Black) is assumed.
        /// </summary>
        public String color { get; set; }
        /// <summary>
        /// Specifies the bold state of the font. If this attribute is not specified within an element, the default(False) is assumed.
        /// </summary>
        public bool bold { get; set; }
        /// <summary>
        /// Specifies the underline state of the font. If this attribute is not specified within an element, the default(None) is assumed.
        /// </summary>
        public Underline underline { get; set; }

        public Font(string name, string size, string color, bool bold, Underline underline)
        {
            this.name = name;
            this.size = size;
            this.color = color;
            this.bold = bold;
            this.underline = underline;
        }

        internal String render()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<Font ss:FontName=\"");
            sb.Append(name);
            sb.Append("\" ss:Size=\"");
            sb.Append(size);
            sb.Append("\" ss:Color=\"");
            sb.Append(color);
            sb.Append("\" ss:Bold=\"");
            sb.Append((bold) ? "1" : "0");
            sb.Append("\" ss:Underline=\"");
            sb.Append(underline.ToString());
            sb.Append("\" />");

            return sb.ToString();
        }
    }
}

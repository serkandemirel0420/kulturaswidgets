﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles.Attributes
{
    /// <summary>
    /// Options available for cell's Alignment property
    /// </summary>
    public enum Align
    {
        Left,
        Right,
        Center,
        Top,
        Bottom,
    }

    /// <summary>
    /// Defines the font alignment attributes to use in this style. Each attribute that is specified is considered an override from the default.
    /// </summary>
    public class Alignment
    {
        /// <summary>
        /// Specifies the left-to-right alignment of text within a cell. If this attribute is not specified within an element, the default(Left) is assumed.
        /// </summary>
        public Align horizontal { get; set; }
        /// <summary>
        /// Specifies the top-to-bottom alignment of text within a cell. If this attribute is not specified within an element, the default(Top) is assumed.
        /// </summary>
        public Align vertical { get; set; }
        /// <summary>
        /// Specifies whether the text in this cell should wrap at the cell boundary. If this attribute is not specified within an element, the default(False) is assumed.
        /// </summary>
        public bool wrapText { get; set; }

        /// <summary>
        /// Creates a new align object to define cell's properties
        /// </summary>
        /// <param name="horizontal">Horizontal alignment</param>
        /// <param name="vertical">Vertical alignment</param>
        public Alignment(Align horizontal, Align vertical, bool wrapText)
        {
            this.horizontal = horizontal;
            this.vertical = vertical;
            this.wrapText = wrapText;
        }

        internal String render()
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("<Alignment ss:Horizontal=\"");
            sb.Append(horizontal.ToString());
            sb.Append("\" ss:Vertical=\"");
            sb.Append(vertical.ToString());
            sb.Append("\" ss:WrapText=\"");
            sb.Append((wrapText) ? "1" : "0");
            sb.Append("\" />");
            
            return sb.ToString();
        }
    }

    
}

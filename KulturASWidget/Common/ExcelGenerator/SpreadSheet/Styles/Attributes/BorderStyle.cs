﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExcelGenerator.SpreadSheet.Styles.Attributes
{
    /// <summary>
    /// Options available for border position property
    /// </summary>
    public enum Position
    {
        Top,
        Right,
        Bottom,
        Left
    }

    /// <summary>
    /// Options available for border line style property
    /// </summary>
    public enum LineStyle
    {
        None,
        Continuous,
        Dash,
        Dot,
        DashDot,
        DashDotDot,
        SlantDashDot,
        Double
    }

    /// <summary>
    /// Options available for border weight(thickness) property
    /// </summary>
    public enum Weight : int
    {
        Hairline = 0,
        Thin = 1,
        Medium = 2,
        Thic = 3
    }

    /// <summary>
    /// Defines a single border within this style's Borders collection.
    /// </summary>
    public class BorderStyle
    {
        /// <summary>
        /// Specifies which of the four possible borders this element represents. Duplicate borders are not permitted and are considered invalid.
        /// </summary>
        public Position position { get; set; }
        /// <summary>
        /// Specifies the appearance of this border. If this attribute is not specified within an element, the default(Continuous) is assumed.
        /// </summary>
        public LineStyle lineStyle { get; set; }
        /// <summary>
        /// Specifies the color of the border line. This value must be a 6-hexadecimal digit number in "#rrggbb" format. This string is case insensitive. If this attribute is not specified within an element, the default(Black) is assumed.
        /// </summary>
        public String color { get; set; }
        /// <summary>
        /// Specifies the weight (or thickness) of this border. If this attribute is not specified within an element, the default(Hairline) is assumed.
        /// </summary>
        public Weight weight { get; set; }


        public BorderStyle(Position position, LineStyle lineStyle, Weight weight, String color)
        {
            this.position = position;
            this.lineStyle = lineStyle;
            this.weight = weight;
            this.color = color;
        }

        internal String render()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<Border ss:Position=\"");
            sb.Append(position.ToString());
            sb.Append("\" ss:LineStyle=\"");
            sb.Append(lineStyle.ToString());
            sb.Append("\" ss:Weight=\"");
            sb.Append((int)weight);
            sb.Append("\" ss:Color=\"");
            sb.Append(color);
            sb.Append("\" />");

            return sb.ToString();
        }
    }
}

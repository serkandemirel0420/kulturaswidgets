﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelGenerator.SpreadSheet.Styles.Attributes;

namespace ExcelGenerator.SpreadSheet.Styles
{
    /// <summary>
    /// Object to defines all cell styles
    /// </summary>
    public class Style
    {
        /// <summary>
        /// Defines the name attribute to use this style in your Spreadsheet cells.
        /// </summary>
        public String name {get; set;}
        public Alignment alignment { get; set; }
        public Border border { get; set; }
        public Font font {get; set;}
        public Interior interior { get; set; }
        
        public Style(String name)
        {
            this.name = name;
            this.alignment = new Alignment(Align.Left, Align.Top, false);
            this.border = new Border();
            this.font = new Font("Arial", "11", "#000000", false, Underline.None);
            this.interior = new Interior("#ffffff", Pattern.Solid, "#000000");
        }

        internal String renderData()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<Style ss:ID=\"");
            sb.Append(name);
            sb.Append("\" ss:Name=\"");
            sb.Append(name);
            sb.Append("\">");
  
            sb.Append(alignment.render());

            sb.Append(border.render());   

            sb.Append(font.render());

            sb.Append(interior.render());
  
            sb.Append("</Style>");

            return sb.ToString();
        }

    }
}

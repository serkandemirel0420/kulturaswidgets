﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;

namespace KulturASWidget.Common.ExcelGenerator
{

    public class ExcelResult : ActionResult
    {

        private Stream excelStream;
        string filename;
        public ExcelResult(byte[] excel,string p_filename)
        {
            excelStream = new MemoryStream(excel);
            filename = p_filename;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = "application/vnd.ms-excel";

            byte[] buffer = new byte[4096];

            while (true)
            {
                int read = this.excelStream.Read(buffer, 0, buffer.Length);
                if (read == 0)
                {
                    break;
                }

                response.OutputStream.Write(buffer, 0, read);
            }
            response.AddHeader("Content-Disposition","attachment; filename=\""+ filename +".xls\"");
            response.End();

        }
    }

}
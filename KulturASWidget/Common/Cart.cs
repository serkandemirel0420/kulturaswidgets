﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Services;

namespace KulturASWidget.Common
{
    public class Cart : CookieSerializable<Cart>
    {
        public Layout Layout { get; set; }
        public EventAIO Event { get; set; }
        public int Quantity { get; set; }
        public bool Paid
        {
            get
            {
                return Layout.TotalPrice > 0;
            }
        }

        public string Serialize()
        {

            return String.Format("{0}#{1}#{2}", this.Layout.LayoutID, this.Quantity, this.Paid ? 1 : 0);
        }

        public bool IsEmpty
        {
            get
            {
                return this.Layout == null || Quantity == 0;
            }
        }

        public Double SubTotal
        {

            get { return (double)Layout.TotalPrice / 100 * Quantity; }
        }

        public String DisplaySubTotal
        {
            get
            {
                return String.Format("{0} TL", SubTotal);
            }
        }


        public Cart DeSerialize(String value)
        {
            if (value.Contains("#"))
            {
                var layoutId = int.Parse(value.Split('#')[0]);
                var quantity = int.Parse(value.Split('#')[1]);
                var paid = int.Parse(value.Split('#')[2]) == 1;
                var layout = LayoutService.Instance.GetByID(layoutId);
                this.Layout = layout;
                var ev = EventService.Instance.GetByID(layout.EventID);
                this.Event = ev;
                this.Quantity = quantity;
                //this.Paid = paid;
            }

            return this;
        }
    }
}
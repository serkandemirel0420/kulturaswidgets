﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using KulturASWidget.Common.Attributes.ValidationAttributes;
using KulturASWidget.Common.ValidationAttributes;

namespace KulturASWidget.Common
{
    public class AttendeesDatas
    {
        public FormCollection collection { get; set; }
        public String[] FirstNames { get; set; }
        public String[] LastNames { get; set; }
        public String[] Mails { get; set; }
        public String[] Cells { get; set; }
        public Dictionary<string, string[]> QuestionAnswers { get; set; }
        public List<String> Questions { get; set; }
        public AttendeesDatas()
        {
            this.QuestionAnswers = new Dictionary<string, string[]>();
            this.Questions = new List<string>();
        }

        public bool IsValid()
        {
            foreach (var item in this.collection.AllKeys)
            {
                switch (item)
                {
                    case "FirstName":
                        this.FirstNames = this.collection[item].Split(new char[] { ',' }, StringSplitOptions.None);
                        foreach (var firstName in this.FirstNames)
                        {
                            CheckIfAllLetters check = new CheckIfAllLetters();
                            if ((firstName.Length > 40) || !check.IsValid(firstName))
                            {
                                return false;
                            }

                        }
                        break;
                    case "LastName":
                        this.LastNames = this.collection[item].Split(new char[] { ',' }, StringSplitOptions.None);
                        foreach (var lastName in this.LastNames)
                        {
                            CheckIfAllLetters check = new CheckIfAllLetters();
                            if ((lastName.Length > 40) || !check.IsValid(lastName))
                            {
                                return false;
                            }

                        }
                        break;
                    case "Mail":
                        this.Mails = this.collection[item].Split(new char[] { ',' }, StringSplitOptions.None);
                        foreach (var mail in this.Mails)
                        {
                            CheckMailFormatAttribute check = new CheckMailFormatAttribute();
                            if (!check.IsValid(mail))
                            {
                                return false;
                            }

                        }
                        break;
                    case "Cell":
                        this.Cells = this.collection[item].Split(new char[] { ',' }, StringSplitOptions.None);
                        foreach (var cell in this.Cells)
                        {
                            IsCellAcceptableAttribute check = new IsCellAcceptableAttribute();

                            if ((cell.Length < 10) || (cell.Length > 20) || !check.IsValid(cell))
                            {
                                return false;
                            }

                        }
                        break;
                    default:
                        break;
                }
                if (item.StartsWith("question"))
                {
                    string question = item.Replace("question_", "");
                    this.Questions.Add(question);
                    string[] answers = collection[item].Split(new char[] { ',' }, StringSplitOptions.None);
                    foreach (var answer in answers)
                    {
                        if (answer.IsEmpty())
                        {
                            return false;
                        }
                    }
                    this.QuestionAnswers.Add(question, answers);
                }

            }
            return true;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class NoWhitespace : ValidationAttribute
    {
        public NoWhitespace()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                var val = value.ToString().Trim().ClearWhiteSpaces();
                return value.ToString().Trim().Length == val.Length;
            }
            return false;
        }
        
    }
}
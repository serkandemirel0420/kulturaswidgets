﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Resources;
using KulturASWidget.Services;

namespace KulturASWidget.Common.ValidationAttributes
{
    public class IsMailEnrolledAttribute : ValidationAttribute
    {
        public IsMailEnrolledAttribute()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (!String.IsNullOrEmpty((String)value))
            {
                var usr = UserService.Instance.GetUserByMail(value.ToString());

                return !usr.IsExists() || usr.IsSubscriber;

            }
            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            var nm = name;
            nm.Trim();
            ResourceManager manager = new System.Resources.ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);
            return manager.GetString(ErrorMessageResourceName);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class ValidateJSON : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool isValid = true;
            try
            {
                JsonConvert.DeserializeObject(value.ToString());
            }
            catch
            {
                isValid = false;
            }
            return isValid;
        }
    }
}
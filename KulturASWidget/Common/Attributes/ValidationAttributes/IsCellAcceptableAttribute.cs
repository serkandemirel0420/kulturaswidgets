﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Resources;

namespace KulturASWidget.Common.ValidationAttributes
{
    public class IsCellAcceptableAttribute : ValidationAttribute
    {
        public IsCellAcceptableAttribute()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value == null)
                return true;
            if (value.ToString() == "")
                return true;

            string phoneOutput = string.Empty;
            string phoneInput = Convert.ToString(value);
            phoneInput = phoneInput.Replace(" ", "");

            foreach (char ch in phoneInput)
            {
                if (Char.IsNumber(ch))
                {
                    phoneOutput += ch;
                }
                else
                {
                    return false;
                }
            }

            if (phoneOutput.Length <= 14 && phoneOutput.Length >= 10)
            {
                return true;
            }

            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            var nm = name;
            nm.Trim();
            ResourceManager manager = new System.Resources.ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);
            return manager.GetString(ErrorMessageResourceName);
        }
    }
}
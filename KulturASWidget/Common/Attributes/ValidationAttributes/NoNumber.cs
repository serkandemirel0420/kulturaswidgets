﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class NoNumber : ValidationAttribute
    {
        public NoNumber()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                double res;
                return !Double.TryParse(value.ToString().Trim(), out res);
            }
            return false;
        }
        
    }
}
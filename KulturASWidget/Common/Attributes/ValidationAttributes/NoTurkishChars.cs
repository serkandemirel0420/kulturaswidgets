﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class NoTurkishChars : ValidationAttribute
    {
        public NoTurkishChars()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                var val = value.ToString();
                return val.ToLower() == val.ConverToLowerEnglishChars();
            }
            return false;
        }
        
    }
}
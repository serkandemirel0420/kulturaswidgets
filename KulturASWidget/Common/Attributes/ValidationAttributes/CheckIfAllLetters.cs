﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class CheckIfAllLetters : ValidationAttribute
    {
        public CheckIfAllLetters()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                return value.ToString().Replace(" ","").IsAllLetters();
            }
            return true;
        }
        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Resources;

namespace KulturASWidget.Common.ValidationAttributes
{
    public class CheckMailFormatAttribute : ValidationAttribute
    {
        public CheckMailFormatAttribute()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                if (!String.IsNullOrEmpty(value.ToString().Trim()))
                {
                    return (Regex.IsMatch(value.ToString(), @"^(?("")(""[^""]+?""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" + @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9]{2,17}))$", RegexOptions.IgnoreCase));
                }
            }
            return false;
        }

        public override string FormatErrorMessage(string name)
        {

            ResourceManager manager = new System.Resources.ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);
            return manager.GetString(ErrorMessageResourceName);
        }
    }
}
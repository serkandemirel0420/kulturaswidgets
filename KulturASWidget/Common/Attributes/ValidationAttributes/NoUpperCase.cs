﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class NoUpperCase : ValidationAttribute
    {
        public NoUpperCase()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                var val = value.ToString().Trim();
                return val == val.ConverToLowerEnglishChars();
            }
            return false;
        }
        
    }
}
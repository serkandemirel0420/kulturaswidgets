﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class YoutubeLinkValidation : ValidationAttribute
    {
        public YoutubeLinkValidation()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                var youtubeURL = value.ToString();
                var pattern = @"^(http|https):\/\/(?:www\.)?youtube.com\/watch\?(?=.*v=\w+)(?:(?:\S(?!http))+)?$";
                return Regex.IsMatch(youtubeURL, pattern);
            }
            return true;
        }
        
    }
}
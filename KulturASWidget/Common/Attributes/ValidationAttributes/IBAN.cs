﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Resources;
using System.Text.RegularExpressions;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class CheckCharIBAN : ValidationAttribute
    {
        public CheckCharIBAN() : base() { }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                String iban = value.ToString();
                iban = Regex.Replace(iban, @"\s", "").ToUpper();
                if (Regex.IsMatch(iban, @"\W") || !Regex.IsMatch(iban, @"^\D\D\d\d.+") || Regex.IsMatch(iban, @"^\D\D00.+|^\D\D01.+|^\D\D99.+") || !Regex.IsMatch(iban.Remove(0, 4), @"\d{5}[a-zA-Z0-9]{17}"))
                    return false;
            }
            return true;
        }
        public override string FormatErrorMessage(string name)
        {
            ResourceManager manager = new System.Resources.ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);
            return manager.GetString(ErrorMessageResourceName);
        }
    }
    public class InvalidIBAN : ValidationAttribute
    {
        public InvalidIBAN() : base() { }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                String iban = value.ToString();;
                iban = Regex.Replace(iban, @"\s", "").ToUpper();
                if (Regex.IsMatch(iban, @"\W") || !Regex.IsMatch(iban, @"^\D\D\d\d.+") || Regex.IsMatch(iban, @"^\D\D00.+|^\D\D01.+|^\D\D99.+") || !Regex.IsMatch(iban.Remove(0, 4), @"\d{5}[a-zA-Z0-9]{17}"))
                    return false;

                string modifiedIban = iban.ToUpper().Substring(4) + iban.Substring(0, 4);
                modifiedIban = Regex.Replace(modifiedIban, @"\D", m => ((int)m.Value[0] - 55).ToString());
                int remainer = 0;
                while (modifiedIban.Length >= 7)
                {
                    remainer = int.Parse(remainer + modifiedIban.Substring(0, 7)) % 97;
                    modifiedIban = modifiedIban.Substring(7);
                }
                remainer = int.Parse(remainer + modifiedIban) % 97;

                if (remainer != 1)
                    return false;
            }
            return true;
        }
        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(name);
        }
    }
}
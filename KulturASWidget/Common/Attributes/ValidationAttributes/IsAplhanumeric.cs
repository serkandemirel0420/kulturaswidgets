﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace KulturASWidget.Common.Attributes.ValidationAttributes
{
    public class IsAplhanumeric : ValidationAttribute
    {
        public IsAplhanumeric()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value.IsExists())
            {
                return value.ToString().Trim().IsAlphanumeric();
            }
            return false;
        }
        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using System.Resources;

namespace KulturASWidget.Common.Attributes
{
    public class CharacterCheckAttribute : ValidationAttribute
    {
        public CharacterCheckAttribute()
            : base()
        {

        }

        public override bool IsValid(object value)
        {
            if (value != null)
            {
                if (!String.IsNullOrEmpty(value.ToString()))
                {
                    return (Regex.IsMatch(value.ToString(), @"^[a-zA-ZıIğüşıiöçĞÜŞİÖÇ ]+$", RegexOptions.IgnoreCase));
                }
            }
            return false;
        }

        public override string FormatErrorMessage(string name)
        {
            ResourceManager manager = new System.Resources.ResourceManager(ErrorMessageResourceType.FullName, ErrorMessageResourceType.Assembly);
            return manager.GetString(ErrorMessageResourceName);
        }
    }
}
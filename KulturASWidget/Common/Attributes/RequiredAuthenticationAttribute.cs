﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace KulturASWidget.Common.Attributes
{
    /// <summary>
    /// With this atribute if request is authenticated redirect to given paremeters.
    /// </summary>
    public enum AuthenticationMode
    {
        AuthenticatedOnly,
        NonAuthenticatedOnly,
        AllowAll
    }

    public class RequiredAuthenticationAttribute : ActionFilterAttribute
    {
        string returnURL = String.Empty;

        AuthenticationMode authenticationMode;

        /// <summary>
        /// Constructor Method For Attribute
        /// </summary>
        /// <param name="authenticated">Parameter for checking for authentication state/param>
        /// <param name="redirectURL">URL to redirect when authentication is made and requested page requires only non-authenticated users</param>
        public RequiredAuthenticationAttribute(AuthenticationMode authenticationMode, String redirectURL)
        {
            this.returnURL = redirectURL;
            this.authenticationMode = authenticationMode;
        }

        public RequiredAuthenticationAttribute(AuthenticationMode authenticationMode, String action, String controller)
        {
            UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
            this.returnURL = URL.Action(action, controller);
            this.authenticationMode = authenticationMode;

        }

        public RequiredAuthenticationAttribute(AuthenticationMode authenticationMode)
        {
            UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
            this.returnURL = URL.Action("Index", "Home");
            this.authenticationMode = authenticationMode;

        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpRequestBase Request = filterContext.RequestContext.HttpContext.Request;
            if (this.authenticationMode != AuthenticationMode.AllowAll)
            {
                bool isAuthenticated = Request.IsAuthenticated;
                if (this.authenticationMode == AuthenticationMode.AuthenticatedOnly && !isAuthenticated)
                {
                    string returnURL = String.Empty;
                    if (filterContext.RouteData.Values.ContainsKey("organizationID"))
                    {
                        returnURL = String.Format("{0}.{1}{2}", filterContext.RouteData.Values["subdomain"], ApplicationConfiguration.Path, Request.Url.PathAndQuery);
                    }
                    else
                    {
                        returnURL = Request.Url.PathAndQuery;
                    }
                  
                    filterContext.Result = new RedirectResult(String.Format("{0}{1}?returnUrl={2}", ApplicationConfiguration.AppRoot,FormsAuthentication.LoginUrl, returnURL));
                }
                else if (this.authenticationMode == AuthenticationMode.NonAuthenticatedOnly && isAuthenticated)
                {
                    string rtnURL = Request.GetReturnUrl();
                    if (!rtnURL.IsEmpty())
                    {
                        filterContext.Result = new RedirectResult("http://" + rtnURL);

                    }
                    else
                    {
                        filterContext.Result = new RedirectResult(returnURL);
                    }
                }
            }
        }
    }
}
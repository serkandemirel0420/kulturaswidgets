﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Common
{
    public interface CookieSerializable<T>
    {
        String Serialize();
        T DeSerialize(String value);
    }
}
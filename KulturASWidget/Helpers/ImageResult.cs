﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace KulturASWidget.Helpers
{
    public class ImageResult : ActionResult
    {
        private Stream imageStream;
        string filename;

        public ImageResult(Stream image, string p_filename)
        {
            imageStream = image;
            imageStream.Position = 0;
            filename = p_filename;
        }
        public override void ExecuteResult(ControllerContext context)
        {

            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            HttpResponseBase response = context.HttpContext.Response;

            response.ContentType = "image/png";

            byte[] buffer = new byte[4096];

            while (true)
            {
                int read = this.imageStream.Read(buffer, 0, buffer.Length);
                if (read == 0)
                {
                    break;
                }

                response.OutputStream.Write(buffer, 0, read);
            }

            response.AddHeader("Content-Disposition", "attachment; filename=\"" + filename + ".png\"");
            response.End();

        }

    }
}
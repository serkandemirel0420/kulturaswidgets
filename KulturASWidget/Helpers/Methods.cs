﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using KulturASWidget.Controllers;
using KulturASWidget.Objects;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security.Cryptography;
using KulturASWidget.Services;

using System.IO;

namespace System
{
    public class Methods
    {
        private static volatile Methods instance;
        private static object syncRoot = new Object();

        private Methods()
        {

        }

        public static Methods Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Methods();
                    }
                }

                return instance;
            }
        }

        public Dictionary<string, object> AnonymousObjectToDictionary(object propertyBag)
        {
            var result = new Dictionary<string, object>();
            if (propertyBag != null)
            {
                foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(propertyBag))
                {
                    result.Add(property.Name, property.GetValue(propertyBag));
                }
            }
            return result;
        }

        public bool IsEventURLTaken(int organizationID, string eventURL)
        {
            List<EventAIO> eventList = EventService.Instance.GetByUserID(organizationID, true);
            if (!eventList.IsEmpty())
            {
                return eventList.Where(x => x.UserEvent.EventURL == eventURL).Count() != 0;
            }
            return false;
        }

        public String GenerateRandomAlphanumeric(int length)
        {


            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
                                .Select(s => s[getRandom(s.Length)])
                                .ToArray());
        }

        public int getRandom(int max)
        {
            byte[] data = new byte[32];
            var random = RandomNumberGenerator.Create();
            random.GetNonZeroBytes(data);
            return Math.Abs(convertByteToInt(data)) % max;
        }

        public int convertByteToInt(byte[] b)
        {
            int value = 0;
            for (int i = 0; i < b.Length; i++)
            {
                value = (value << 8) | b[i];
            }
            return value;
        }

        public bool CompareAndAssingDifferences<T>(T source, T dest)
        {
            var sourceType = source.GetType();
            var properties = sourceType.GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var value = properties[i].GetValue(source, null);
                if (value.IsExists() && properties[i].CanWrite && !value.Equals(GetDefault(value.GetType())))
                {
                    properties[i].SetValue(dest, value, null);
                }
            }
            return true;
        }

        public object GetDefault(Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static DateTime UtcToday
        {
            get
            {
                return DateTime.UtcNow.Date;
            }
        }

       
        /// <summary>
        /// Gets CRC32 hash of file
        /// </summary>
        /// <param name="path">path of file</param>
        /// <returns>hash of file</returns>
        public string GetFileHash(string path)
        {
            KulturASWidget.Common.Crc32 crc32 = new KulturASWidget.Common.Crc32();
            String hash = String.Empty;
            using (FileStream fs = File.Open(HttpContext.Current.Server.MapPath(path), FileMode.Open))
            {
                foreach (byte b in crc32.ComputeHash(fs))
                {
                    hash += b.ToString("x2").ToLower();
                }
            }
            return hash;
        }
    }
}
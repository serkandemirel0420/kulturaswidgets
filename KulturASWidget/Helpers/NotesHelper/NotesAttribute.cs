﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Helpers.NotesHelper
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    sealed class NotesAttribute : Attribute
    {
        public String notesGroup { get; set; }
        public String extraFor { get; set; }
        public bool isExtra { get; set; }
        public NoteMode mode { get; set; }
        public NotesAttribute(String notesGroup)
        {
            this.notesGroup = notesGroup;
            this.extraFor = "";
            this.mode = NoteMode.Normal;
        }

        public NotesAttribute(String notesGroup,NoteMode mode)
        {
            this.notesGroup = notesGroup;
            this.extraFor = "";
            this.mode = mode;
        }
        public NotesAttribute(String notesGroup, String extraFor)
        {
            this.notesGroup = notesGroup;
            this.extraFor = extraFor;
            this.isExtra = true;
        }

        public const String EXTRA_DICTIONARY_NOTES_GROUP = "ExtraDictionaryNotesGroup";
    }

    enum NoteMode
    {
        Normal,ReadOnly,WriteOnly
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Collections;
using System.Reflection;

namespace KulturASWidget.Helpers.NotesHelper
{
    /// <summary>
    /// Creates json string from object. Json contains properties that has NotesAttribute. 
    /// To use NotesAttribute must provide "group" parameter.
    /// With group parameter you can create json with different groups
    /// </summary>
    public class NotesCreator
    {
        private static volatile NotesCreator instance;
        private static object syncRoot = new Object();
        private NotesCreator() { }
        public static NotesCreator Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new NotesCreator();
                    }
                }
                return instance;
            }
        }

        public string GetJsonString(Object obj, String notesGroup)
        {
            Dictionary<String, object> objToSerialize = new Dictionary<string, object>();
            PropertyInfo extraProp = null;
            var properties = obj.GetType().GetProperties();
            for (int i = 0; i < properties.Length; i++)
            {
                var prop = properties[i];
                var attributes = (NotesAttribute[])prop.GetCustomAttributes(typeof(NotesAttribute), false);
                if (!attributes.IsEmpty())
                {
                    var notesAttributes = (from attr in attributes where attr.notesGroup == notesGroup select attr).ToList();
                    if (!notesAttributes.IsEmpty())
                    {
                        if (notesAttributes.First().mode != NoteMode.WriteOnly)
                        {
                            objToSerialize.Add(prop.Name, prop.GetValue(obj, null));
                        }


                    }
                    //Check if propery is extra area
                    var extraPropList = (from attr in attributes where attr.notesGroup == NotesAttribute.EXTRA_DICTIONARY_NOTES_GROUP && attr.extraFor == notesGroup select attr).ToList();
                    if (!extraPropList.IsEmpty())
                    {
                        extraProp = prop;
                    }
                }
            }
            //if there is extra area put values to serialize
            if (extraProp.IsExists())
            {
                Dictionary<String, object> extras = (Dictionary<String, object>)extraProp.GetValue(obj, null);
                foreach (var item in extras)
                {
                    objToSerialize.Add(item.Key, item.Value);
                }
            }
            return JsonConvert.SerializeObject(objToSerialize);
        }

        public void SetValuesToObject(Object obj, string values, string notesGroup)
        {
            if (obj.IsExists() && values.IsExists())
            {
                try
                {
                    Object deserializedObj = JsonConvert.DeserializeObject(values, obj.GetType());
                    Dictionary<String, Object> extras = JsonConvert.DeserializeObject<Dictionary<String, Object>>(values);
                    var properties = obj.GetType().GetProperties();
                    PropertyInfo extraProp = null;
                    for (int i = 0; i < properties.Length; i++)
                    {

                        var prop = properties[i];
                        extras.Remove(prop.Name);
                        var attributes = (NotesAttribute[])prop.GetCustomAttributes(typeof(NotesAttribute), false);
                        if (!attributes.IsEmpty())
                        {
                            var notesAttributes = (from attr in attributes where attr.notesGroup == notesGroup select attr).ToList();
                            if (!notesAttributes.IsEmpty())
                            {
                                if (notesAttributes.First().mode != NoteMode.ReadOnly)
                                {
                                    prop.SetValue(obj, prop.GetValue(deserializedObj, null), null);
                                }

                            }
                            //Check if propery is extra area
                            var extraPropList = (from attr in attributes where attr.notesGroup == NotesAttribute.EXTRA_DICTIONARY_NOTES_GROUP && attr.extraFor == notesGroup select attr);
                            if (!extraPropList.IsEmpty())
                            {
                                extraProp = prop;
                            }
                        }
                    }
                    //if there is extra area get values
                    if (extraProp.IsExists())
                    {
                        extraProp.SetValue(obj, extras, null);
                    }
                }
                catch (JsonException ex)
                {
                    var errObj = new { action="SetNotesToObject",exceptionMessage = ex.Message,values=values,group=notesGroup};
                    Exception e = new Exception(JsonConvert.SerializeObject(errObj), ex);
                    Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                }
            }


        }
    }
}
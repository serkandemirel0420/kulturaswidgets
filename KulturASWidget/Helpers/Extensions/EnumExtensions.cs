﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Resources;
namespace System
{
    public static class EnumExtensions
    {

        /// <summary>
        /// Generates Selectlist For Enums
        /// Selectlist's values are enums values and
        /// display values  are obtained from resource with spesific prefix.
        /// <example>
        /// if prefix is 'PRE' and enums values are 'enum1','enum2','enum3'
        /// required resource names are 'PREenum1','PREenum2','PREenum3'.
        /// All resources must be exists otherwise empty list will return
        /// </example>
        /// </summary>
        /// <typeparam name="TEnum">Type of enumeration.</typeparam>
        /// <param name="enumObj">Enum to generate for.</param>
        /// <param name="resourcePrefix">Prefix for resource names.</param>
        /// <returns>Generated selectlist</returns>
        public static SelectList GenerateSelectList<TEnum>(this TEnum enumObj, String prefix)
        {
            var enumList = from TEnum e in Enum.GetValues(typeof(TEnum))
                            select e.ToString();
            try
            {
                var values = from en in enumList
                             select
                                 new
                                 {
                                     key = en,
                                     value = typeof(PageResources).GetProperty(String.Format("{0}{1}", prefix, en)).GetValue(null, null).ToString()
                                 };
                return new SelectList(values, "key", "value", enumObj.ToString());
            }
            catch (NullReferenceException ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return new SelectList(new List<String>());

        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Common;
using System.IO;

namespace System
{
    public static class UrlHelperExtensions
    {
        public static Dictionary<string, string> ContentURLCache = new Dictionary<string, string>();

        public static String ContentWithCacheBuster(this UrlHelper Url, string path)
        {
            var contentURL = "";
            if (ContentURLCache.ContainsKey(path))
            {
                contentURL = ContentURLCache[path];
            }
            else
            {
                try
                {
                    var hash = Methods.Instance.GetFileHash(path);
                    contentURL = String.Format("{0}?h={1}", Url.Content(path), hash);
                    ContentURLCache.Add(path, contentURL);
                }
                catch
                {
                    contentURL = Url.Content(path);
                }
            }
            return contentURL;
        }

    }
}
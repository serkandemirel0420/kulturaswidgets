﻿
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Resources;
using KulturASWidget.Common;

namespace System
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString GeneralSuccess<TModel>(this HtmlHelper<TModel> html, object htmlAttributes = null)
        {
            String errorMessage = html.ViewBag.GeneralSuccessText;
            Dictionary<string, object> attributes = Methods.Instance.AnonymousObjectToDictionary(htmlAttributes);

            TagBuilder validation_message = new TagBuilder("div");
            TagBuilder alert = new TagBuilder("div");
            TagBuilder field_validation_error = new TagBuilder("div");

            validation_message.AddCssClass("validation_message");
            alert.AddCssClass("alert");
            alert.AddCssClass("alert-success");
            if (errorMessage.IsEmpty())
            {
                validation_message.AddCssClass("valid");
            }
            else
            {
                //field_validation_error.SetInnerText(errorMessage);
                field_validation_error.InnerHtml = errorMessage;
            }
            if (attributes.ContainsKey("class"))
            {
                validation_message.AddCssClass(attributes["class"].ToString());
            }

            foreach (var item in attributes)
            {
                validation_message.Attributes.Add(item.Key, item.Value.ToString());
            }
            alert.InnerHtml = field_validation_error.ToString();
            validation_message.InnerHtml = alert.ToString();

            return new MvcHtmlString(validation_message.ToString());
        }
        public static MvcHtmlString GeneralError<TModel>(this HtmlHelper<TModel> html, object htmlAttributes = null)
        {
            String errorMessage = html.ViewBag.GeneralErrorText;
            Dictionary<string, object> attributes = Methods.Instance.AnonymousObjectToDictionary(htmlAttributes);
            
            TagBuilder validation_message = new TagBuilder("div");
            TagBuilder alert = new TagBuilder("div");
            TagBuilder field_validation_error = new TagBuilder("div");

            validation_message.AddCssClass("validation_message");
            alert.AddCssClass("alert");
            alert.AddCssClass("alert-error");
            if (errorMessage.IsEmpty())
            {
                validation_message.AddCssClass("valid");
            }
            else
            {
                //field_validation_error.SetInnerText(errorMessage);
                field_validation_error.InnerHtml = errorMessage;
            }
            if (attributes.ContainsKey("class"))
            {
                validation_message.AddCssClass(attributes["class"].ToString());
            }

            foreach (var item in attributes)
            {
                validation_message.Attributes.Add(item.Key, item.Value.ToString());
            }
            alert.InnerHtml = field_validation_error.ToString();
            validation_message.InnerHtml = alert.ToString();

            return new MvcHtmlString(validation_message.ToString());
        }
        public static MvcHtmlString ModelStateError<TModel>(this HtmlHelper<TModel> html, object htmlAttributes = null)
        {
            Dictionary<string, object> attributes = Methods.Instance.AnonymousObjectToDictionary(htmlAttributes);
            List<String> errorMessageList = html.ViewBag.ModelStateErrorList;
            String classAttribute = String.Empty;
            TagBuilder divBuilder = new TagBuilder("div");

            if (errorMessageList.IsEmpty())
            {
                classAttribute = "valid";
            }
            else
            {
                String errorMessages = string.Empty;

                foreach (var errorMessage in errorMessageList)
                {
                    errorMessages += String.Format("<p class=\"invalid\"><span class=\"field-validation-error\" >{0}</span></p>", errorMessage);
                }
                divBuilder.InnerHtml = errorMessages;
            }

            if (attributes.ContainsKey("class"))
            {
                classAttribute = String.Format("{0} {1}", classAttribute, attributes["class"].ToString());
                attributes.Remove("class");
            }
            divBuilder.Attributes.Add("class", String.Format("validation_message {0}", classAttribute));

            foreach (var item in attributes)
            {
                divBuilder.Attributes.Add(item.Key, item.Value.ToString());
            }

            return new MvcHtmlString(divBuilder.ToString());
        }

        public static void SetFooterType(this HtmlHelper page, FooterType type)
        {
            page.ViewBag.FooterType = type;
        }

        public static FooterType GetFooterType(this HtmlHelper page)
        {
            return page.ViewBag.FooterType;
        }

        public static void SetHeaderType(this HtmlHelper page, HeaderType type)
        {
            page.ViewBag.HeaderType = type;
        }

        public static void SetPageType(this HtmlHelper page, PageType type)
        {
            page.ViewBag.PageType = type;
        }

        public static PageType GetPageType(this HtmlHelper page)
        {
            return page.ViewBag.PageType;
        }


        public static HeaderType GetHeaderType(this HtmlHelper page)
        {
            return page.ViewBag.HeaderType;
        }

        public static void SetMetaTitle(this HtmlHelper page, String title)
        {
            page.ViewBag.MetaTitle = title;
        }

        public static String GetMetaTitle(this HtmlHelper page)
        {

            String text = page.ViewBag.MetaTitle;
            if (text.IsExists())
            {
                String postFix = page.ViewBag.TitlePostfix;
                if (!postFix.IsExists())
                {
                    postFix = KulturASWidget.Resources.PageResources.DefaultPageTitle;
                }
                return String.Format("{0} — {1}", text, postFix); ;
            }
            else
            {
                return PageResources.MainPageMetaTitle;
            }

        }

        public static void SetMetaDescription(this HtmlHelper page, String title)
        {
            page.ViewBag.MetaDescription = title;
        }

        public static String GetMetaDescription(this HtmlHelper page)
        {
            String text = page.ViewBag.MetaDescription;
            if (text.IsExists())
            {
                return text;
            }
            else
            {
                return PageResources.MainPageMetaDecription;
            }
        }

        public static void SetMetaImageUrl(this HtmlHelper page, String title)
        {
            page.ViewBag.MetaImageUrl = title;
        }

        public static String GetMetaImageUrl(this HtmlHelper page)
        {
            String text = page.ViewBag.MetaImageUrl;
            if (text.IsExists())
            {
                return text;
            }
            else
            {
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                return String.Format("{0}{1}", ApplicationConfiguration.AppRoot, Url.Content("~/Static/images/facebook_thumb.jpg"));
            }
        }

        public static void SetMetaKeywords(this HtmlHelper page, String title)
        {
            page.ViewBag.MetaKeywords = title;
        }

        public static String GetMetaKeywords(this HtmlHelper page)
        {

            String text = page.ViewBag.MetaKeywords;
            if (text.IsExists())
            {
                return text;
            }
            else
            {
                return PageResources.MainPageMetaKeywords;
            }
        }
    }
}
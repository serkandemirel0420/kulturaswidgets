﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Globalization;

namespace System
{
    public static class StringExtensions
    {
        private static Dictionary<char, char> turkishChars = new Dictionary<char, char>();

        // private static List<String> noReturnURLs = new List<string>();
        static StringExtensions()
        {
            turkishChars.Add('ç', 'c');
            turkishChars.Add('ı', 'i');
            turkishChars.Add('ğ', 'g');
            turkishChars.Add('ö', 'o');
            turkishChars.Add('ş', 's');
            turkishChars.Add('ü', 'u');
            turkishChars.Add('Ç', 'C');
            turkishChars.Add('İ', 'I');
            turkishChars.Add('Ğ', 'G');
            turkishChars.Add('Ö', 'O');
            turkishChars.Add('Ş', 'S');
            turkishChars.Add('Ü', 'U');



            /* Daha sonra kullanılacak  
             * UrlHelper URL = new UrlHelper(HttpContext.Current.Request.RequestContext);
             noReturnURLs.Add(URL.Action("UserLogon","User"));
             noReturnURLs.Add(URL.Action("UserSignUp", "User"));
             noReturnURLs.Add(URL.Action("UserCreationSuccesfull", "User"));*/
        }

        public static Boolean IsEmpty(this string obj)
        {
            return String.IsNullOrEmpty(obj) || String.IsNullOrEmpty(obj.Trim());
        }

        public static string ClearWhiteSpaces(this string str)
        {
            if (!str.IsEmpty())
            {
                return new string(str.Where(c => !char.IsWhiteSpace(c)).ToArray());
            }
            return str;
        }
        public static string ClearNonDigits(this string str)
        {
            return new string(str.Where(c => Char.IsNumber(c)).ToArray());
        }

        public static string ClearNonAlphanumeric(this string str)
        {
            return new string(str.Where(c => Char.IsLetterOrDigit(c)).ToArray());
        }

        public static bool IsAllLetters(this string str)
        {
            return str.Where(c => Char.IsLetter(c)).Count() == str.Length;
        }

        public static bool IsAllDigits(this string str)
        {

            return str.Where(c => Char.IsDigit(c)).Count() == str.Length;

        }

        public static bool IsAlphanumeric(this string str)
        {
            return str.Where(c => Char.IsLetterOrDigit(c)).Count() == str.Length;
        }

        public static string ClipToLength(this string str, int length)
        {
            if (!String.IsNullOrEmpty(str))
            {
                if (str.Length > 10)
                {
                    str = str.Remove(0, str.Length - 10);
                }
            }
            return str;
        }

        public static bool IsEmailAddress(this String text)
        {
            Regex emailRegex = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            return !String.IsNullOrEmpty(text) && emailRegex.IsMatch(text);
        }

        public static String FormatPhoneNumber(this String phoneNumber)
        {
            if (!phoneNumber.IsEmpty())
            {
                phoneNumber = phoneNumber.ClearWhiteSpaces().ClearNonDigits();
                phoneNumber = phoneNumber.ClipToLength(Constants.PHONE_MAX_LENGTH);
                return phoneNumber;
            }
            return String.Empty;
        }

        public static bool IsSameUrl(this string testUrl, string url)
        {
            return !String.IsNullOrEmpty(testUrl) && !String.IsNullOrEmpty(url) && testUrl.ToLower().StartsWith(url.ToLower());
        }

        public static bool IsValidUrl(this string url)
        {
            UrlHelper urlHelper = new UrlHelper(HttpContext.Current.Request.RequestContext);
            return urlHelper.IsLocalUrl(url) && url.Length > 1 && url.StartsWith("/")
                        && !url.StartsWith("//") && !url.StartsWith("/\\");
        }

        public static String PrepareReturnUrl(this string url)
        {
            String returnUrl = String.Empty;
            if (!String.IsNullOrEmpty(url))
            {
                returnUrl = url.ToString();
                UrlHelper Url = new UrlHelper(HttpContext.Current.Request.RequestContext);
                if (!returnUrl.IsEmpty())
                {
                    returnUrl = returnUrl.ToLower();
                }
                else
                {
                    returnUrl = Url.Action("Index", "Home");
                }
                string logonURL = Url.Action("Logon", "User").ToLower();
                if (returnUrl.StartsWith(logonURL))
                {
                    int questionMarkIndex;
                    if ((questionMarkIndex = returnUrl.IndexOf('?')).IsValidIndex())
                    {
                        returnUrl = returnUrl.Substring(questionMarkIndex);
                        returnUrl = returnUrl.Substring(returnUrl.IndexOf('=') + 1);
                        returnUrl = Uri.UnescapeDataString(returnUrl);
                    }
                    else
                    {
                        returnUrl = "";
                    }

                }
                else if (returnUrl.IsSameUrl(Url.Action("Index", "Error")))
                {
                    returnUrl = Url.Action("Index", "Home");
                }
            }
            return returnUrl;
        }
        public static String ConvertToEnglishChars(this String str)
        {
            foreach (var item in turkishChars)
            {
                str = str.Replace(item.Key, item.Value);
            }
            return str;
        }

        public static String ConverToLowerEnglishChars(this String str)
        {
            return ConvertToEnglishChars(str).ToLower(new CultureInfo("en-US"));
        }

        /// <summary>
        /// Removes special chars from url to avoid invalid urls
        /// </summary>
        /// <returns>Stripped String</returns>
        public static String RemoveCharsForUrl(this String str)
        {
            str = ConverToLowerEnglishChars(str);
            str = str.Replace(" ", "_")
                .Replace(".", "")
                .Replace(",", "")
                .Replace("'", "")
                .Replace("+", "_")
                .Replace("?", "")
                .Replace("&", "_")
                .Replace(":", "");
            return str;
        }

        public static string[] SplitChar(this string str, char sepetator)
        {
            if (!(String.IsNullOrEmpty(str)))
            {
                return str.Split(new char[] { sepetator }, StringSplitOptions.RemoveEmptyEntries);
            }
            return new string[] { };
        }

        public static String ToUrlEncode(String target)
        {
            target = ConverToLowerEnglishChars(target);
            target = target.Replace(" ", "_");
            target = target.Replace(".", "");
            target = target.Replace(",", "");
            target = target.Replace("'", "");
            target = target.Replace("+", "");
            target = target.Replace(":", "");
            return target;
        }
        public static string ReplaceTurkishChars(String source)
        {
            String result = source;

            result = result.Replace("&Ouml;", "Ö");
            result = result.Replace("&#214;", "Ö");
            result = result.Replace("&ouml;", "ö");
            result = result.Replace("&#246;", "ö");
            result = result.Replace("&Uuml;", "Ü");
            result = result.Replace("&#220;", "Ü");
            result = result.Replace("&uuml;", "ü");
            result = result.Replace("&#252;", "ü");
            result = result.Replace("&Ccedil;", "Ç");
            result = result.Replace("&#199;", "Ç");
            result = result.Replace("&ccedil;", "ç");
            result = result.Replace("&#231;", "ç");
            result = result.Replace("&nbsp;", " ");
            result = result.Replace("&#304", "İ");
            result = result.Replace("&#305", "ı");
            result = result.Replace("&#286", "Ğ");
            result = result.Replace("&#287", "ğ");
            result = result.Replace("&#350", "Ş");
            result = result.Replace("&#351", "ş");
            result = result.Replace("&#305", "ı");

            return result;

        }
        public static string StripHTML(this string source)
        {
            try
            {
                string result;

                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = source.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");

                // Remove the header (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*head([^>])*>", "<head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*head( )*>)", "</head>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<head>).*(</head>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all scripts (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*script([^>])*>", "<script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*script( )*>)", "</script>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                //result = System.Text.RegularExpressions.Regex.Replace(result,
                //         @"(<script>)([^(<script>\.</script>)])*(</script>)",
                //         string.Empty,
                //         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<script>).*(</script>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // remove all styles (prepare first by clearing attributes)
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*style([^>])*>", "<style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"(<( )*(/)( )*style( )*>)", "</style>",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(<style>).*(</style>)", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert tabs in spaces of <td> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*td([^>])*>", "\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line breaks in places of <BR> and <LI> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*br( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*li( )*>", "\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // insert line paragraphs (double line breaks) in place
                // if <P>, <DIV> and <TR> tags
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*div([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*tr([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<( )*p([^>])*>", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // Remove remaining tags like <a>, links, images,
                // comments etc - anything that's enclosed inside < >
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"<[^>]*>", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // replace special characters:
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @" ", " ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&bull;", " * ",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lsaquo;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&rsaquo;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&trade;", "(tm)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&frasl;", "/",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&lt;", "<",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&gt;", ">",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&copy;", "(c)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&reg;", "(r)",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                //Replace Turkish Characters
                result = ReplaceTurkishChars(result);
                // Remove all others. More can be added, see
                // http://hotwired.lycos.com/webmonkey/reference/special_characters/
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         @"&(.{2,6});", string.Empty,
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // for testing
                //System.Text.RegularExpressions.Regex.Replace(result,
                //       this.txtRegex.Text,string.Empty,
                //       System.Text.RegularExpressions.RegexOptions.IgnoreCase);

                // make line breaking consistent
                result = result.Replace("\n", "\r");

                // Remove extra line breaks and tabs:
                // replace over 2 breaks with 2 and over 4 tabs with 4.
                // Prepare first to remove any whitespaces in between
                // the escaped characters and remove redundant tabs in between line breaks
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\t)", "\t\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\t)( )+(\r)", "\t\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)( )+(\t)", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove redundant tabs
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+(\r)", "\r\r",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Remove multiple tabs following a line break with just one tab
                result = System.Text.RegularExpressions.Regex.Replace(result,
                         "(\r)(\t)+", "\r\t",
                         System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                // Initial replacement target string for line breaks
                string breaks = "\r\r\r";
                // Initial replacement target string for tabs
                string tabs = "\t\t\t\t\t";
                for (int index = 0; index < result.Length; index++)
                {
                    result = result.Replace(breaks, "\r\r");
                    result = result.Replace(tabs, "\t\t\t\t");
                    breaks = breaks + "\r";
                    tabs = tabs + "\t";
                }

                // That's it.
                return result.Trim();
            }
            catch
            {

                return source;
            }
        }

        public static string StripWhiteSpace(this string source)
        {
            var result = String.Empty;
            if (source.IsExists())
            {
                result = source.ToString();
                // Remove HTML Development formatting
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\r", " ");
                // Replace line breaks with space
                // because browsers inserts space
                result = result.Replace("\n", " ");
                // Remove step-formatting
                result = result.Replace("\t", string.Empty);
                // Remove repeating spaces because browsers ignore them
                result = System.Text.RegularExpressions.Regex.Replace(result,
                                                                      @"( )+", " ");
            }
            return result;
        }

        public static string StripIframe(this string source)
        {
            var result = source.ToString();

            result = result.StripWhiteSpace();

            // remove all styles (prepare first by clearing attributes)
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     @"<( )*iframe([^>])*>", "<iframe>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     @"(<( )*(/)( )*iframe( )*>)", "</iframe>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     "(<iframe>).*(</iframe>)", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            return result;
        }

        public static string StripDiv(this string source)
        {
            var result = source.ToString();

            result = result.StripWhiteSpace();

            // remove all styles (prepare first by clearing attributes)
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     @"<( )*div([^>])*>", "<div>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     @"(<( )*(/)( )*div( )*>)", "</div>",
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            result = System.Text.RegularExpressions.Regex.Replace(result,
                     "(<div>).*(</div>)", string.Empty,
                     System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            return result;
        }
    }
}
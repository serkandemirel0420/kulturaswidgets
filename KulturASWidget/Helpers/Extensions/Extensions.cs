﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Globalization;
using System.ComponentModel;
using System.Text;
using KulturASWidget.Helpers;
using KulturASWidget.Objects;
using System.Web.WebPages;
using System.Dynamic;

namespace System
{
    public static class Extensions
    {

        public static bool HasError(this ModelStateDictionary modelState, string key)
        {
            if (modelState.ContainsKey(key))
            {
                var state = modelState[key];
                if (state.Errors.IsExists() && !state.Errors.IsEmpty())
                {
                    return true;
                }
            }
            return false;
        }

        public static String GetSubdomain(this HttpRequestBase request)
        {
            if (request.RequestContext.RouteData.Values.ContainsKey("subdomain"))
            {
                return request.RequestContext.RouteData.Values["subdomain"].ToString();
            }
            else
            {
                return String.Empty;
            }
        }

        public static int GetOrganizerID(this HttpRequestBase request)
        {
            if (request.RequestContext.RouteData.Values.ContainsKey("organizationID"))
            {
                return (int)request.RequestContext.RouteData.Values["organizationID"];
            }
            else
            {
                return 0;
            }
        }


        #region IEnumerable
        public static bool IsEmpty<TSource>(this IEnumerable<TSource> lst)
        {
            if (lst.IsExists())
            {
                return lst.Count() == 0;
            }
            return true;
        }
        #endregion

        #region FormCollection
        public static List<String> GetEventQuestionList(this FormCollection collection)
        {
            return new List<string>();
        }
        #endregion

        #region Integer
        public static bool IsValidIndex(this int integer)
        {
            return integer != -1;
        }
        #endregion

        #region Object
        public static bool IsExists(this object obj)
        {
            return obj != null;
        }
        #endregion


        #region HtmlHelper

        #endregion

        #region HttpRequestBase
        public static String GetReturnUrl(this HttpRequestBase Request)
        {
            return Request.QueryString.Get("returnURL");
        }
        #endregion

        #region Facebook
        public static bool IsUserApprovedPermissions(this String code)
        {
            return !code.IsEmpty();
        }

        public static bool IsAccessTokenValid(this String code)
        {
            return !code.IsEmpty();
        }
        #endregion

        #region ViewBag or any dynamic object HAS
        public static bool Has(this object obj, string propertyName)
        {
            var dynamic = obj as DynamicObject;
            if (dynamic == null) return false;
            return dynamic.GetDynamicMemberNames().Contains(propertyName);
        }
        #endregion



    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Helpers
{
    public class SessionHelper
    {
        private IDictionary<String, Object> sessionBag = new Dictionary<String, Object>();
        public static String SESSIONHELPER_INSTANCE_KEY = "SessionHelperInstanceKey";

        public bool Has(String key)
        {
            return sessionBag.ContainsKey(key);
        }

        public void Add(String key, Object value)
        {
            sessionBag.Remove(key);
            sessionBag.Add(key, value);
        }

        public T Get<T>(String key)
        {
            if (sessionBag.ContainsKey(key))
            {
                return (T)sessionBag[key];
            }
            return default(T);
        }

        public void Remove(String key)
        {
            if (sessionBag.ContainsKey(key))
            {
                sessionBag.Remove(key);
            }
        }

        public void Reset()
        {
            sessionBag = new Dictionary<String, Object>();
        }
    }
}
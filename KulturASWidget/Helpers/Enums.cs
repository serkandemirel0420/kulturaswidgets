﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public enum FooterType
    {
        NoFooter, Normal, Big
    }

    public enum HeaderType
    {
        MainPage, Normal, Admin, NoHeader
    }

    public enum PageType
    {
        Normal, Partial
    }

    public enum CommissionPayer
    {
        byOrganization, byUser
    }

    public enum VenueSourceType
    {
        Foursquare, Google
    }
    public enum EventCreateFormAction
    {
        Save, Publish
    }

    public enum Gender
    {
        undefined,
        male,
        female
    }
    public enum TicketType
    {
        email,
        sms,
        passbook
    }
    public enum Role
    {
        Admin, Sales, Booth
    }

    public enum SendMethod
    {
        Email, Sms, Passbook
    }
}
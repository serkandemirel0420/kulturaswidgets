﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Objects;

namespace KulturASWidget.Helpers
{
    public static class UrlEncoder
    {

        public static String Encode(this UrlHelper urlHelper, EventAIO aioEvent)
        {

            string urlString = aioEvent.UserEvent.EventURL + "_" + aioEvent.User.OrganizationName + "_" + aioEvent.UserVenue.Name + "_" + aioEvent.UserEvent.Name;
            urlString = StringExtensions.ToUrlEncode(ReplaceInvalidChars(urlString));
            return urlHelper.Encode(urlString);
        }

        public static string DecodeEventURL(this string url)
        {
            string res =url;
            if(url.Contains('_'))
            {
                int index = url.IndexOf('_');
                res = url.Substring(0, index);
                 
            }
            return res;
        }

        private static String ReplaceInvalidChars(String str)
        {

            for (int i = 0; i < str.Length; i++)
            {
                string prefix = str.Substring(0, i);
                string postFix = str.Substring(i + 1, str.Length - (i + 1));
                char replaceChar = Char.IsLetterOrDigit(str[i]) ? str[i] : '_';
                str = String.Format("{0}{1}{2}", prefix, replaceChar, postFix);
            }


            //String result = str.Replace("@", "_").Replace("&","_").Replace(" ","_").Replace("!","").Replace("?","");
            return str;
        }
    }
}
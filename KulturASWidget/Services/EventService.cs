﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using KulturASWidget.Services.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;

namespace KulturASWidget.Services
{
    public class EventService
    {
        private static volatile EventService instance;
        private static object syncRoot = new Object();

        private EventService()
        {

        }

        public static EventService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new EventService();
                    }
                }

                return instance;
            }
        }

        public List<EventAIO> GetByUserID(int userID, bool isMyEvents = false)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/{1}/events", ApplicationConfiguration.RestServerUrl, userID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("aio", "true");
            HttpResult httpResult = connection.Execute();
            List<EventAIO> eventList = new List<EventAIO>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }
                if (!isMyEvents)
                {
                    eventList = SelectPublic(eventList);
                }
                SortEventsByDate(eventList);
                return eventList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return eventList;
            }
            var errorObj = new { response = httpResult.Response, method = "Event/GetByUserID", eventList = eventList };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return eventList;
        }
        public EventAIO GetByID(int ID)
        {
            EventAIO Event = new EventAIO();
            String URL = String.Empty;
            URL = string.Format("{0}/event/{1}", ApplicationConfiguration.RestServerUrl, ID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("aio", "true");

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Event.deserialize(httpResult.GetAsJson());
                return Event;
            }
            else
            {
                var errorObj = new { response = httpResult.Response, method = "Event/GetByID", ev = Event };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
                return null;
            }
        }
        public Event UpdateEvent(Event Event)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/event/{1}/update", ApplicationConfiguration.RestServerUrl, Event.EventID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("venueID", Event.VenueID);
            connection.AddParameter("userID", Event.UserID);
            connection.AddParameter("name", Event.Name);

            Event.CommenceDate = TimeZoneInfo.ConvertTimeToUtc(new DateTime(Event.CommenceDate.Ticks), ApplicationConfiguration.CurrentTimezone);
            connection.AddParameter("commence", String.Format("{0:yyyyMMddHHmmss}", Event.CommenceDate));

            Event.EndDate = TimeZoneInfo.ConvertTimeToUtc(new DateTime(Event.EndDate.Ticks), ApplicationConfiguration.CurrentTimezone);
            connection.AddParameter("end", String.Format("{0:yyyyMMddHHmmss}", Event.EndDate));

            if (Event.Detail != null && Event.Detail.Length > 8000)
            {
                Event.Detail = Event.Detail.Substring(0, 8000);
            }

            connection.AddParameter("details", Event.Detail);
            connection.AddParameter("tags", JsonConvert.SerializeObject(Event.Tags));
            connection.AddParameter("status", Event.Status ? 1 : 0);
            connection.AddParameter("notes", Event.NotesJson);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Event.deserialize(httpResult.GetAsJson());
                return Event;
            }
            var errorObj = new { response = httpResult.Response, method = "Event/UpdateEvent", ev = Event };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }


        public List<EventAIO> Search(string eventName, DateTime commence, string detail, int id)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/event/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            commence = TimeZoneInfo.ConvertTimeToUtc(commence, ApplicationConfiguration.TurkeyTimeZoneInfo);

            if (commence != new DateTime(0))
            {
                connection.AddParameter("commencerangestart", String.Format("{0:yyyyMMdd}210000", commence));
                connection.AddParameter("commencerangeend", String.Format("{0:yyyyMMdd}210000", commence.AddDays(1)));
            }
            if (id != 0)
            {
                connection.AddParameter("userId", id);
            }
            connection.AddParameter("name", eventName);
            connection.AddParameter("details", detail);
            connection.AddParameter("solr", false);


            HttpResult httpResult = connection.Execute();

            List<EventAIO> eventList = new List<EventAIO>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }


            }
            else
            {

                var errorObj = new { response = httpResult.Response, method = "Event/Search", eventName = eventName, commence = commence, detail = detail, id = id, eventList = eventList };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return eventList.Where(x=>!x.UserEvent.IsPrivate && x.UserEvent.Status).ToList();
        }

        public void SortEventsByDate(List<EventAIO> events)
        {
            events.Sort(delegate(EventAIO e1, EventAIO e2)
            {
                return e1.UserEvent.CommenceDate.CompareTo(e2.UserEvent.CommenceDate);
            });
        }
        public List<EventAIO> SelectPublic(IList<EventAIO> eventList)
        {
            return (from e in eventList where !e.UserEvent.IsPrivate select e).ToList();
        }

        public List<EventAIO> GetAllEvents()
        {

            String URL = String.Empty;
            URL = string.Format("{0}/event/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("commenceRangeStart", "20000101000000");
            connection.AddParameter("solr", false);

            HttpResult httpResult = connection.Execute();

            List<EventAIO> eventList = new List<EventAIO>();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }
                return eventList;
            }
            var errorObj = new { response = httpResult.Response, method = "Event/GetAllEvents", eventList = eventList };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return eventList;

        }
        //public List<EventAIO> UnifiedSearch(String keyword, int offset, int count, DateTime date = new DateTime())
        //{

        //    String URL = String.Empty;
        //    URL = string.Format("{0}/unifiedSearch", ApplicationConfiguration.RestServerUrl);

        //    HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
        //    connection.AddParameter("query", keyword);
        //    connection.AddParameter("start", offset);
        //    connection.AddParameter("count", count);
        //    connection.AddParameter("sortOrder", "ascending");

        //    if (date != new DateTime())
        //    {
        //        connection.AddParameter("commencerangestart", String.Format("{0:yyyyMMddHHmm}00", TimeZoneInfo.ConvertTimeToUtc(date, ApplicationConfiguration.TurkeyTimeZoneInfo)));
        //    }


        //    HttpResult httpResult = connection.Execute();

        //    List<EventAIO> eventList = new List<EventAIO>();

        //    if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
        //    {
        //        foreach (var item in httpResult.GetAsJArray())
        //        {
        //            eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
        //        }
        //        return eventList;
        //    }
        //    var errorObj = new { response = httpResult.Response, method = "Event/UnifiedSearch", eventList = eventList};
        //    Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
        //    return eventList;

        //}
        //public List<EventAIO> GetSimilarEvents(EventAIO ev)
        //{

        //    String strTags = ev.UserEvent.StrTags;
        //    string[] comingTags = strTags.Split(new string[] { ":" }, StringSplitOptions.RemoveEmptyEntries);

        //    List<EventAIO> allEvents = GetAllEvents();
        //    //remove item itself
        //    foreach (var item in allEvents)
        //    {
        //        if (item.UserEvent.StrTags == strTags)
        //        {
        //            allEvents.Remove(item);
        //            break;
        //        }
        //    }
        //    List<IList<String>> tagsList = new List<IList<String>>();
        //    List<IList<String>> wantedTagsLists = new List<IList<String>>();
        //    foreach (var item in allEvents)
        //    {
        //        tagsList.Add(item.UserEvent.Tags);

        //    }
        //    foreach (var ct in comingTags)
        //    {

        //        foreach (var item in tagsList)
        //        {

        //            if (item.Contains((ct)))
        //            {
        //                wantedTagsLists.Add(item);
        //            }

        //        }

        //    }

        //    wantedTagsLists = wantedTagsLists.Distinct().ToList();
        //    List<EventAIO> similarEvents = new List<EventAIO>();

        //    foreach (var wt in wantedTagsLists)
        //    {
        //        foreach (var ae in allEvents)
        //        {
        //            if (ae.UserEvent.Tags == wt)
        //            {
        //                similarEvents.Add(ae);
        //            }
        //        }
        //    }

        //    return similarEvents;
        //}
        //public List<EventAIO> FilterEvents(List<EventAIO> eventList, DateTime TurkeyNow)
        //{
        //    var duplicated = (from ev in eventList
        //                      group ev by ev.UserEvent.EventID into duplicate
        //                      from ev in duplicate.Skip(1)
        //                      select ev);
        //    eventList.RemoveAll(ev => duplicated.Contains(ev));

        //    eventList = (from ev in eventList where !ev.UserEvent.IsPrivate && ev.UserEvent.CommenceDate > TurkeyNow && ev.UserEvent.Status select ev).ToList();

        //    EventService.Instance.SortEventsByDate(eventList);
        //    return eventList;
        //}


        public List<Layout> GetLayoutsByEventID(int ID)
        {
            List<Layout> layoutList = new List<Layout>();

            String URL = String.Empty;
            URL = string.Format("{0}/event/{1}/layouts", ApplicationConfiguration.RestServerUrl, ID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    layoutList.Add(new Layout().deserialize(item.ToObject<JObject>()));
                }
                return layoutList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return layoutList;
            }
            return null;
            // throw new BiletinoBusinessException(httpResult);
        }
        public List<EventAIO> UnifiedSearch(String keyword, int offset, int count, DateTime date = new DateTime())
        {

            String URL = String.Empty;
            URL = string.Format("{0}/unifiedSearch", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("query", keyword);
            connection.AddParameter("start", offset);
            connection.AddParameter("count", count);
            connection.AddParameter("sortOrder", "ascending");

            if (date != new DateTime())
            {
                connection.AddParameter("commencerangestart", String.Format("{0:yyyyMMddHHmm}00", TimeZoneInfo.ConvertTimeToUtc(date, ApplicationConfiguration.CurrentTimezone)));
            }


            HttpResult httpResult = connection.Execute();

            List<EventAIO> eventList = new List<EventAIO>();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }
            }

            return eventList;
        }
        public bool CreateEvent(ref Event userEvent, ModelStateDictionary ModelState)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/event/create", ApplicationConfiguration.RestServerUrl);

            //Event.End = Event.Commence;

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("venueID", userEvent.VenueID);
            connection.AddParameter("userID", userEvent.UserID);
            connection.AddParameter("name", userEvent.Name);
            DateTime UTCCommence = TimeZoneInfo.ConvertTimeToUtc(userEvent.CommenceDate, ApplicationConfiguration.CurrentTimezone);
            connection.AddParameter("commence", UTCCommence.ToString("yyyyMMddHHmmss"));

            DateTime UTCEnd = TimeZoneInfo.ConvertTimeToUtc(userEvent.EndDate, ApplicationConfiguration.CurrentTimezone);
            connection.AddParameter("end", UTCEnd.ToString("yyyyMMddHHmmss"));

            if (userEvent.Detail != null && userEvent.Detail.Length > 8000)
            {
                userEvent.Detail = userEvent.Detail.Substring(0, 8000);
            }
            connection.AddParameter("details", userEvent.Detail);
            connection.AddParameter("tags", JsonConvert.SerializeObject(userEvent.Tags));
            connection.AddParameter("notes", userEvent.NotesJson);
            connection.AddParameter("status", userEvent.Status);
            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                userEvent.deserialize(httpResult.GetAsJson());
                return true;
            }
            else
            {

            }
            return false;
        }
        public List<EventAIO> SearchByTag(string tag)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/event/search", ApplicationConfiguration.RestServerUrl);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            tag = tag.ConverToLowerEnglishChars();
            httpConnection.AddParameter("tags", HttpUtility.UrlEncode(tag));

            HttpResult httpResult = httpConnection.Execute();
            List<EventAIO> eventList = new List<EventAIO>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }
                return eventList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return eventList;
            }
            return eventList;
        }
        public List<EventAIO> SearchByCityName(string cityName)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/event/search", ApplicationConfiguration.RestServerUrl);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            httpConnection.AddParameter("city", cityName);

            HttpResult httpResult = httpConnection.Execute();
            List<EventAIO> eventList = new List<EventAIO>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    eventList.Add(new EventAIO().deserialize(item.ToObject<JObject>()));
                }
                return eventList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return eventList;
            }
            return eventList;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using KulturASWidget.Services.Http;
using System.Xml.Serialization;
using System.IO;
using KulturASWidget.Common;
using KulturASWidget.Objects;
using KulturASWidget.Models.Garanti.Response;
using KulturASWidget.Models.Garanti;

namespace KulturASWidget.Services.Security
{
    public class GarantiService
    {
        private static volatile GarantiService instance;
        private static object syncRoot = new Object();

        private GarantiService()
        {

        }


        public static GarantiService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new GarantiService();
                    }
                }

                return instance;
            }
        }

        public string GetSHA1(string SHA1Data)
        {
            SHA1 sha = new SHA1CryptoServiceProvider();
            string HashedPassword = SHA1Data;
            byte[] hashbytes = Encoding.GetEncoding("ISO-8859-9").GetBytes(HashedPassword);
            byte[] inputbytes = sha.ComputeHash(hashbytes);
            return GetHexaDecimal(inputbytes);
        }

        public string GetHexaDecimal(byte[] bytes)
        {
            StringBuilder s = new StringBuilder();
            int length = bytes.Length;
            for (int n = 0; n <= length - 1; n++)
            {
                s.Append(String.Format("{0,2:x}", bytes[n]).Replace(" ", "0"));
            }
            return s.ToString();
        }


        public Garanti3DRequest GetWidgetRequestObject(HttpRequestBase Request, User user, double amount, string orderID, UrlHelper Url, string secureKey, string provpassword)
        {
            string SuccessURL = String.Format("{0}{1}", ApplicationConfiguration.AppRoot, Url.Action("CreditCardPaymentReturn", "Home"));
            string ErrorURL = String.Format("{0}{1}", ApplicationConfiguration.AppRoot, Url.Action("CreditCardPaymentReturn", "Home"));

            string TerminalUserID = "PROVAUT"; //Sabit
            string TerminalID = ApplicationConfiguration.GarantiTerminalID; //8 Haneli TerminalID yazılmalı. ----------------!!-----------
            string TerminalMerchantID = ApplicationConfiguration.GarantiMarchantID; //Üye İşyeri Numarası ----------------!!-----------
            String IPAddress = ApplicationConfiguration.IsDevelopment ? ApplicationConfiguration.OfficeIPAddress : Request.UserHostAddress;
            Garanti3DRequest request = new Garanti3DRequest(SuccessURL, ErrorURL, secureKey, provpassword, amount, orderID, IPAddress, user.Mail, TerminalID, TerminalUserID, TerminalMerchantID);
            //request.Mode = "TEST";

            return request;
        }


        public Garanti3DRequest GetRequestObject(HttpRequestBase Request, User user, double amount, string orderID, UrlHelper Url, string secureKey, string provpassword)
        {
            string SuccessURL = String.Format("{0}{1}", ApplicationConfiguration.AppRoot, Url.Action("CreditCardPaymentReturn", "Event"));
            string ErrorURL = String.Format("{0}{1}", ApplicationConfiguration.AppRoot, Url.Action("CreditCardPaymentReturn", "Event"));

            string TerminalUserID = "PROVAUT"; //Sabit
            string TerminalID = ApplicationConfiguration.GarantiTerminalID; //8 Haneli TerminalID yazılmalı. ----------------!!-----------
            string TerminalMerchantID = ApplicationConfiguration.GarantiMarchantID; //Üye İşyeri Numarası ----------------!!-----------
            String IPAddress = ApplicationConfiguration.IsDevelopment ? ApplicationConfiguration.OfficeIPAddress : Request.UserHostAddress;
            Garanti3DRequest request = new Garanti3DRequest(SuccessURL, ErrorURL, secureKey, provpassword, amount, orderID, IPAddress, user.Mail, TerminalID, TerminalUserID, TerminalMerchantID);
            //request.Mode = "TEST";

            return request;
        }

        public GVPSResponse MakeProvisionRequest(Garanti3DResponse requestResponse)
        {
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.POST, ApplicationConfiguration.GarantiProvURL);
            connection.AddBody("data=" + requestResponse.XML);

            HttpResult result = connection.Execute();

            XmlSerializer responseSerializer = new XmlSerializer(typeof(GVPSResponse));
            byte[] byteArray = Encoding.ASCII.GetBytes(result.Response);
            return (GVPSResponse)responseSerializer.Deserialize(new MemoryStream(byteArray));
        }
       
    }
}
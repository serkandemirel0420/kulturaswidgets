﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using KulturASWidget.Objects;
using KulturASWidget.Services;


namespace KulturASWidget.Services.Security
{
    public class BiletinoRoleProvider : RoleProvider
    {
        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        private const int AdminRoleID = 1;
        public override string[] GetRolesForUser(string username)
        {
            User user = UserService.Instance.GetByID(Int32.Parse(username));
            if (user.RoleID == AdminRoleID)
            {
                return new string[] { "ROLE_ADMIN", "ROLE_USER" }; 
            }
           
            return new string[] { "ROLE_USER" };
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            if (roleName == "ROLE_ADMIN")
            {
                User user = UserService.Instance.GetByID(Int32.Parse(username));
                /*if (username.IsEmailAddress())
                {
                    user = UserService.Instance.GetUserByMail(username);
                }
                else
                {
                    user = UserService.Instance.GetUserByFacebookId(username);
                }*/
                if (user.RoleID == AdminRoleID)
                {
                    return true;
                }
                return false;
            }

            return !String.IsNullOrEmpty(username);
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}
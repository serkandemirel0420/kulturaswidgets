﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using KulturASWidget.Services.Http;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;


namespace KulturASWidget.Service
{
    public class VenueService
    {
        private static volatile VenueService instance;
        private static object syncRoot = new Object();

        private VenueService()
        {

        }


        public static VenueService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new VenueService();
                    }
                }

                return instance;
            }
        }

        public bool CreateVenue(ref Venue venue,ModelStateDictionary modelstate)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/venue/create", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("userID", venue.UserID);
            connection.AddParameter("name", (venue.Name));
            connection.AddParameter("longitude", venue.Longitude);
            connection.AddParameter("latitude", venue.Latitude);
            connection.AddParameter("countryCode", venue.CountryCode);
            connection.AddParameter("city", (venue.City));
            connection.AddParameter("district", (venue.District));
            connection.AddParameter("address", (venue.Address));
            connection.AddParameter("details", (venue.Details));
            connection.AddParameter("FoursquareId", (venue.FoursquareId));
            connection.AddParameter("notes", venue.NotesJson);
            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                venue.deserialize(httpResult.GetAsJson());
                return true;
            }
            return false;
        }
        public Venue UpdateVenue(Venue venue)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/venue/{1}/update", ApplicationConfiguration.RestServerUrl,venue.VenueID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("userID", venue.UserID);
            connection.AddParameter("name", (venue.Name));
            connection.AddParameter("longitude", venue.Longitude);
            connection.AddParameter("latitude", venue.Latitude);
            connection.AddParameter("countryCode", (venue.CountryCode));
            connection.AddParameter("city", (venue.City));
            connection.AddParameter("district", (venue.District));
            connection.AddParameter("address", (venue.Address));
            connection.AddParameter("details", (venue.Details));
            connection.AddParameter("notes", venue.NotesJson);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                venue.deserialize(httpResult.GetAsJson());
                
            }
            return venue;
        }
        public Venue FindBy(int ID)
        {
            Venue venue = new Venue();
            String URL = String.Empty;
            URL = string.Format("{0}/venue/{1}", ApplicationConfiguration.RestServerUrl, ID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            
            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                //JObject jobject = JObject.Parse(httpResult.Response.Substring(1, httpResult.Response.Length - 2));
                venue.deserialize(httpResult.GetAsJson());
                return venue;
            }
            return venue;
        }
     
        public IList<Venue> Search(String name)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/venue/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("name",( name));
          

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                IList<Venue> venueList = new List<Venue>();
                var result = JArray.Parse(httpResult.Response);

                for (int i = 0; i < result.Count; i++)
                {
                    var item = result[i].ToObject<JObject>();
                    venueList.Add(new Venue().deserialize(item));
                }
                return venueList;
            }
            return null;
        }
        public List<Venue> GetAllVenues()
        {
            String URL = String.Empty;
            URL = string.Format("{0}/venue/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("name", "%");


            HttpResult httpResult = connection.Execute();
            List<Venue> venueList = new List<Venue>();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                
                var result = JArray.Parse(httpResult.Response);

                for (int i = 0; i < result.Count; i++)
                {
                    venueList.Add(new Venue().deserialize(result[i].ToObject<JObject>()));
                } 
            }

            return venueList;
                
        }
        
    }
}
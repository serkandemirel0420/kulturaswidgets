﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using KulturASWidget.Services.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using KulturASWidget.Resources;

namespace KulturASWidget.Services
{
    public class LayoutService
    {
        private static volatile LayoutService instance;
        private static object syncRoot = new Object();

        private LayoutService()
        {

        }

        public static LayoutService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new LayoutService();
                    }
                }

                return instance;
            }
        }


        public Layout GetByID(int layoutID)
        {
            Layout layout = new Layout();
            String URL = String.Empty;
            URL = string.Format("{0}/layout/{1}", ApplicationConfiguration.RestServerUrl, layoutID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                layout.deserialize(httpResult.GetAsJson()); 
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Layout/GetByLayoutID", layoutID = layoutID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return layout;
        }

        public List<Layout> GetByEvent(int eventID)
        {
            List<Layout> layoutList = new List<Layout>();

            String URL = String.Empty;
            URL = string.Format("{0}/event/{1}/layouts", ApplicationConfiguration.RestServerUrl, eventID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    layoutList.Add(new Layout().deserialize(item.ToObject<JObject>()));
                }
                return layoutList;
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Layout/GetByEvent", eventID = eventID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return layoutList;
        }

        public bool CreateLayout(ref Layout layout, out ServiceError errorMessage)
        {
            errorMessage = new ServiceError();
            String URL = String.Empty;
            URL = string.Format("{0}/layout/create", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("eventID", layout.EventID);
            connection.AddParameter("name", layout.Name);
            connection.AddParameter("fee", layout.PriceWithoutCommission);
            connection.AddParameter("maxCapacity", layout.MaxCapacity);
            connection.AddParameter("notes",  layout.NotesJson);
            connection.AddParameter("status", layout.Status ? 1 : 0);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                layout.deserialize(httpResult.GetAsJson());
                return true;
            }
            else
            {
                errorMessage.errorTarget = "Layout";
                errorMessage.errorText = PageResources.CreateLayoutError;
            }
            var errorObj = new { response = httpResult.Response, method = "Layout/CreateLayout", layout = layout };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }

        public Layout UpdateLayout(Layout layout)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/layout/{1}/update", ApplicationConfiguration.RestServerUrl, layout.LayoutID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("name",  layout.Name);
            connection.AddParameter("fee", layout.PriceWithoutCommission);
            connection.AddParameter("maxCapacity", layout.MaxCapacity);
            connection.AddParameter("usedCapacity", layout.UsedCapacity);
            connection.AddParameter("status", layout.Status ? 1 : 0);
            connection.AddParameter("notes",  layout.NotesJson);


            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                layout.deserialize(httpResult.GetAsJson());
                return layout;
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Layout/UpdateLayout", layoutID = layout.LayoutID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return layout;
        }
    }
}
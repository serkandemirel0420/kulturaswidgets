﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services.Http;
using Newtonsoft.Json;
using KulturASWidget.Common;

namespace KulturASWidget.Services
{
    public class TumblrService
    {
        private static volatile TumblrService instance;
        private static object syncRoot = new Object();

        private TumblrService()
        {

        }

        public static TumblrService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new TumblrService();
                    }
                }

                return instance;
            }
        }

        private TumblrResponse postCache;
        private long cacheTime;
        public TumblrData GetPosts()
        {
            TumblrResponse res = new TumblrResponse();

            TimeSpan span = new TimeSpan(DateTime.UtcNow.Ticks - cacheTime);
            if (span.TotalMinutes > 1)
            {
                var url = "http://api.tumblr.com/v2/blog/biletino.tumblr.com/posts";
                HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, url);
                conn.AddParameter("api_key", ApplicationConfiguration.TumblrKey);
                conn.AddParameter("notes_info", "false");

                var result = conn.Execute();

                if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    res = JsonConvert.DeserializeObject<TumblrResponse>(result.Response);
                    postCache = res;
                    cacheTime = DateTime.UtcNow.Ticks;
                }
            }
            else
            {
                res = postCache;
            }
            return res.response;
        }


    }

    public class TumblrResponse
    {
        public TumblrData response { get; set; }
    }

    public class TumblrData
    {
        public TumblrBlog blog { get; set; }
        public List<TumblrPost> posts { get; set; }
        public long total_posts { get; set; }
    }
    public class TumblrBlog
    {
        public string title { get; set; }
        public long posts { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public long updated { get; set; }
        public string description { get; set; }
        public bool ask { get; set; }
        public bool ask_anon { get; set; }
        public bool share_likes { get; set; }
        public long likes { get; set; }
    }

    public class TumblrPost
    {
        public string blog_name { get; set; }
        public long id { get; set; }
        public string post_url { get; set; }
        public string slug { get; set; }
        public string type { get; set; }
        public DateTime date { get; set; }
        public long timestamp { get; set; }
        public string state { get; set; }
        public string format { get; set; }
        public string reblog_key { get; set; }
        public List<String> tags { get; set; }
        public string short_url { get; set; }
        public List<String> highlighted { get; set; }
        public int note_count { get; set; }
        public string title { get; set; }
        public string body { get; set; }
    }
}
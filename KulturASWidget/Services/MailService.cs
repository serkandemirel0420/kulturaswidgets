﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services.Http;
using KulturASWidget.Objects;

using KulturASWidget.Services.Security;

namespace KulturASWidget.Services
{
    /// <summary>
    /// Mail referans: http://api.taglon.com/communication/mail/templates
    /// </summary>
    public class MailService
    {

        private static volatile MailService instance;
        private static object syncRoot = new Object();

        private MailService()
        {

        }

        public static MailService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new MailService();
                    }
                }

                return instance;
            }
        }

        private const string MailServiceURL = "http://api.taglon.com/communication/mail/send";
        public bool SendActivationMail(User user)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "activation");
            conn.AddParameter("mail", user.Mail);
            conn.AddParameter("activationlink", user.ActivationLink);

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public bool SendTicketRequestNotificationMail(string mail, EventAIO userEvent)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "attendanceRequest");
            conn.AddParameter("mail", mail);

            conn.AddParameter("eventname", userEvent.UserEvent.Name);
            conn.AddParameter("eventdetails", userEvent.UserEvent.Detail.StripHTML());
            conn.AddParameter("eventdate", userEvent.UserEvent.FormattedCommenceLong);
            conn.AddParameter("eventvenue", userEvent.UserVenue.Name);
            conn.AddParameter("eventID", userEvent.UserEvent.EventID);

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public bool SendTicketMail(string mail, int reservationID, EventAIO userEvent)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "ticket");
            conn.AddParameter("mail", mail);

            conn.AddParameter("eventname", userEvent.UserEvent.Name);
            //conn.AddParameter("eventdetails", userEvent.UserEvent.Detail.StripHTML());
            conn.AddParameter("eventdetails", "");
            conn.AddParameter("eventdate", userEvent.UserEvent.FormattedCommenceLong);
            conn.AddParameter("eventvenue", userEvent.UserVenue.Name);
            conn.AddParameter("eventID", userEvent.UserEvent.EventID);
            conn.AddParameter("reservationID", reservationID);
            /*if (ImageService.Instance.ImageExists(String.Format("Event/{0}", userEvent.UserEvent.EventID), "Poster_0"))
            {*/
                conn.AddParameter("eventpichref", String.Format(userEvent.UserEvent.PosterBase, 0));
 /*           }
            else
            {
                conn.AddParameter("eventpichref", "http://resources-biletino.s3.amazonaws.com/images/event/placeholder/medium.jpg");
            }*/

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public bool SendResetPasswordMail(string mail, string code)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "resetpassword");
            conn.AddParameter("mail", mail);

            string href = String.Format("{0}/{1}", KulturASWidget.Common.ApplicationConfiguration.AppRoot, "User/UserResetPassword?code=" + code);
            conn.AddParameter("href", href);
            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }
       /* public bool SendContactMail(UserContactModel model)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "contact");
            conn.AddParameter("mail", "info@taglon.com");

            conn.AddParameter("name", model.FullName);
            conn.AddParameter("email", model.Mail);
            conn.AddParameter("reason", model.Reason);
            conn.AddParameter("message", model.Message);

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }*/
        public bool SendMailToOrganizer(string mail, string senderName, string senderMail, string content)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "organizerContactMail");
            conn.AddParameter("mail", mail);
            conn.AddParameter("email", senderMail);
            conn.AddParameter("sendername", senderName);
            conn.AddParameter("content", content);

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }
        
        public bool SendNotificationMailToUserForOrganizationAdding(User user, User currentOrganization)
        {
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, MailServiceURL);
            conn.AddParameter("template", "activationOfOrganization");
            conn.AddParameter("mail", user.Mail);
            CryptolographyService service = new CryptolographyService(CryptolographyService.ServiceProviderEnum.Rijndael);

            conn.AddParameter("acceptlink", String.Format("{0}/Organizer/ActivateUserInOrganization?organizationIDKey={1}&userIDKey={2}", KulturASWidget.Common.ApplicationConfiguration.AppRoot, service.Encrypt(currentOrganization.UserID.ToString()), service.Encrypt(user.UserID.ToString())));
            conn.AddParameter("organization", currentOrganization.OrganizationName);

            var response = conn.Execute();
            return response.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }


    }
}
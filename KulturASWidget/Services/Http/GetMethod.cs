﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;

using System.Text;

namespace KulturASWidget.Services.Http
{
    public class GetMethod : HttpMethod
    {
        public GetMethod(String Url)
            : base(Url)
        {

        }

        public override HttpResult Execute(IDictionary<string, object> parameters, IList<String> headers,String body)
        {
            if (parameters.Count > 0)
            {
                String postData = this.parametersToString(parameters);
                Url = Url + "?" + postData;
            }
            if (headers.Count > 0)
            {
                foreach (var item in headers)
                {
                    WebRequest.Headers.Add(item);
                }
            }

            WebRequest = HttpWebRequest.Create(Url);
            return extractResult();
        }
    }
}
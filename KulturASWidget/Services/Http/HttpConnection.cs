﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
//using log4net;
using KulturASWidget.Common;


namespace KulturASWidget.Services.Http
{
    public class HttpConnection
    {
        
        //private static readonly ILog log = LogManager.GetLogger(typeof(HttpConnection));


        public HttpMethod httpMethod;
        public HttpConnection(HttpMethod httpMethod)
        {
            this.httpMethod = httpMethod;
        }

        private IDictionary<String, Object> parameters = new Dictionary<String, Object>();
        private IList<String> headers = new List<String>();

        private String body;

        public void AddHeader(String value)
        {
            headers.Add(value);

        }

        public void AddBody(String body)
        {
            this.body = body;
        }

        public void AddParameter(String key, Object value)
        {
            parameters.Add(key, value);
        }

        public HttpResult Execute()
        {
            /*if (log.IsDebugEnabled)
                log.Debug(String.Format("Http connection is about to Execute url: {0}, params: {1}, at: {2}", httpMethod.Url, httpMethod.SerializedParams, DateTime.Now));
            */
            if (httpMethod.Url.ToLower().StartsWith(ApplicationConfiguration.RestServerUrl.ToLower()))
            {
                this.AddParameter("applicationKey", ApplicationConfiguration.AppKey);
            }
            HttpResult httpResult = httpMethod.Execute(parameters, headers, body);

            /*if (log.IsDebugEnabled)
                log.Debug(String.Format("Http connection result with status: {0} and response: {1}, at: {2}", httpResult.HttpStatusCode, httpResult.Response, DateTime.Now));
            */

            return httpResult;
        }




    }
}
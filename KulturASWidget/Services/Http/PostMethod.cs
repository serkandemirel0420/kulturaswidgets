﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using KulturASWidget.Common;

namespace KulturASWidget.Services.Http
{
    public class PostMethod : HttpMethod
    {
        public PostMethod(String url)
            : base(url)
        {
            WebRequest = HttpWebRequest.Create(url);
            WebRequest.Method = "POST";
        }

        public override HttpResult Execute(IDictionary<string, object> parameters, IList<String> headers, String body)
        {
            String postData = String.Empty;

            byte[] byteArray;

            if (parameters.Count > 0)
            {
                postData += this.parametersToString(parameters);
            }
            if (!String.IsNullOrEmpty(body))
            {
                byteArray = Encoding.UTF8.GetBytes(body);
                WebRequest.ContentType = "application/x-www-form-urlencoded";
            }else{
               
                byteArray = Encoding.UTF8.GetBytes(postData);
                WebRequest.ContentType = "application/x-www-form-urlencoded";
            }
           /* if (!String.IsNullOrEmpty(body))
            {
                byteArray = Encoding.ASCII.GetBytes(body);
                WebRequest.ContentType = "application/json";
            }
            else
            {

                byteArray = Encoding.UTF8.GetBytes(postData);
                WebRequest.ContentType = "application/x-www-form-urlencoded";
            }*/

            if (headers.Count > 0)
            {
                foreach (var item in headers)
                {
                    WebRequest.Headers.Add(item);
                }
            }

            WebRequest.ContentLength = byteArray.Length;

            using (var dataStream = WebRequest.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            return extractResult();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KulturASWidget.Services.Http
{
    public class HttpConnectionFactory
    {

        public static HttpConnection Create(HttpMethodType httpMethodType, String url)
        {

            if (httpMethodType.Equals(HttpMethodType.POST))
            {
                return new HttpConnection(new PostMethod(url));
            }
            else
            {
                return new HttpConnection(new GetMethod(url));
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using KulturASWidget.Common;

namespace KulturASWidget.Services.Http
{
    public abstract class HttpMethod
    {
        public String Url { get; protected set; }
        public WebRequest WebRequest { get; protected set; }
        public String SerializedParams;

        public HttpMethod(String Url)
        {
            this.Url = Url;
        }

        public abstract HttpResult Execute(IDictionary<String, Object> parameters,IList<String> headers,String body);

        protected String parametersToString(IDictionary<String, Object> parameters)
        {
            StringBuilder postData = new StringBuilder();

            int i = 0;
            foreach (var item in parameters)
            {
                postData.Append(item.Key);
                postData.Append("=");
                String value = item.Value.IsExists() ? HttpUtility.UrlEncode(item.Value.ToString()) : null;
                postData.Append(value);

                if (i != parameters.Count - 1)
                {
                    postData.Append("&");
                }
                i++;
            }
            SerializedParams = postData.ToString();
            return postData.ToString();
        }

        protected HttpResult extractResult()
        {
            try
            {
                using (var response = WebRequest.GetResponse())
                {
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var streamReader = new StreamReader(responseStream))
                        {
                            return new HttpResult(HttpStatusCode.OK, streamReader.ReadToEnd());
                        }
                    }
                }
            }
            catch (WebException webException)
            {
                if (webException != null && webException.Response != null && webException.Response.ContentLength != 0)
                {
                    using (var responseStream = webException.Response.GetResponseStream())
                    {
                        using (var streamReader = new StreamReader(responseStream))
                        {
                            return new HttpResult(((HttpWebResponse)webException.Response).StatusCode, streamReader.ReadToEnd());
                        }
                    }
                }
                throw new Exception(String.Format("Unexpected Result From url: {0}", Url), webException);
            }
        }
    }

    public enum HttpMethodType
    {

        GET, POST, PUT
    }
}
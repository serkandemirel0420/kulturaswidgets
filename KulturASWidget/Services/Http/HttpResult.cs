﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json.Linq;

namespace KulturASWidget.Services.Http
{
    public class HttpResult
    {
        public HttpStatusCode HttpStatusCode { get; private set; }
        public String Response { get; private set; }

        public HttpResult(HttpStatusCode httpStatusCode, String response)
        {
            this.HttpStatusCode = httpStatusCode;
            this.Response = response;
        }

        public JObject GetAsJson()
        {
            return JObject.Parse(this.Response);
        }

        public JArray GetAsJArray()
        {
            string res;
            if(this.Response.Length < 2)
            {
                res = "[]";
            }
            else if (this.Response[0] == '[')
            {
                res = this.Response;
            }
            else
            {
                res = String.Format("[{0}]", this.Response);
            }
            return JArray.Parse(res);
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using KulturASWidget.Services.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using KulturASWidget.Resources;
using KulturASWidget.Models.Garanti.Response;

namespace KulturASWidget.Services
{
    public class ReservationService
    {
        private static volatile ReservationService instance;
        private static object syncRoot = new Object();

        private ReservationService()
        {

        }
        public static ReservationService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new ReservationService();
                    }
                }
                return instance;
            }
        }
        public Reservation GetByID(int reservationID)
        {

            String URL = string.Format("{0}/reservation/{1}", ApplicationConfiguration.RestServerUrl, reservationID);
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            HttpResult httpResult = connection.Execute();

            Reservation reservation = new Reservation();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                reservation.deserialize(httpResult.GetAsJson());
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Reservation/GetByID", reservationID = reservationID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return reservation;
        }

        public Reservation Update(Reservation reservation)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/{1}/update", ApplicationConfiguration.RestServerUrl, reservation.ReservationID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("notes", reservation.NotesJson);
            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                reservation.deserialize(httpResult.GetAsJson());
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Reservation/Update", reservation = reservation, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return reservation;
        }

        public Reservation SendSMS(Reservation reservation, string cellNumber)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/{1}/sendSMS", ApplicationConfiguration.RestServerUrl, reservation.ReservationID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("MSISDN", cellNumber);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                reservation.deserialize(httpResult.GetAsJson());
            }
            else
            {
                var errorObj = new { method = "Reservation/SendSMS", reservation = reservation, cellNumber = cellNumber, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return reservation;
        }

        public List<Reservation> GetByEvent(int eventID)
        {

            String URL = string.Format("{0}/event/{1}/reservations", ApplicationConfiguration.RestServerUrl, eventID);
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            HttpResult httpResult = connection.Execute();

            List<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    reservationList.Add(new Reservation().deserialize(item.ToObject<JObject>()));
                }
                return reservationList;
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Reservation/GetByEvent", eventID = eventID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return reservationList;
        }

        public List<Reservation> GetByUser(int userID)
        {

            String URL = string.Format("{0}/user/{1}/reservations", ApplicationConfiguration.RestServerUrl, userID);
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            HttpResult httpResult = connection.Execute();

            List<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    reservationList.Add(new Reservation().deserialize(item.ToObject<JObject>()));
                }
                return reservationList;
            }
            else if (httpResult.HttpStatusCode != System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Reservation/GetByUser", userID = userID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return reservationList;
        }

        public List<Reservation> GetByLayout(int layoutID)
        {

            String URL = String.Empty;
            URL = string.Format("{0}/reservation/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("layoutID", layoutID);

            HttpResult httpResult = connection.Execute();
            List<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = httpResult.GetAsJArray();

                for (int i = 0; i < result.Count; i++)
                {
                    reservationList.Add(new Reservation().deserialize(result[i].ToObject<JObject>()));
                }
                return reservationList;
            }

            var errorObj2 = new { method = "Reservation/GetByLayout", httpResult = httpResult };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj2)));
            return reservationList;
            //throw new BiletinoBusinessException(httpResult);
        }
        public IList<Reservation> Search(int userID, int layoutID = -1)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("userID", userID);

            if (layoutID != -1)
            {
                connection.AddParameter("layoutID", layoutID);
            }


            HttpResult httpResult = connection.Execute();
            IList<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = httpResult.GetAsJArray();

                for (int i = 0; i < result.Count; i++)
                {
                    reservationList.Add(new Reservation().deserialize(result[i].ToObject<JObject>()));
                }
                return reservationList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                var errorObj = new { method = "Reservation/Search", userID = userID, httpResult = httpResult };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
                return reservationList;
            }
            var errorObj2 = new { method = "Reservation/Search", userID = userID, httpResult = httpResult };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj2)));
            return reservationList;
            //throw new BiletinoBusinessException(httpResult);
        }


        public bool Pay(ref Reservation reservation, string method, out ServiceError errorMessage)
        {
            errorMessage = new ServiceError();
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/{1}/payReservation", ApplicationConfiguration.RestServerUrl, reservation.ReservationID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("method", method);
            connection.AddParameter("transactionID", reservation.CreditCardTransactionID);
            connection.AddParameter("authorizationID", reservation.CreditCardTransactionAuthCode);
            connection.AddParameter("notes", reservation.PaymentNotesJson());

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                reservation.deserialize(httpResult.GetAsJson());
                return true;
            }
            var errorObj = new { response = httpResult.Response, method = "Reservation/Pay", reservation = reservation, errorMessage = errorMessage };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }
        public List<Reservation> PayAll(List<Reservation> resList, GVPSResponse response)
        {

            for (int i = 0; i < resList.Count; i++)
            {

                resList[i].CreditCardTransactionID = response.Transaction.RetrefNum;
                resList[i].CreditCardTransactionAuthCode = response.Transaction.AuthCode;

                resList[i].CreditCardTransactionBatchNumber = response.Transaction.BatchNum;
                resList[i].CreditCardTransactionProvDate = response.Transaction.ProvDate;
                resList[i].CreditCardTransactionSquenceNumber = response.Transaction.SequenceNum;
                resList[i].CreditCardOrderID = response.Order.OrderID;

                var reservation = resList[i];
                ServiceError error;
                ReservationService.Instance.Pay(ref reservation, "CREDITCART", out error);

            }

            return resList;
        }
        //Queue
        public Reservation QueueCreate(int userID, int layoutID)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/queue/create", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("userID", userID);
            connection.AddParameter("layoutID", layoutID);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Reservation reservation = new Reservation();
                reservation.deserialize(httpResult.GetAsJson());
                return reservation;
            }
            var errorObj = new { response = httpResult.Response, method = "Reservation/QueueCreate", userid = userID, layoutid = layoutID };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }
        public Reservation QueueFinalize(string reservationQueue)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/queue/finalize", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("reservationQueue", reservationQueue);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                Reservation reservation = new Reservation();
                reservation.deserialize(httpResult.GetAsJson());
                return reservation;
            }
            var errorObj = new { response = httpResult.Response, method = "Reservation/QueueFinalize", reservationQueue = reservationQueue };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }
        public List<Reservation> QueueGetUserQueue(int userID)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/queue/user/{1}", ApplicationConfiguration.RestServerUrl, userID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);


            HttpResult httpResult = connection.Execute();

            List<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = httpResult.GetAsJArray();

                foreach (var item in httpResult.GetAsJArray())
                {
                    reservationList.Add(new Reservation().deserialize(item.ToObject<JObject>()));
                }
            }
            return reservationList;
        }
        public List<Reservation> QueueAll()
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/queue/all", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            HttpResult httpResult = connection.Execute();

            List<Reservation> reservationList = new List<Reservation>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                var result = httpResult.GetAsJArray();

                foreach (var item in httpResult.GetAsJArray())
                {
                    reservationList.Add(new Reservation().deserialize(item.ToObject<JObject>()));
                }
            }
            return reservationList;
        }
        public bool QueueRemove(string reservationQueue)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/reservation/queue/remove", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("reservationQueue", reservationQueue);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return true;
            }
            var errorObj = new { response = httpResult.Response, method = "Reservation/QueueRemove", reservationQueue = reservationQueue };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }

        /*
        public List<Reservation> GetReservationsByEventID(int eventID)
        {
            List<Reservation> reservationList = new List<Reservation>();
            String URL = string.Format("{0}/reservation/search", ApplicationConfiguration.RestServerUrl);
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("eventID", eventID);
            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    reservationList.Add(new Reservation().deserialize(item.ToObject<JObject>()));
                }
                return reservationList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return reservationList;
            }
            return null; //TODO : Eren
            // throw new BiletinoBusinessException(httpResult);
        }*/
    }
}
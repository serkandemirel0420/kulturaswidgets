﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services.Http;
using Newtonsoft.Json.Linq;
using System.Web.Security;
using System.Text.RegularExpressions;
using KulturASWidget.Objects;
using Newtonsoft.Json;
using KulturASWidget.Common;
using System.Net;
using KulturASWidget.Resources;

namespace KulturASWidget.Services
{
    public class UserService
    {
        private static volatile UserService instance;
        private static object syncRoot = new Object();

        private UserService()
        {

        }


        public static UserService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new UserService();
                    }
                }
                return instance;
            }
        }

        public bool CreateUser(ref User user, out ServiceError errorMessage)
        {
            errorMessage = new ServiceError();
            String URL = String.Empty;
            URL = string.Format("{0}/user/create", ApplicationConfiguration.RestServerUrl);

            user.CreationDateUTC = DateTime.UtcNow;
            user.OrganizationName = user.OrganizationName.IsEmpty() ? user.FullName : user.OrganizationName;
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("firstName", user.FirstName);
            connection.AddParameter("lastName", user.LastName);
            connection.AddParameter("mail", user.Mail);
            connection.AddParameter("organization", user.OrganizationName);
            connection.AddParameter("address", user.Address);
            connection.AddParameter("notes", user.NotesJson);
            connection.AddParameter("facebookID", user.FacebookID);
            connection.AddParameter("status", user.FacebookID != null ? 1 : 0);
            connection.AddParameter("password", user.Password);
            connection.AddParameter("cell", user.Cell);
            connection.AddParameter("city", user.City);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == HttpStatusCode.OK)
            {
                user.deserialize(httpResult.GetAsJson());

                return true;
            }
            else if (httpResult.HttpStatusCode == HttpStatusCode.Conflict)
            {
                errorMessage.errorTarget = "Mail";
                errorMessage.errorText = PageResources.UserMailIsEnrolled;
            }
            user.Password = null;

            var errorObj = new { response = httpResult.Response, method = "User/CreateUser", user = user };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }
        public bool UpdateUser(ref User user)
        {
            ServiceError err;
            return this.UpdateUser(ref user, out err);
        }

        public bool UpdateUser(ref User user, out ServiceError errorMessage)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/{1}/update", ApplicationConfiguration.RestServerUrl, user.UserID);
            errorMessage = new ServiceError();

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("firstName", user.FirstName);
            connection.AddParameter("lastName", user.LastName);
            connection.AddParameter("mail", user.Mail);
            connection.AddParameter("facebookID", user.FacebookID);
            connection.AddParameter("cell", user.Cell);
            connection.AddParameter("countryCode", user.CountryCode);
            connection.AddParameter("city", user.City);
            connection.AddParameter("district", user.District);
            connection.AddParameter("address", user.Address);
            connection.AddParameter("organization", user.OrganizationName);
            connection.AddParameter("notes", user.NotesJson);
            connection.AddParameter("status", user.Status ? 1 : 0);

            HttpResult httpResult = connection.Execute();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                user.deserialize(httpResult.GetAsJson());
                return true;
            }
            user.Password = null;
            var errorObj = new { response = httpResult.Response, method = "User/UpdateUser", user = user };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }
        public User ChangePassword(User currentUser)
        {
            String URL = String.Empty;

            URL = string.Format("{0}/user/{1}/update", ApplicationConfiguration.RestServerUrl, currentUser.UserID);
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("password", currentUser.Password);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return currentUser;
            }
            currentUser.Password = null;
            var errorObj = new { response = httpResult.Response, method = "User/ChangePassword", user = currentUser };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;


        }
        public User GetByID(int ID)
        {
            User user = new User();
            String URL = String.Empty;
            URL = string.Format("{0}/user/{1}", ApplicationConfiguration.RestServerUrl, ID);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                user.deserialize(httpResult.GetAsJson());
                return user;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return null;
            }
            var errorObj = new { response = httpResult.Response, method = "User/GetByID", id = ID };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }
        public List<User> GetByName(string name)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("firstname", name);
            HttpResult httpResult = connection.Execute();
            List<User> userList = new List<User>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

                var result = httpResult.GetAsJArray();

                foreach (var item in httpResult.GetAsJArray())
                {
                    userList.Add(new User().deserialize(item.ToObject<JObject>()));
                }

                return userList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return userList;
            }
            var errorObj = new { response = httpResult.Response, method = "User/GetByName", userList = userList };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return userList;

        }
        public User GetByFacebook(string ID)
        {
            User user = new User();
            String URL = String.Empty;
            URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            connection.AddParameter("facebookID", ID);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    user.deserialize(item.ToObject<JObject>());
                }
                return user;
            }
            var errorObj = new { response = httpResult.Response, method = "User/GetByFacebook", fbID = ID };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }

        public User GetUserByMail(String email)
        {
            email = email.ToLower(ApplicationConfiguration.TurkishCultureInfo);
            if (email.IsEmailAddress())
            {
                User user = new User();
                user.Mail = email;
                String URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

                HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
                connection.AddParameter("mail", email);

                HttpResult httpResult = connection.Execute();

                if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    foreach (var item in httpResult.GetAsJArray())
                    {
                        user.deserialize(item.ToObject<JObject>());
                        if (user.Mail == email)
                        {
                            return user;
                        }
                    }
                    return null; 
                }
                else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }
                var errorObj = new { response = httpResult.Response, method = "User/GetUserByMail", mailAddress = email };
                Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            }
            return null;
        }
        public List<User> Search(String firstName, String lastName, String organization, String mail)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("firstName", firstName);
            connection.AddParameter("lastName", lastName);
            connection.AddParameter("organization", organization);
            connection.AddParameter("mail", mail);


            HttpResult httpResult = connection.Execute();
            List<User> userList = new List<User>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

                var result = httpResult.GetAsJArray();

                foreach (var item in httpResult.GetAsJArray())
                {
                    userList.Add(new User().deserialize(item.ToObject<JObject>()));
                }

                return userList;
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return userList;
            }
            var errorObj = new { response = httpResult.Response, firstname = firstName, lastName = lastName, organization = organization, mail = mail, method = "User/Search" };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;
        }
        public bool Login(string userName, string password, out User user)
        {
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, string.Format("{0}/user/login", ApplicationConfiguration.RestServerUrl));
            HttpResult httpResult;
            if (userName.IsEmailAddress())
            {
                httpConnection.AddParameter("mail", userName);
                httpConnection.AddParameter("password", password);
            }
            else
            {
                httpConnection.AddParameter("facebookID", userName);
            }

            httpResult = httpConnection.Execute();
            user = new User();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                user = new User().deserialize(httpResult.GetAsJson());
                return true;
            }
            var errorObj = new { response = httpResult.Response, method = "User/Login", user = user };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return false;
        }
        public User LoginUserByFacebookId(string facebookId)
        {
            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, string.Format("{0}/user/login", ApplicationConfiguration.RestServerUrl));

            connection.AddParameter("facebookID", facebookId);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return new User().deserialize(httpResult.GetAsJson());
            }
            var errorObj = new { response = httpResult.Response, method = "User/LoginUserByFacebookId", fbID = facebookId };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return null;

        }
        public List<User> GetByOrganization(string organization)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("organization", organization);

            HttpResult httpResult = connection.Execute();
            List<User> userList = new List<User>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    userList.Add(new User().deserialize(item.ToObject<JObject>()));
                }
                return userList;
            }
            var errorObj = new { response = httpResult.Response, method = "User/GetByOrganization", userList = userList };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return userList;

        }
        public List<User> GetAllUsers()
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/search", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("firstName", "%");

            HttpResult httpResult = connection.Execute();
            List<User> userList = new List<User>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    userList.Add(new User().deserialize(item.ToObject<JObject>()));
                }
                return userList;
            }
            var errorObj = new { response = httpResult.Response, method = "User/GetByOrganization", userList = userList };
            Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(JsonConvert.SerializeObject(errorObj)));
            return userList;

        }


        public User GetByTwitter(string pTwitterID)
        {
            String URL = String.Empty;
            URL = string.Format("{0}/user/mifareUID", ApplicationConfiguration.RestServerUrl);

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);

            connection.AddParameter("mifareUID", pTwitterID);

            HttpResult httpResult = connection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                foreach (var item in httpResult.GetAsJArray())
                {
                    return (new User().deserialize(item.ToObject<JObject>()));
                }
            }

            return new User();
        }
    }
}
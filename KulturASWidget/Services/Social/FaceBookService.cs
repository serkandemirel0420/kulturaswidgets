﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using KulturASWidget.Common;

using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using System.Dynamic;
using KulturASWidget.Services.Http;
using KulturASWidget.Objects;
using Newtonsoft.Json;
using System.Globalization;

namespace KulturASWidget.Services.Social
{
    public class FacebookService
    {

        private const string FACEBOOKBASEURL = @"https://graph.facebook.com/";
        public String PrepareFacebookConnectUrl(String scope, String redirectAction)
        {
            return string.Format("https://www.facebook.com/dialog/oauth?client_id={0}&redirect_uri={1}{2}&scope={3}", ApplicationConfiguration.FacebookAppID, ApplicationConfiguration.AppRoot, redirectAction, scope);
        }

        public String GetAccessToken(String redirectAction, String code)
        {
            String facebookAccessTokenURL = FACEBOOKBASEURL + "oauth/access_token";
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, facebookAccessTokenURL);
            httpConnection.AddParameter("client_id", ApplicationConfiguration.FacebookAppID);
            httpConnection.AddParameter("redirect_uri", String.Format("{0}{1}", ApplicationConfiguration.AppRoot, redirectAction));
            httpConnection.AddParameter("client_secret", ApplicationConfiguration.FacebookAppSecret);
            httpConnection.AddParameter("code", code);

            HttpResult httpResult = httpConnection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                string accessToken = httpResult.Response.Replace("access_token=", "");
                if (accessToken.IndexOf("&").IsValidIndex())
                {
                    accessToken = accessToken.Substring(0, accessToken.IndexOf("&"));
                }
                return accessToken;
            }
            return "";
        }
        //TODO: Rewrite
        public User GetUser(String accessToken)
        {
            String facebookProfileUrl = FACEBOOKBASEURL + "me";
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, facebookProfileUrl);
            httpConnection.AddParameter("access_token", accessToken);

            HttpResult httpResult = httpConnection.Execute();

            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                JObject jsonResult = httpResult.GetAsJson();

                User user = new User();
                user.FirstName = jsonResult.Value<String>("first_name");
                user.LastName = jsonResult.Value<String>("last_name");
                user.Mail = jsonResult.Value<String>("email");
                user.FacebookID = jsonResult.Value<String>("id");
              
                var genderStr = jsonResult.Value<String>("gender");
                if(!genderStr.IsEmpty())
                {
                    user.Gender = (Gender)Enum.Parse(typeof(Gender), genderStr, true);
                }
                return user;
            }

            return null;
        }

        public List<FBAccount> GetFBAccounts(String accessToken)
        {
            String URL = String.Format("{0}me/accounts", FACEBOOKBASEURL);
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            conn.AddParameter("access_token", accessToken);
            var result = conn.Execute();

            if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<FBAccountsResponse>(result.Response).data.Where(x => x.category != "Application").ToList();
            }
            return new List<FBAccount>();
        }

        public FBCreateEventResponse ExportEventToFacebook(FBAccount account, EventAIO ev)
        {
            String URL = String.Format("{0}/{1}/events", FACEBOOKBASEURL, account.id);
            var dateFormat = "yyyy-MM-ddTHH:mm:ss-0000";
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.POST, URL);
            conn.AddParameter("access_token", account.access_token);
            conn.AddParameter("name", ev.UserEvent.Name);
            var commenceUTC = TimeZoneInfo.ConvertTimeToUtc(ev.UserEvent.CommenceDate, ApplicationConfiguration.CurrentTimezone);
            conn.AddParameter("start_time", commenceUTC.ToString(dateFormat, CultureInfo.InvariantCulture));
            var endUTC = TimeZoneInfo.ConvertTimeToUtc(ev.UserEvent.EndDate, ApplicationConfiguration.CurrentTimezone);
            conn.AddParameter("end_time", endUTC.ToString(dateFormat, CultureInfo.InvariantCulture));
            conn.AddParameter("description", ev.UserEvent.Detail.StripHTML());
            conn.AddParameter("location", ev.UserVenue.Name);
            conn.AddParameter("privacy_type", ev.UserEvent.IsPrivate ? "SECRET" : "OPEN");

            var response = conn.Execute();
            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<FBCreateEventResponse>(response.Response);
            }
            return new FBCreateEventResponse();

        }

        public bool UploadImageForEvent(string eventID, string url, string accessToken)
        {
            throw new NotImplementedException();
            /*String URL = String.Format("{0}/{1}/picture", FACEBOOKBASEURL, eventID);
            HttpConnection conn = HttpConnectionFactory.Create(HttpMethodType.POST, URL);
            conn.AddParameter("access_token", accessToken);
            conn.AddParameter("source", url);
            var response = conn.Execute();
            if (response.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

            }
            else
            { 
            }

            return true;*/
        }

        #region hide old
        /*
        public string ShareOnFacebook(Event userEvent, String accessToken, String selectedFriends)
        {
            UrlHelper url = new UrlHelper(HttpContext.Current.Request.RequestContext);
            User user = GetUser(accessToken);
            String[] selectedFriendsArr = selectedFriends.Split(',');
            List<string> friendIds = new List<string>();
            foreach (var item in selectedFriendsArr)
            {
                friendIds.Add(item);
            }


            //string urlCon = String.Format("{0}{1}/feed", FACEBOOKBASEURL, "me");
            //HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.POST, FACEBOOKBASEURL +"100004077819509"+ "/feed");

            //httpConnection.AddParameter("access_token", accessToken);
            //httpConnection.AddParameter("link", @"https://www.biletino.com/Event/Show/228/hakan_karakurt_bohem_bar_sahanelik");
            //httpConnection.AddParameter("name", userEvent.Name);
            //httpConnection.AddParameter("caption", userEvent.Detail);
            //httpConnection.AddParameter("picture", userEvent.Thumb90);
            //httpConnection.AddParameter("to", "100004077819509");

            //HttpResult result = httpConnection.Execute();
            //if (result.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
            //{
            //    return null;
            //}





            return "OK";


        }*/



        /*
        public List<FacebookEvent> GetFacebookEventList(String accessToken, User CurrentUser)
        {
            String facebookProfileUrl = FACEBOOKBASEURL + "me/events";
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, facebookProfileUrl);
            httpConnection.AddParameter("access_token", accessToken);
            httpConnection.AddParameter("fields", "id,name,description,start_time,end_time,timezone,location,venue,privacy,updated_time,owner,picture");

            HttpResult httpResult = httpConnection.Execute();

            List<FacebookEvent> eventList = new List<FacebookEvent>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

                FacebookEventList list = new FacebookEventList().deserialize(httpResult.GetAsJson());

                foreach (var item in list.eventList)
                {
                    FacebookEvent ev = GetFacebookEvent(item.id, accessToken);
                    if (ev.owner.id == CurrentUser.FacebookID)
                    {

                        eventList.Add(ev);
                    }
                }
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                return null;
            }
            return eventList;
        }
        public List<FacebookFriend> GetFacebookFriendList(String accessToken)
        {
            String facebookProfileUrl = FACEBOOKBASEURL + "me/friends";
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, facebookProfileUrl);
            httpConnection.AddParameter("access_token", accessToken);

            HttpResult httpResult = httpConnection.Execute();
            int a = 0;
            List<FacebookFriend> friendList = new List<FacebookFriend>();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {

                FacebookFriendList list = new FacebookFriendList().deserialize(httpResult.GetAsJson());

                foreach (var item in list.friendList)
                {
                    FacebookFriend ff = GetFacebookFriend(item.id, accessToken);
                    if (a == 20)
                    {
                        return friendList;
                    }
                    friendList.Add(ff);
                    a++;

                }
            }
            else if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                return null;
            }
            return friendList;
        }
        
        public FacebookEvent GetFacebookEvent(string eventID, string accessToken)
        {
            String URL = String.Format("{0}{1}", FACEBOOKBASEURL, eventID);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            httpConnection.AddParameter("access_token", accessToken);
            httpConnection.AddParameter("fields", "id,name,description,start_time,end_time,timezone,location,venue,privacy,updated_time,owner,picture");

            HttpResult httpResult = httpConnection.Execute();
            FacebookEvent ev = new FacebookEvent();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                ev.deserialize(httpResult.GetAsJson());
                ev.venue = FacebookService.Instance.GetFacebookVenue(ev.venueID, accessToken);
            }
            return ev;
        }
        public FacebookFriend GetFacebookFriend(string friendID, string accessToken)
        {
            String URL = String.Format("{0}{1}", FACEBOOKBASEURL, friendID);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            httpConnection.AddParameter("access_token", accessToken);

            HttpResult httpResult = httpConnection.Execute();
            FacebookFriend ff = new FacebookFriend();
            ff.deserialize(httpResult.GetAsJson());

            return ff;
        }
        public FacebookVenue GetFacebookVenue(string venueID, string accessToken)
        {
            String URL = String.Format("{0}{1}", FACEBOOKBASEURL, venueID);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, URL);
            httpConnection.AddParameter("access_token", accessToken);

            HttpResult httpResult = httpConnection.Execute();
            FacebookVenue ve = new FacebookVenue();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                ve.deserialize(httpResult.GetAsJson());
            }
            return ve;
        }
        public string GetFacebookFriendListAsJson(string AccessToken, string FacebookID)
        {
            String url = String.Format("{0}{1}/friends", FACEBOOKBASEURL, FacebookID);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, url);
            httpConnection.AddParameter("access_token", AccessToken);

            HttpResult httpResult = httpConnection.Execute();

            return httpResult.Response;
        }
        public String GetWhoIsAttending(string FacebookEventId, string AccessToken)
        {
            String url = String.Format("{0}{1}/attending", FACEBOOKBASEURL, FacebookEventId);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, url);
            httpConnection.AddParameter("access_token", AccessToken);
            HttpResult httpResult = httpConnection.Execute();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return httpResult.Response;
            }
            throw new Exception(httpResult.Response);
        }
        public string GetAppAccessToken()
        {
            //oauth/access_token?grant_type=client_credentials&client_id=273585196042945&client_secret=067e250e98941215abe233f9f842f99c
            String url = String.Format("{0}oauth/access_token", FACEBOOKBASEURL);
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.GET, url);
            httpConnection.AddParameter("grant_type", "client_credentials");
            httpConnection.AddParameter("client_id", ApplicationConfiguration.FacebookAppID);
            httpConnection.AddParameter("client_secret", ApplicationConfiguration.FacebookAppSecret);
            HttpResult httpResult = httpConnection.Execute();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return httpResult.Response.Replace("access_token=", "");

            }
            throw new Exception(httpResult.Response);

        }
        public string CreateEventAtFacebook(string name, string start_time,string access_token)
        {
            //// 405863856125233/events?name=deneme2&start_time=2012-09-20
            String url = String.Format("{0}{1}/events", FACEBOOKBASEURL, "418271358237388");
            HttpConnection httpConnection = HttpConnectionFactory.Create(HttpMethodType.POST, url);
            httpConnection.AddParameter("name", name);
            httpConnection.AddParameter("start_time", start_time);
            httpConnection.AddParameter("access_token", access_token);
            //httpConnection.AddParameter("end_time", ApplicationConfiguration.FacebookAppSecret);
            HttpResult httpResult = httpConnection.Execute();
            if (httpResult.HttpStatusCode == System.Net.HttpStatusCode.OK)
            {
                return httpResult.Response;

            }
            throw new Exception(httpResult.Response);

        }*/
        #endregion
        private static volatile FacebookService instance;
        private static object syncRoot = new Object();

        private FacebookService()
        {

        }

        public static FacebookService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new FacebookService();
                    }
                }

                return instance;
            }
        }

    }

    public class FBAccountsResponse
    {
        public List<FBAccount> data { get; set; }
    }

    public class FBAccount
    {
        public string name { get; set; }
        public string access_token { get; set; }
        public string category { get; set; }
        public string id { get; set; }
    }

    public class FBCreateEventResponse
    {
        public string id { get; set; }
    }

}
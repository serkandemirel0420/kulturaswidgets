﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using KulturASWidget.Services.Security;

namespace KulturASWidget.Services
{
    public class CookieService
    {
        Dictionary<String, Dictionary<String, CookieObj>> CookieValues;
        Dictionary<long, List<RemoveObj>> TimerObj;

        private static volatile CookieService instance;
        private static object syncRoot = new Object();


        public static CookieService Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new CookieService();
                    }
                }
                return instance;
            }
        }

        private CookieService()
        {
            CookieValues = new Dictionary<string, Dictionary<string, CookieObj>>();
            TimerObj = new Dictionary<long, List<RemoveObj>>();
        }

        private class CookieObj
        {
            public DateTime endTime;
            public object obj;

            public CookieObj(DateTime p_endTime, object p_obj)
            {
                endTime = p_endTime;
                obj = p_obj;
            }
        }

        private class RemoveObj
        {
            public string userKey;
            public string cookieID;

            public RemoveObj(string p_userKey, string p_cookieID)
            {
                userKey = p_userKey;
                cookieID = p_cookieID;
            }
        }

        public DateTime GetExpireTime(String key, HttpRequestBase request)
        {
            var cService = new CryptolographyService(CryptolographyService.ServiceProviderEnum.TripleDES);
            String cookieKey;
            if (request.Cookies.AllKeys.Contains(COOKIE_KEY_ID))
            {
                cookieKey = cService.Decrypt(request.Cookies[COOKIE_KEY_ID].Value);

                var matches = (from item in (from obj in TimerObj select obj.Value).SelectMany(x => x) where item.cookieID == key && item.userKey == cookieKey select item);
                if (matches.Count() > 0)
                {
                    var timeKeys = (from lst in TimerObj where lst.Value.Contains(matches.First()) select lst.Key);
                    if (timeKeys.Count() > 0)
                    {
                        return new DateTime(timeKeys.First() * (long)1E7);
                    }
                }
            }

            return new DateTime(0);

        }

        public String COOKIE_KEY_ID = "ASDPASNDOASDPO";
        public T Get<T>(String key, HttpRequestBase request)
        {
            var cService = new CryptolographyService(CryptolographyService.ServiceProviderEnum.TripleDES);
            String cookieKey;
            if (request.Cookies.AllKeys.Contains(COOKIE_KEY_ID))
            {
                cookieKey = cService.Decrypt(request.Cookies[COOKIE_KEY_ID].Value);
                if (CookieValues.ContainsKey(cookieKey) && CookieValues[cookieKey].ContainsKey(key))
                {
                    var cookieObj = CookieValues[cookieKey][key];
                    if (cookieObj.endTime > DateTime.Now)
                    {
                        return (T)cookieObj.obj;
                    }
                }
            }

            var timeKey = (long)(DateTime.Now.Ticks / 1E7);
            var ListsToRemove = (from tObj in TimerObj where tObj.Key > timeKey select tObj.Value);
            RemoveAll(TimerObj, tDic => ListsToRemove.Contains(tDic.Value));

            foreach (var item in ListsToRemove.SelectMany(x => x).ToList())
            {
                CookieValues[item.userKey].Remove(item.cookieID);
            }

            return default(T);
        }



        public void Set<T>(String key, T obj, int minutes, HttpRequestBase request, HttpResponseBase response)
        {
            var cService = new CryptolographyService(CryptolographyService.ServiceProviderEnum.TripleDES);
            String cookieKey;
            if (request.Cookies.AllKeys.Contains(COOKIE_KEY_ID))
            {
                cookieKey = cService.Decrypt(request.Cookies[COOKIE_KEY_ID].Value);
            }
            else
            {
                Random rand = new Random();
                int seed = 0;
                for (int i = 0; i < 5; i++, seed = rand.Next()) ;
                rand = new Random(seed);
                for (int i = 0; i < 5; i++, seed = rand.Next()) ;
                rand = new Random(seed);

                cookieKey = String.Format("{0}{1:yyhhssmm}", rand.Next(), DateTime.Now);
            }

            if (!CookieValues.ContainsKey(cookieKey))
            {
                CookieValues.Add(cookieKey, new Dictionary<string, CookieObj>());
            }

            var values = CookieValues[cookieKey];

            var endTime = DateTime.Now.AddMinutes(minutes);
            var timeKey = (long)(endTime.Ticks / 1E7);

            if (!TimerObj.ContainsKey(timeKey))
            {
                TimerObj.Add(timeKey, new List<RemoveObj>());
            }

            var unifiedList = (from tObj in TimerObj select tObj.Value).SelectMany(x => x);
            var objList = (from rObj in unifiedList where rObj.userKey == cookieKey && rObj.cookieID == key select rObj);
            RemoveObj objExists = null;
            if (objList.Count() > 0)
            {
                objExists = objList.First();
            }

            if (objExists != null)
            {
                var listToRemoveFrom = TimerObj.Where(x => x.Value.Where(v => v == objExists).Count() > 0).Select(x => x.Value).First();
                listToRemoveFrom.Remove(objExists);
            }


            TimerObj[timeKey].Add(new RemoveObj(cookieKey, key));


            if (values.ContainsKey(key))
            {
                values[key] = new CookieObj(endTime, obj);
            }
            else
            {
                values.Add(key, new CookieObj(endTime, obj));
            }

            var cookie = new HttpCookie(COOKIE_KEY_ID);

            cookie.Expires = DateTime.Now.AddYears(1);
            cookie.Value = cService.Encrypt(cookieKey);

            response.SetCookie(cookie);
        }

        public bool Has(String key, HttpRequestBase request)
        {
            var cService = new CryptolographyService(CryptolographyService.ServiceProviderEnum.TripleDES);
            String cookieKey;
            if (request.Cookies.AllKeys.Contains(COOKIE_KEY_ID))
            {
                cookieKey = cService.Decrypt(request.Cookies[COOKIE_KEY_ID].Value);
                if (CookieValues.ContainsKey(cookieKey) && CookieValues[cookieKey].ContainsKey(key))
                {
                    return CookieValues[cookieKey][key].endTime > DateTime.Now;
                }
            }
            return false;
        }


        public void RemoveAll<TKey, TValue>(Dictionary<TKey, TValue> dict,
                                     Func<KeyValuePair<TKey, TValue>, bool> condition)
        {
            foreach (var cur in dict.Where(condition).ToList())
            {
                dict.Remove(cur.Key);
            }
        }
    }
}
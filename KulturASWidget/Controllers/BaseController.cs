﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Helpers;

namespace KulturASWidget.Controllers
{
    public class BaseController : Controller
    {

        protected SessionHelper sessionHelper;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            initSessionHelper();
        }

        private void initSessionHelper()
        {
            Object currentSessionHelper = Session[SessionHelper.SESSIONHELPER_INSTANCE_KEY];
            if (currentSessionHelper == null)
            {
                sessionHelper = new SessionHelper();
                Session[SessionHelper.SESSIONHELPER_INSTANCE_KEY] = sessionHelper;
            }
            else
            {
                sessionHelper = (SessionHelper)currentSessionHelper;

            }

        }
    }
}

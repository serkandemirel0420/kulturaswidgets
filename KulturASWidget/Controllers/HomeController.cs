﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KulturASWidget.Services;
using KulturASWidget.Models.Widget;
using KulturASWidget.Objects;
using KulturASWidget.Common;
using KulturASWidget.Services.Security;
using KulturASWidget.Models.Garanti;
using Newtonsoft.Json;
using KulturASWidget.Models.Event;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using KulturASWidget.Services.Http;
using KulturASWidget.Models.Garanti.Response;
using KulturASWidget.Resources;

namespace KulturASWidget.Controllers
{
    public class HomeController : BaseController
    {
        private const string EVENTIDSESSIONKEY = "eventIdSessionKey";
        private const String ATTENDEEINFOSESSIONKEY = "attendeeSessionKey";
        private const String RESLIST_SESSIONKEY = "resListSessionKey";
        int eventID = 1;
        int layoutID = 1;
        int userID = 1;
        int priceKr = 1000;

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MainWidget()
        {
            EventTicketDetailModel model = new EventTicketDetailModel(eventID);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MainWidget(EventTicketDetailModel model)
        {
            if (ModelState.IsValid && model.conditionsAgree)
            {
                Objects.User user = UserService.Instance.GetByID(eventID);

                int Quantity = model.ticketCount;
                ViewBag.IsFree = true;

                user.Cell = model.AttendeeGSM;
                List<Reservation> reslist = new List<Reservation>();

                for (int i = 0; i < Quantity; i++)
                {
                    reslist.Add(KulturASWidget.Services.ReservationService.Instance.QueueCreate(user.UserID, layoutID));
                }
                sessionHelper.Add(RESLIST_SESSIONKEY, reslist);
                sessionHelper.Add(ATTENDEEINFOSESSIONKEY, model);
                return RedirectToAction("PaymentWidget");
            }
            else
            {
                if (!model.conditionsAgree)
                {
                    ModelState.AddModelError("conditionsAgree","Bilet alabilmek için kullanım şartlarını kabul etmelisiniz.");
                }
                var newModel = new EventTicketDetailModel(eventID);
                newModel.AttendeeGSM = model.AttendeeGSM;
                newModel.AttendeeMail = model.AttendeeMail;
                newModel.AttendeeName = model.AttendeeName;
                return View(newModel);
            }
        }

        //3D Secure şifresi
        private string SecureKey = ApplicationConfiguration.Garanti3DSecureKey;
        //TerminalProvUserID şifresi
        private string ProvPassword = ApplicationConfiguration.GarantiProvisionPassword;
        private const String TRACKINGCODESESSIONKEY = "TrackingCode";
        public ActionResult PaymentWidget()
        {

            if (!sessionHelper.Has(RESLIST_SESSIONKEY))
            {
                return RedirectToAction("MainWidget", "Home");
            }

            var model = sessionHelper.Get<EventTicketDetailModel>(ATTENDEEINFOSESSIONKEY);
            var user = UserService.Instance.GetByID(userID);

            var resQueueList = sessionHelper.Get<List<Reservation>>(RESLIST_SESSIONKEY);
            int quantity = resQueueList.Count;

            var layout = LayoutService.Instance.GetByID(layoutID);
            layout.commissionPayer = CommissionPayer.byOrganization;
            layout.PriceWithoutCommission = priceKr;


            var resID = resQueueList.First().ReservationQueue;

            ViewBag.PaymentURL = String.Format("{0}/Event/Paypal", ApplicationConfiguration.AppRoot);

            String orderID = resID.ToString().PadLeft(11, 'Z');
            int lastIndex = orderID.LastIndexOf('Z');
            if (lastIndex != -1)
            {
                orderID = orderID.Substring(lastIndex, 11);
            }

            double subtotal = (double)layout.TotalPrice / 100 * quantity;
            Garanti3DRequest request = GarantiService.Instance.GetWidgetRequestObject(Request, user, subtotal, orderID, Url, SecureKey, ProvPassword);
            request.customeremailaddress = user.Mail;
            ViewBag.Garanti3DRequest = request;

            Response.Buffer = true;
            Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Response.Expires = -1500;
            Response.CacheControl = "no-cache";
            Response.Cache.SetNoStore();

            if (TempData.ContainsKey(MESSAGETEMPDATAKEY))
            {
                ViewBag.Message = TempData[MESSAGETEMPDATAKEY];
                TempData.Remove(MESSAGETEMPDATAKEY);
            }

            return View();
        }

        public ActionResult DirectPayment(FormCollection collection)
        {
            var user = UserService.Instance.GetByID(userID);
            var resQueueList = sessionHelper.Get<List<Reservation>>(RESLIST_SESSIONKEY);
            var model = sessionHelper.Get<EventTicketDetailModel>(ATTENDEEINFOSESSIONKEY);

            if (resQueueList.Count == 0)
            {
                var obj = new { method = "DirectPayment", mes = "No Reservation in queue" };
                Exception ex = new Exception(JsonConvert.SerializeObject(obj));
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return RedirectToAction("MainWidget", "Home");
            }
            int quantity = resQueueList.Count;
            var layout = LayoutService.Instance.GetByID(layoutID);
            layout.commissionPayer = CommissionPayer.byOrganization;
            layout.PriceWithoutCommission = priceKr;

            var eventAIO = EventService.Instance.GetByID(layout.EventID);


            DateTime resExpireTime = resQueueList.First().CreationDate.AddMinutes(15);
            TimeSpan cartExpireTime = resExpireTime - DateTime.UtcNow;
            if (DateTime.Compare(resExpireTime, DateTime.UtcNow) < 0 && cartExpireTime.Seconds < 60)
            {
                var obj = new { method = "DirectPayment", mes = "Queue Expire", CurrrentTime = DateTime.UtcNow.ToLongDateString(), ExpireTime = resExpireTime.ToLongDateString() };
                Exception ex = new Exception(JsonConvert.SerializeObject(obj));
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return RedirectToAction("MainWidget", "Home");
            }



            var discountModel = new EventPaymentModel(layout, user, quantity);
            String discountCode = sessionHelper.Get<String>("DiscountCode");
            double discountAmount = 0;
            if (discountCode.IsExists())
            {
                discountModel.findDiscount(layout, user, quantity, discountCode);
                if (discountModel.Discount.IsExists())
                {
                    discountAmount = (discountModel.Discount.DiscountPrice) * 100;
                    ViewBag.discountModel = discountModel;
                }
            }

            var resID = resQueueList.First().ReservationQueue; //her tamamlanmamış rezervasyonda unique bir queue değeri var. Bu değeri ID olarak kullan
            String orderID = resID.ToString().PadLeft(11, 'Z');
            int lastIndex = orderID.LastIndexOf('Z');
            if (lastIndex != -1)
            {
                orderID = orderID.Substring(lastIndex, 11);
            }
            GVPSRequest request = new GVPSRequest();
            request.Mode = "PROD";

            Random rand = new Random();
            var order = new Order();
            order.OrderID = orderID.Substring(0, 11);
            //order.AddressList = new Address[] { new Address() { City = "Istanbul", Company = "GranulTech", Country = "Turkiye", District = "Kadikoy", LastName = "Yildirim", Name = "Adiguzel", PhoneNumber = "00905424025896", PostalCode = "34765", Text = "DenemeAddress", Type = "S" } };
            request.Order = order;

            var card = new Card() { };
            card.CVV2 = collection["cardcvv2"];
            card.ExpireDate = String.Format("{0}{1}", collection["cardexpiredatemonth"], collection["cardexpiredateyear"]);
            card.Number = collection["cardnumber"];
            request.Card = card;


            var transaction = new KulturASWidget.Models.Garanti.Transaction();
            transaction.InstallmentCnt = "0";
            transaction.Amount = ((int)(((layout.TotalPrice * quantity) - (discountAmount * quantity)))).ToString();
            request.Transaction = transaction;

            Hash hash = new Hash(Hash.ServiceProviderEnum.SHA1);
            var provisionPassword = ApplicationConfiguration.GarantiProvisionPassword;

            var terminal = new Terminal();
            terminal.ID = ApplicationConfiguration.GarantiTerminalID;
            terminal.MerchantID = ApplicationConfiguration.GarantiMarchantID;
            terminal.UserID = "PROVAUT";
            var securityData = GarantiService.Instance.GetSHA1(provisionPassword + terminal.ID.ToString().PadLeft(9, '0')).ToUpper();
            var hashData = GarantiService.Instance.GetSHA1(order.OrderID + terminal.ID + card.Number + transaction.Amount + securityData).ToUpper();


            terminal.HashData = hashData;


            request.Terminal = terminal;

            var customer = new Customer();
            customer.EmailAddress = model.AttendeeMail;
            if (ApplicationConfiguration.IsDevelopment)
            {
                customer.IPAddress = ApplicationConfiguration.OfficeIPAddress;
            }
            else
            {
                customer.IPAddress = Request.UserHostAddress;
            }
            request.Customer = customer;

            XmlSerializer ser = new XmlSerializer(request.GetType());
            var stream = new MemoryStream();

            ser.Serialize(stream, request);

            string xml;
            xml = Encoding.UTF8.GetString(stream.GetBuffer());

            HttpConnection connection = HttpConnectionFactory.Create(HttpMethodType.POST, "https://sanalposprov.garanti.com.tr/VPServlet");
            connection.AddBody("data=" + xml);

            HttpResult result = connection.Execute();

            XmlSerializer responseSerializer = new XmlSerializer(typeof(GVPSResponse));
            byte[] byteArray = Encoding.UTF8.GetBytes(result.Response);
            GVPSResponse response = (GVPSResponse)responseSerializer.Deserialize(new MemoryStream(byteArray));
            
            //Başarılı

            if (response.Transaction.Response.ReasonCode == "00")
            {
                List<Reservation> reservationsToPayList = new List<Reservation>();

                for (int i = 0; i < resQueueList.Count; i++)
                {
                    if (resQueueList[i].LayoutID == layout.LayoutID)
                    {
                        var res = ReservationService.Instance.QueueFinalize(resQueueList[i].ReservationQueue);

                        res.AttendeeEmail = model.AttendeeMail;
                        res.AttendeeName = model.AttendeeName;
                        res.AttendeePhone = model.AttendeeGSM;

                        res.DiscountAmount = discountAmount.ToString();
                        res.DiscountCode = discountCode;
                        ReservationService.Instance.Update(res);
                        reservationsToPayList.Add(res);
                    }
                }
                ReservationService.Instance.PayAll(reservationsToPayList, response);

                string TrackingCode = sessionHelper.Get<String>(TRACKINGCODESESSIONKEY);
                var trackingCodes = eventAIO.UserEvent.Tracing;
                foreach (var reservation in reservationsToPayList)
                {
                    if (trackingCodes.IsExists())
                    {
                        if (trackingCodes.IsExists())

                            foreach (var trackingCode in trackingCodes)
                            {
                                String code = trackingCode.tracingCode;
                                if (code == TrackingCode)
                                {
                                    reservation.TrackingCode = TrackingCode;
                                }
                            }
                    }
                    ReservationService.Instance.Update(reservation);

                    //TODO:
                    MailService.Instance.SendTicketMail(model.AttendeeMail, reservation.ReservationID, EventService.Instance.GetByID(reservation.EventID));
                    if (model.AttendeeGSM.Replace(" ", "").Length >= 10)
                    {
                        ReservationService.Instance.SendSMS(reservation, model.AttendeeGSM.Replace(" ", "").Substring(model.AttendeeGSM.Length - 10, model.AttendeeGSM.Length));
                    }
                }
                //sessionHelper.Add(RESERVATIONSTOSENDKEY, reservationsToPayList);
                return RedirectToAction("MessageWidget");
            }
            else
            {
                //başarısız
                TempData.Remove(MESSAGETEMPDATAKEY);
                TempData.Add(MESSAGETEMPDATAKEY, response.Transaction.Response.ErrorMessageFromCode);
                var obj = new { method = "DirectPayment", mes = "Payment Not Made", responseCode = response.Transaction.Response.ReasonCode };
                Exception ex = new Exception(JsonConvert.SerializeObject(obj));
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return RedirectToAction("PaymentWidget");
            }
        }

        public ActionResult CreditCardPaymentReturn()
        {
            var user = UserService.Instance.GetByID(userID);
            var resQueueList = sessionHelper.Get<List<Reservation>>(RESLIST_SESSIONKEY);
            var model = sessionHelper.Get<EventTicketDetailModel>(ATTENDEEINFOSESSIONKEY);
            int layoutId = resQueueList.First().LayoutID;
            int quantity = resQueueList.Count;
            var layout = LayoutService.Instance.GetByID(layoutId);
            var eventAIO = EventService.Instance.GetByID(layout.EventID);


            if (resQueueList.Count == 0)
            {
                var obj = new { method = "CreditCardPaymentReturn", mes = "No Reservation in queue" };
                Exception ex = new Exception(JsonConvert.SerializeObject(obj));
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return RedirectToAction("MainWidget", "Home");
            }



            var cart = new Cart();
            cart.Layout = layout;
            cart.Quantity = quantity;
            var resExpireTime = resQueueList.First().CreationDate.AddMinutes(15);
            TimeSpan cartExpireTime = resExpireTime - DateTime.UtcNow;
            if (DateTime.Compare(resExpireTime, DateTime.UtcNow) < 0 && cartExpireTime.Seconds < 60)
            {
                var obj = new { method = "CreditCardPaymentReturn", mes = "Expired", expireTime = resExpireTime.ToLongDateString(), currentTime = DateTime.UtcNow.ToLongDateString() };
                Exception ex = new Exception(JsonConvert.SerializeObject(obj));
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return RedirectToAction("MainWidget", "Home");
            }

            Garanti3DResponse requestResponse = new Garanti3DResponse(Request.Form, SecureKey, ProvPassword);


            string message;
            if (requestResponse.ValidateHashData == requestResponse.hashDataFromServer)
            {

                if (requestResponse.MDStatus == "1" | requestResponse.MDStatus == "2" | requestResponse.MDStatus == "3" | requestResponse.MDStatus == "4")
                {


                    GVPSResponse response = GarantiService.Instance.MakeProvisionRequest(requestResponse);

                    //Başarılı
                    if (response.Transaction.Response.ReasonCode == "00")
                    {
                        List<Reservation> reservationsToPayList = new List<Reservation>();
                        for (int i = 0; i < resQueueList.Count; i++)
                        {

                            if (resQueueList[i].LayoutID == layout.LayoutID)
                            {
                                reservationsToPayList.Add(ReservationService.Instance.QueueFinalize(resQueueList[i].ReservationQueue));
                            }
                        }

                        ReservationService.Instance.PayAll(reservationsToPayList, response);



                        /* var discountModel = new EventPaymentModel(layout, user, quantity);
                         String discountCode = sessionHelper.Get<String>(DISCOUNTCODESESSIONKEY);
                         double discountAmount = 0;
                         if (discountCode.IsExists())
                         {
                             discountModel.findDiscount(layout, CurrentUser, quantity, discountCode);
                             if (discountModel.Discount.IsExists())
                             {
                                 discountAmount = (discountModel.Discount.DiscountPrice) * 100;
                                 ViewBag.discountModel = discountModel;
                             }
                         }*/

                        string TrackingCode = sessionHelper.Get<String>(TRACKINGCODESESSIONKEY);
                        var trackingCodes = eventAIO.UserEvent.Tracing;
                        foreach (var reservation in reservationsToPayList)
                        {
                            if (trackingCodes.IsExists())
                            {
                                if (trackingCodes.IsExists())

                                    foreach (var trackingCode in trackingCodes)
                                    {
                                        String code = trackingCode.tracingCode;
                                        if (code == TrackingCode)
                                        {
                                            reservation.TrackingCode = TrackingCode;
                                        }
                                    }
                            }

                            ReservationService.Instance.Update(reservation);

                            //TODO:
                            MailService.Instance.SendTicketMail(model.AttendeeMail, reservation.ReservationID, EventService.Instance.GetByID(reservation.EventID));

                            ReservationService.Instance.SendSMS(reservation, model.AttendeeGSM.Replace(" ", "").Substring(model.AttendeeGSM.Length - 10, model.AttendeeGSM.Length));
                        }

                        return RedirectToAction("MessageWidget");
                    }
                    else
                    {
                        //başarısız
                        TempData.Add(MESSAGETEMPDATAKEY, response.Transaction.Response.ErrorMessageFromCode);

                        return RedirectToAction("PaymentWidget");
                    }
                }
                else
                {

                    message = String.Format("{0} açıklama : {1}", PageResources.BankProcessError, requestResponse.MDStatusText);
                }

            }
            else
            {
                message = String.Format("{0} açıklama : {1}", PageResources.BankSystemError, requestResponse.MDStatusText);
            }

            object errorMessage;
            if (TempData.TryGetValue(MESSAGETEMPDATAKEY, out errorMessage))
            {
                TempData.Remove(MESSAGETEMPDATAKEY);
            }

            return RedirectToAction("PaymentWidget");
        }
        private const String MESSAGETEMPDATAKEY = "messagedatakey";


        public ActionResult MessageWidget()
        {


            return View();
        }
    }
}
